<?php
/******************************************************************************/
/*																			*/
/* map_editor.php - Map Editor												*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 3 June 2001														*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require('config.inc.php');
include('lib.inc.php');
include('lib_template.inc.php');

require('lib_session.inc.php');

require('lib_events.inc.php');

if ($char['admin_level'] <= 8)
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Not a high enough admin to access map editor!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

// Vars from post go here.  Oh god is this a biatch.
$option = Retrieve_var('option');
$map_selected = Retrieve_var('map_selected');
$map_name = Retrieve_var('map_name');
$map_width = Retrieve_var('map_width');
$map_height = Retrieve_var('map_height');
$xpos = Retrieve_var('xpos');
$ypos = Retrieve_var('ypos');
$sector = Retrieve_var('sector');

require('template_header.inc.php');

OpenTable('title', '600');
echo 'phpRPG Map Editor';
OpenTable('content');

switch (@$option)
{
	case 'delete_map_now':
		echo '
<font color="#eeeeee">Deleting Map</font><br>
		';
		$result = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map_selected'") or die('Database Error: ' . mysql_error() . '<br>');
		echo '
Map "<font color="#eeeeee">' . $map_selected . '</font>" has been deleted.<br>
		';
		$result = mysql_query("OPTIMIZE TABLE " . PHPRPG_DB_PREFIX . "_map") or die('Database Error: ' . mysql_error() . '<br>');
		echo '
Table has been optimized.<br>
		';
		break;

	case 'delete_map':
		echo '
<font color="#eeeeee">Delete Map</font><br>
		';
		$result = mysql_query("SELECT DISTINCT name FROM " . PHPRPG_DB_PREFIX . "_map ORDER BY name") or die('Database Error: ' . mysql_error() . '<br>');
		if (mysql_num_rows($result) != 0)
		{
			echo '
<form method="post" action="' . PHP_SELF . '?s=' . $s . '">
<input type="hidden" name="option" value="delete_map_now">
<table cellpadding="0" cellspacing="4" border="0">
			';
			while ($map = mysql_fetch_array($result))
			{
				echo '
  <tr><td><input type="radio" name="map_selected" value="' . $map['name'] . '"></td><td>' . $map['name'] . '</td></tr>
				';
			}
			echo '
  <tr><td colspan="2"><div align="right"><input type="submit" value="Delete Map" class="inputbutton"></div></td></tr>
</table>
</form>
			';
		}
		else
		{
			echo '
Error: No map entry was found!<br>
			';
		}
		break;

	case 'edit_sector_now':
		// Need to modify this for arrays
		if(is_array($sector))
		{
			foreach ($sector as $x => $x_array)
			{
				foreach ($x_array as $y => $y_array)
				{
					$sql = "UPDATE " . PHPRPG_DB_PREFIX . "_map
							SET type = '{$y_array['type']}'
								,subtype = '{$y_array['subtype']}'
								,move_ul = '{$y_array['ul']}'
								,move_dl = '{$y_array['dl']}'
								,move_up = '{$y_array['up']}'
								,move_dn = '{$y_array['dn']}'
								,move_ur = '{$y_array['ur']}'
								,move_dr = '{$y_array['dr']}'
							WHERE (name = '{$map_selected}' AND xpos = {$x} AND ypos = {$y})";
					mysql_query($sql) or die('Database Error: ' . mysql_error() . '<br>');
				}
			}
		}
		else
		{
			$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_map SET move_up='$move_up', move_ur='$move_ur', move_dr='$move_dr', move_dn='$move_dn', move_dl='$move_dl', move_ul='$move_ul', type='$type', subtype='$subtype' WHERE (name='$map_selected' AND xpos=$xpos AND ypos=$ypos)") or die('Database Error: ' . mysql_error() . '<br>');
			$edit_sector_message = 'Sector (' . $xpos . ', ' . $ypos . ')</b> has been successfully updated.<br>';
		}
		// Fall through to 'edit_sector', refresh updated sector

	case 'edit_sector':
		/*																							 */
		/* string SectorEditImageString($x, $y, $direction, $info, $imageOnly = False)				 */
		/*	Returns the image string required for editing the directional access from of the sector  */
		/*	$x and $y are the coordinates of the sector in question								 */
		/*	$direction is the direction of the movement											 */
		/*	$info is the current value for moving in the direction indocated by $direction			*/
		/*	$imageOnly indicates if the function is to return the img link only					 */

		function SectorEditImageString($x, $y, $direction, $info, $imageOnly = False)
		{
			$sector_edit_image_string = '<img src="' . PHPRPG_IMG . 'sector_edit_';

			switch ($info)
			{
				case 'N':
					// 'N' is the fixed map border
					if ($direction == 'up' || $direction == 'dn')
					{
						$sector_edit_image_string .= 'vert_no.png" width="7" height="13"';
					}
					else
					{
						$sector_edit_image_string .= 'horz_no.png" width="12" height="8"';
					}
					break;

				case '0':
					// 0 is free move, represented by an arrow
					if ($direction == 'up' || $direction == 'dn')
					{
						$sector_edit_image_string .= $direction . '.png" width="7" height="13"';
					}
					else
					{
						$sector_edit_image_string .= $direction . '.png" width="12" height="8"';
					}
					break;

				default:
					// By default, the difficulty to move is represented by a number (1 - 5)
					if ($direction == 'up' || $direction == 'dn')
					{
						$sector_edit_image_string .= 'vert_' . $info . '.png" width="7" height="13"';
					}
					else
					{
						$sector_edit_image_string .= 'horz_' . $info . '.png" width="12" height="8"';
					}
					break;
			}

			$sector_edit_image_string .= ' border="0" alt="' . $info . '" name="sector_edit_' . $x . '_' . $y . '_' . $direction . '"';

			if (!$imageOnly)
			{
				$sector_edit_image_string .= ' onClick="cycleDirection(' . $x . ',' . $y . ',\'' . $direction . '\')"';
			}

			$sector_edit_image_string .= '>';

			return $sector_edit_image_string;
		}

		echo '
<font color="#eeeeee">' . $map_selected . ' - Location (' . $xpos . ', ' . $ypos . ')</font><br>
<br>
		';

		if (!empty($edit_sector_message))
		{
			echo $edit_sector_message;
		}

		echo '
<form method="post" action="' . PHP_SELF . '?s=' . $s . '" name="sector_edit_form">
<input type="hidden" name="option" value="edit_sector_now"/>
<input type="hidden" name="map_selected" value="' . $map_selected .'"/>
<input type="hidden" name="xpos" value="' . $xpos . '"/>
<input type="hidden" name="ypos" value="' . $ypos . '"/>
		';

		// Select all sectors within range of the selected sector
		$sql = "
			SELECT *
			FROM " . PHPRPG_DB_PREFIX . "_map
			WHERE name = '$map_selected'
			AND (
		";
		if($xpos%2) // $xpos is odd
		{
			$sql .= "
			 (xpos=".($xpos-2)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+1).") OR
			 (xpos=".($xpos-1)." AND ypos BETWEEN ".($ypos-2)." AND ".($ypos+1).") OR
			 (xpos=".($xpos)."	AND ypos BETWEEN ".($ypos-2)." AND ".($ypos+2).") OR
			 (xpos=".($xpos+1)." AND ypos BETWEEN ".($ypos-2)." AND ".($ypos+1).") OR
			 (xpos=".($xpos+2)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+1).")
			";
		}
		else // $xpos is even
		{
			$sql .= "
			 (xpos=".($xpos-2)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+1).") OR
			 (xpos=".($xpos-1)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+2).") OR
			 (xpos=".($xpos)."	AND ypos BETWEEN ".($ypos-2)." AND ".($ypos+2).") OR
			 (xpos=".($xpos+1)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+2).") OR
			 (xpos=".($xpos+2)." AND ypos BETWEEN ".($ypos-1)." AND ".($ypos+1).")
			";
		}
		$sql .= "
			)
			LIMIT 19";
		$db_sectors = mysql_query($sql) or die('Database Error: ' . mysql_error() . '<br>');
		while($db_sector = mysql_fetch_assoc($db_sectors)) {
			$display_sectors[intval($db_sector['xpos'])][intval($db_sector['ypos'])] = array
			(
				'type' => $db_sector['type'],
				'subtype' => $db_sector['subtype'],
				'ul' => $db_sector['move_ul'],
				'dl' => $db_sector['move_dl'],
				'up' => $db_sector['move_up'],
				'dn' => $db_sector['move_dn'],
				'ur' => $db_sector['move_ur'],
				'dr' => $db_sector['move_dr']
			);
			echo '
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_type" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][type]" value="' . $db_sector['type'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_subtype" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][subtype]" value="' . $db_sector['subtype'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_ul" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][ul]" value="' . $db_sector['move_ul'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_dl" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][dl]" value="' . $db_sector['move_dl'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_up" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][up]" value="' . $db_sector['move_up'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_dn" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][dn]" value="' . $db_sector['move_dn'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_ur" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][ur]" value="' . $db_sector['move_ur'] . '"/>
<input type="hidden" id="sector_' . $db_sector['xpos'] . '_' . $db_sector['ypos'] . '_dr" name="sector[' . $db_sector['xpos'] . '][' . $db_sector['ypos'] . '][dr]" value="' . $db_sector['move_dr'] . '"/>
			';
		}
		mysql_free_result($db_sectors);

		echo '
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
		';

#		ksort($display_sectors);

#		foreach ($display_sectors as $x_sector => $y_sector_array)
		for ($x=-2;$x<=2;$x++)
		{
			$x_sector=$xpos+$x;
			echo '
	<td>
			';
#			if ($x_sector != $xpos)
			if ($x_sector%2 == 0) // x is even - add an extra 1/2 square at the top
			{
				echo '
	 <img src="' . PHPRPG_IMG . 'x.png" width="56" height="29" border="0"><br>
				';
			}

#			foreach ($y_sector_array as $y_sector => $values_array)
			for($y=-2;$y<=2;$y++)
			{
				$y_sector=$ypos+$y;

				if($x_sector%2) // x is odd
				{
					$y_delta = 0;
				}
				else // x is even
				{
					$y_delta = 1;
				}
				if(key_exists($x_sector, $display_sectors) && key_exists($y_sector, $display_sectors[$x_sector]))
				{
					echo '
			<table cellpadding="0" cellspacing="0" border="0">
			 <tr>
				<td>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				 ' . SectorEditImageString($x_sector, $y_sector, 'ul', $display_sectors[$x_sector][$y_sector]['ul']) . '<br>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				 ' . SectorEditImageString($x_sector, $y_sector, 'dl', $display_sectors[$x_sector][$y_sector]['dl']) . '<br>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				</td>
				<td>
				 <div align="center">
				 ' . SectorEditImageString($x_sector, $y_sector, 'up', $display_sectors[$x_sector][$y_sector]['up']) . '<br>
				 <img id="tile_' . $x_sector . '_' . $y_sector . '" src="' . PHPRPG_IMG . 'tile_' . $display_sectors[$x_sector][$y_sector]['type'] . $display_sectors[$x_sector][$y_sector]['subtype'] . '.png" width="32" height="32" border="0" alt="(' . ($x_sector) . ', ' . ($y_sector) . ')" name="sector_edit_' . ($x_sector) . '_' . ($y_sector) . '_tile" onClick="focusOn(' . ($x_sector) . ',' . ($y_sector) . ')"><br>
				 ' . SectorEditImageString($x_sector, $y_sector, 'dn', $display_sectors[$x_sector][$y_sector]['dn']) . '<br>
				 </div>
				</td>
				<td>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				 ' . SectorEditImageString($x_sector, $y_sector, 'ur', $display_sectors[$x_sector][$y_sector]['ur']) . '<br>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				 ' . SectorEditImageString($x_sector, $y_sector, 'dr', $display_sectors[$x_sector][$y_sector]['dr']) . '<br>
				 <img src="' . PHPRPG_IMG . 'x.png" width="12" height="14" border="0"><br>
				</td>
			 </tr>
			</table>
					';
				}
				else
				{
					echo '
			<img src="' . PHPRPG_IMG . 'x.png" width="56" height="58" border="0">
					';
				}
			}

#			if($x_sector != $xpos)
			if($x_sector%2 != 0) // x is odd - add another 1/2 square at the bottom
			{
				echo '
	 <img src="' . PHPRPG_IMG . 'x.png" width="56" height="29" border="0"><br>
				';
			}
			echo '
	</td>
			';
		}

		echo '
	</td>
	<td width="400">

	 <div align="right">
	 <font size="2">
	 Functions for Sector (<span id="x_sector_display">' . $xpos . '</span>,<span id="y_sector_display">' . $ypos . '</span>):<br>
	 <a href="javascript:cycleTile()">Change tile at (<span id="x_sector_display">' . $xpos . '</span>,<span id="y_sector_display">' . $ypos . '</span>)</a><br>
	 <a href="javascript:saveFocus()">Save changes and center on (<span id="x_sector_display">' . $xpos . '</span>,<span id="y_sector_display">' . $ypos . '</span>)</a><br>
	 <a href="javascript:cancelFocus();">Cancel changes and center on (<span id="x_sector_display">' . $xpos . '</span>,<span id="y_sector_display">' . $ypos . '</span>)</a><br>
	 <a href="javascript:matchSectors();">Match sectors adjacent to (<span id="x_sector_display">' . $xpos . '</span>,<span id="y_sector_display">' . $ypos . '</span>)</a><br>
	 </font>
	 </div>

	</td>
  </tr>
</table>

<script language="Javascript">
<!--
function focusOn(x, y)
{
	document.sector_edit_form.xpos.value = x;
	document.sector_edit_form.ypos.value = y;
	span_array = document.all.x_sector_display;
	for (i=0;i<span_array.length;i++)
	{
		span_array[i].innerText = x;
	}
	span_array = document.all.y_sector_display;
	for (i=0;i<span_array.length;i++)
	{
		span_array[i].innerText = y;
	}
}

function cycleDirection(x, y, direction)
{
	sector_input = eval("document.sector_edit_form.sector_" + x + "_" + y + "_" + direction);
	sector_image = eval("document.sector_edit_" + x + "_" + y + "_" + direction);

	if (direction == "up" || direction == "dn")
	{
		sector_image_direction = "vert";
	}
	else
	{
		sector_image_direction = "horz";
	}
	switch(sector_input.value)
	{
		case "N": // cannot change a solid map border
			break;

		case "6":
			sector_input.value = 0;
			sector_image.src = "' . PHPRPG_IMG . 'sector_edit_" + direction + ".png";
			break;

		default:
			sector_input.value++;
			sector_image.src = "' . PHPRPG_IMG . 'sector_edit_" + sector_image_direction + "_" + sector_input.value + ".png";
			break;
	}
	sector_image.alt = sector_input.value;
}

function cycleTile()
{
	x = parseInt(document.sector_edit_form.xpos.value);
	y = parseInt(document.sector_edit_form.ypos.value);
	tile_image = eval("document.tile_" + x + "_" + y);
	tile_type = eval("document.sector_edit_form.sector_" + x + "_" + y + "_type");
	tile_subtype = eval("document.sector_edit_form.sector_" + x + "_" + y + "_subtype");
	switch(tile_subtype.value)
	{
		case "":
			tile_subtype.value = "1";
			break;
		case "20":
			tile_subtype.value = "";
			break;
		default:
			tile_subtype.value = parseInt(tile_subtype.value) + 1;
			break;
	}
	tile_image.src = "' . PHPRPG_IMG . 'tile_" + tile_type.value + tile_subtype.value + ".png";
}

function saveFocus()
{
	// xpos and ypos are set to new coords anyway, so just submit the form
	document.sector_edit_form.submit();
}

function cancelFocus()
{
	// Just change the location - changed info will not be submitted.  Pick up new x and y coords from the form
	location.href="' . PHP_SELF . '?s=' . $s . '&map_selected=' . $map_selected . '&option=edit_sector&xpos="  + document.sector_edit_form.xpos.value + "&ypos=" + document.sector_edit_form.ypos.value;
}

function matchSectors()
{
	x = parseInt(document.sector_edit_form.xpos.value);
	y = parseInt(document.sector_edit_form.ypos.value);
	// Find xpos, ypos of each hex around the current - if available, set the direction values the same
	direction_array = new Array ("ul","dl","up","dn","ur","dr");
	for (var direction in direction_array)
	{
		sector_input = eval("document.sector_edit_form.sector_" + x + "_" + y + "_" + direction_array[direction]);
		sector_image = eval("document.sector_edit_" + x + "_" + y + "_" + direction_array[direction]);
		switch(direction_array[direction])
		{
			case "ul":
				sector_image_direction = "horz";
				adjacent_direction = "dr";
				adjacent_x = x-1;
				adjacent_y = y-1;
				if (x % 2 == 0) // x is even
				{
					adjacent_y++;
				}
				break;
			case "dl":
				sector_image_direction = "horz";
				adjacent_direction = "ur";
				adjacent_x = x-1;
				adjacent_y = y;
				if (x % 2 == 0) // x is even
				{
					adjacent_y++;
				}
				break;
			case "up":
				sector_image_direction = "vert";
				adjacent_direction = "dn";
				adjacent_x = x;
				adjacent_y = y-1;
				break;
			case "dn":
				sector_image_direction = "vert";
				adjacent_direction = "up";
				adjacent_x = x;
				adjacent_y = y+1;
				break;
			case "ur":
				sector_image_direction = "horz";
				adjacent_direction = "dl";
				adjacent_x = x+1;
				adjacent_y = y-1;
				if (x % 2 == 0) // x is even
				{
					adjacent_y++;
				}
				break;
			case "dr":
				sector_image_direction = "horz";
				adjacent_direction = "ul";
				adjacent_x = x+1;
				adjacent_y = y;
				if (x % 2 == 0) // x is even
				{
					adjacent_y++;
				}
				break;
			default:
				// lets screw things up "to avoid problems"
				adjacent_direction = "";
				adjacent_x = "";
				adjacent_y = "";
				break;
		}
		adjacent_input = eval("document.sector_edit_form.sector_" + adjacent_x + "_" + adjacent_y + "_" + adjacent_direction);
		adjacent_image = eval("document.sector_edit_" + adjacent_x + "_" + adjacent_y + "_" + adjacent_direction);
		if(sector_input && adjacent_input) // have to figure out if ther is an existing object / input
		{
			sector_input.value = adjacent_input.value;
			switch(sector_input.value)
			{
				case "0":
					sector_image.src = "' . PHPRPG_IMG . 'sector_edit_" + direction_array[direction] + ".png";
					break;

				default:
					sector_image.src = "' . PHPRPG_IMG . 'sector_edit_" + sector_image_direction + "_" + sector_input.value + ".png";
					break;
			}
		}
	}
}
// -->
</script>

<!-- redundant now the functions are in
<div align="right"><input type="submit" value="Modify Sector" class="inputbutton"></div>
-->

</form>
		';
		break;

	case 'edit_map_now':
		echo '
		<script language="Javascript">
		<!--
		function MapClick(map_plus, x, y)
		{
			edit_win = window.open("' . PHP_SELF . '?s=' . $s . '&option=edit_sector&map_selected=" + map_plus + "&xpos=" + x + "&ypos=" + y, "Editing_Window", "height=500, width=620, resizable=yes, scrollbars=yes");
			edit_win.focus();
		}
		-->
		</script>';
		echo '
<font color="#eeeeee">Map ' . $map_selected . '</font><br>
		';
		// Get map size
		$result = mysql_query("SELECT MAX(xpos) AS xmax, MAX(ypos) AS ymax FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map_selected'") or die('Database Error: ' . mysql_error() . '<br>');
		$result_array = mysql_fetch_array($result);
		$xmax = $result_array['xmax'] + 1;
		$ymax = $result_array['ymax'] + 1;

		$max_exec = ini_get('max_execution_time');
		if ($max_exec < ($xmax * $ymax))
		{
			set_time_limit($xmax * $ymax);
		}

		echo '
<br>
Size: ' . $xmax . ' x ' . $ymax . '<br>
<br>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
		';

		$map_selected_plus = str_replace (' ', '+', $map_selected);

		for ($x = 0; $x < $xmax; $x++)
		{
			echo '
	<td>
			';
			if ($x % 2 == 0)
			{
				echo '
	 <img src="' . PHPRPG_IMG . 'x.png" width="16" height="8" border="0"><br>
				';
			}
			for ($y = 0; $y < $ymax; $y++)
			{
			$result = mysql_query("SELECT move_up, move_ur, move_dr, move_dn, move_dl, move_ul, type, subtype, region FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map_selected' AND xpos=$x AND ypos=$y") or die('Database Error: ' . mysql_error() . '<br>');
			$map_row = mysql_fetch_array($result);

			$sector_string = '<a onClick="MapClick(\'' . $map_selected_plus . '\', ' . $x . ',' . $y . ');"><img src="' . PHPRPG_IMG . 'tile_' . $map_row['type'] . $map_row['subtype'] . '.png" width="16" height="16" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';

			echo $sector_string;

			mysql_free_result($result);
			}
			if ($x % 2 == 1)
			{
				echo '
	 <img src="' . PHPRPG_IMG . 'x.png" width="16" height="8" border="0"><br>
				';
			}
			echo '
	</td>
			';
		}

		echo '
  </tr>
</table>
		';
		break;

	case 'edit_map':
		echo '
<font color="#eeeeee">Edit Map</font><br>
		';
		$result = mysql_query("SELECT DISTINCT name FROM " . PHPRPG_DB_PREFIX . "_map ORDER BY name");
		if (mysql_num_rows($result) != 0)
		{
			echo '
<form method="post" action="' . PHP_SELF . '?s=' . $s . '">
<input type="hidden" name="option" value="edit_map_now">
<table cellpadding="0" cellspacing="4" border="0">
			';
			while ($map = mysql_fetch_array($result))
			{
				echo '
  <tr>
	<td>
	 <input type="radio" name="map_selected" value="' . $map['name'] . '"></td><td>' . $map['name'] . '
	</td>
  </tr>
				';
			}
		echo '
  <tr>
	<td colspan="2">
	 <div align="right"><input type="submit" value="Edit Map" class="inputbutton"></div>
	</td>
  </tr>
</table>
</form>
		';
		}
		else
		{
			echo '
Error: No map entry was found!<br>
			';
		}
		break;

	case 'create_map_now':
		echo '
<font color="#eeeeee">Creating Map</font><br>
		';
		$result = mysql_query("SELECT DISTINCT name FROM " . PHPRPG_DB_PREFIX . "_map WHERE name = '$map_name'");

		if (mysql_num_rows($result) == 0) // Name does not exist yet
		{
			// We give it one second per square we need to make.
			// Larger maps will time out otherwise.
			$max_exec = ini_get("max_execution_time");
			if ($max_exec < ($map_width * $map_height))
			{
				set_time_limit($map_width * $map_height);
			}

			for ($x = 0; $x < $map_width; $x++)
			{
				for ($y = 0; $y < $map_height; $y++)
				{
					$move_up = '0'; $move_ur = '0'; $move_dr = '0'; $move_dn = '0'; $move_dl = '0'; $move_ul = '0';
					$type = 'realm';  // Damn...  hard coded again :(
					$subtype = '';

					// Default map borders
					if ($x == 0)
					{
						$move_ul = 'N'; $move_dl = 'N';
					}
					if ($x == $map_width - 1)
					{
						$move_ur = 'N'; $move_dr = 'N';
					}

					// More complicated when $y is at the border
					if ($y == 0)
					{
						$move_up = 'N';
					}
					if ($y == $map_width - 1)
					{
						$move_dn = 'N';
					}
					if (($y == 0) && ($x % 2 == 1))
					{
						$move_ul = 'N'; $move_ur = 'N';
					}
					if (($y == $map_height - 1) && ($x % 2 == 0))
					{
						$move_dl = 'N'; $move_dr = 'N';
					}
					$result = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_map VALUES ('$map_name', $x, $y, '$move_up', '$move_ur', '$move_dr', '$move_dn', '$move_dl', '$move_ul', '$type', '$subtype', '')") or die ('Database Error: ' . mysql_error() . '<br>');
				}
			}
			echo '
Map "<font color="#eeeeee">' . $map_name . '</font>" has been generated successfully!<br>
			';
		}
		else
		{
			echo '
Error: A map with the same name was found!<br>
			';
		}
		break;

	case 'create_map':
		echo '
<font color="#eeeeee">Create New Map</font><br>
<form method="post" action="' . PHP_SELF . '?s=' . $s . '">
<input type="hidden" name="option" value="create_map_now">
<table cellpadding="0" cellspacing="4" border="0">
  <tr>
	<td>Map Name:</td>
	<td><input type="text" name="map_name" size="20" maxlength="20" class="input"></td>
  </tr>
  <tr>
	<td>Map Width (x):</td>
	<td><input type="text" name="map_width" value="40" size="4" maxlength="2" class="input"></td>
  </tr>
  <tr>
	<td>Map Height (y):</td>
	<td><input type="text" name="map_height" value="20" size="4" maxlength="2" class="input"></td>
  </tr>
  <tr>
	<td colspan="2"><div align="right"><input type="submit" value="Create Map" class="inputbutton"></div></td>
  </tr>
</table>
</form>
		';
		break;

	default:
		echo '
<font color="#eeeeee">Options</font><br>
<a href="' . PHP_SELF . '?s=' . $s . '&option=create_map">Create New Map</a><br>
<a href="' . PHP_SELF . '?s=' . $s . '&option=edit_map">Edit Map</a><br>
<a href="' . PHP_SELF . '?s=' . $s . '&option=delete_map">Delete Map</a><br>
		';
}

echo '
<br><font size="1">
';

if (!empty($option))
{
	if ($option == 'edit_sector' || $option == 'edit_sector_now' )
	{
		echo '
<a href="javascript:self.close()">[Close Window]</a> (this will cancel changes made since last save)
		';
	}
	elseif ($option == 'edit_map_now')
	{
		echo '
<a href="' . PHP_SELF . '?s=' . $s . '&option=edit_map_now&map_selected=' . $map_selected_plus . '">[Refresh]</a> <a href="' . PHP_SELF . '?s=' . $s . '">[Map Editor Index]</a>
		';
	}
	else
	{
		echo '
<a href="javascript:history.go(-1)">[Back]</a> <a href="' . PHP_SELF . '?s=' . $s . '">[Map Editor Index]</a>
		';
	}
}
else
{

	echo '
<a href="local.php?s=' . $s . '">[Local View]</a><br>
	';

}

echo '</font>';

OpenTable('close');

require('template_footer.inc.php');

?>