<?php
/******************************************************************************/
/*																			*/
/* move.php - Handles general movement (over the map)						*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 18 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_delay.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$ref = Retrieve_var('ref');
$move = Retrieve_var('move');
$current = Retrieve_var('current');
$x = Retrieve_var('x');
$y = Retrieve_var('y');

$xpos = $char['map_xpos'];
$ypos = $char['map_ypos'];
$map = $char['map_name'];

// y positions differ on column x
$x_modulus = $xpos % 2;
if ($x_modulus == 0)
{
	$ynew = $ypos;
}
else
{
	$ynew = $ypos - 1;
}

// Fetch movement information of current grid
DbConnect();

$result = mysql_query("SELECT move_up, move_ur, move_dr, move_dn, move_dl, move_ul FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$xpos' AND ypos='$ypos'");
$map_row = mysql_fetch_array($result);
mysql_free_result($result);

if (!empty($move) && ($current == $map) && ($x == $xpos ) && ($y == $ypos) && !empty($ref))
{
	if (($map_row["move_$move"] != 'N') && ($map_row["move_$move"] != '6') && ($map_row["move_$move"] != ''))
	{
		// Retrieve base statistics relevant to movements
		$result = mysql_query("SELECT STR, SPD, CW, CC FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$user_id LIMIT 1");
		list($STR, $SPD, $CW, $CC) = mysql_fetch_row($result);

		// Check if CC is overloaded
		$CC_overload = $CW - $CC;
		if ($CC_overload < 0)
		{
			$CC_overload = 0;
		}

		// No stamina charge if admin
		if (empty($char['admin_level']))
		{
			// New stamina
			$char['STM'] = round($char['STM'] - (12 / $STR) * $map_row["move_$move"] - ($CC_overload / 4));
		}

		// Check if remaining stamina allows the movement
		if ($char['STM'] < 0)
		{
			// Movement is not possible -> DO NOT update new stamina -> return to file
			if (PHPRPG_DEBUG_AUTOREDIRECT)
			{
				echo 'Not enough stamina to move!<br>';
				echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '&reason=stamina">Click to continue</a>';
			} else {
				header("Location: $ref?s=$s&reason=stamina");
			}
			exit;
		}

		// New Delay
		$delay = $user_time + round($CC_overload / ($SPD + $STR) * 1.5);

		// Check if it is a valid move
		$x = $xpos; $y = $ypos;
		switch ($move) {

			case 'up':
				$y = $ypos - 1;
				break;
			case 'ur':
				$x = $xpos + 1; $y = $ynew;
				break;
			case 'dr':
				$x = $xpos + 1; $y = $ynew + 1;
				break;
			case 'dn':
				$y = $ypos + 1;
				break;
			case 'dl':
				$x = $xpos - 1; $y = $ynew +  1;
				break;
			case 'ul':
				$x = $xpos - 1; $y = $ynew;
				break;
			default:
				// Unexpected outcome -> Refresh with old values
		}

		// Clear any player search.
		if (isset($_SESSION['search']))
		{
			unset($_SESSION['search']);
		}
		if (isset($_SESSION['page']))
		{
			unset($_SESSION['page']);
		}
		if (isset($_SESSION['show_all']))
		{
			unset($_SESSION['show_all']);
		}

		// Update new position
		$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM=" . $char['STM'] . ", map_xpos=$x, map_ypos=$y, delay=$delay, delay_reason='move_penalty' WHERE user_id=$user_id");
		if (mysql_error()) die(mysql_error());
	}


	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Move complete!<br>';
		echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '">Click to continue</a>';
	} else {
		header("Location: $ref?s=$s");
	}
}
else
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Problem with the move!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
}


?>