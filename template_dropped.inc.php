<?php
/******************************************************************************/
/*																			 */
/* template_dropped.inc.php - Displays Items Dropped On the Ground			 */
/*																			 */
/******************************************************************************/
/*																			 */
/* Requirements: PHP, MySQL and web-browser									 */
/*																			 */
/* Author: Timothy TS Chung													 */
/*		<ttschung@users.sourceforge.net>									 */
/*																			 */
/* Created: 22 April 2002													 */
/*																			 */
/* Copyright (c) 2001-2002 Timothy TS Chung									 */
/*																			 */
/* This file is part of phpRPG (http://phpRPG.org/)							 */
/*																			 */
/* phpRPG is free software; you can redistribute it and/or modify			 */
/* it under the terms of the GNU General Public License as published by		 */
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										 */
/*																			 */
/* This program is distributed in the hope that it will be useful,			 */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			 */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								 */
/*																			 */
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				 */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			 */
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

DbConnect();

$result_items = mysql_query("SELECT id, img, ident FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=0 AND map_name='" . $char['map_name'] . "' AND map_xpos=" . $char['map_xpos'] . " AND map_ypos=" . $char['map_ypos'] . "  ORDER BY id");
if (mysql_num_rows($result_items) > 0)
{
	echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="5" border="0"><br>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td></td>
<td><img src="' . PHPRPG_IMG . 'quote_ul.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_tp.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_ur.png" width="15" height="7" border="0" alt=""></td>
</tr>
<tr>
<td><img src="' . PHPRPG_IMG . 'finger.png" width="30" height="30" border="0" alt="Take Item"></td>
<td background="' . PHPRPG_IMG . 'quote_lt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
	';

	while ($items = mysql_fetch_array($result_items))
	{
		echo '
<td><a href="item.php?s=' . $s . '&option=take&id=' . $items['id'] . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'item_' . $items['img'] . '.png" width="30" height="30" border="0" alt="' . $items['ident'] . '"></td>
		';
	}

	echo '
</tr>
</table>
</td>
<td background="' . PHPRPG_IMG . 'quote_rt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
</tr>
<tr>
<td></td>
<td><img src="' . PHPRPG_IMG . 'quote_dl.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_bt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_dr.png" width="15" height="7" border="0" alt=""></td>
</tr>
</table>
	';
}


?>