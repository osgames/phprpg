<?php
/******************************************************************************/
/*																			*/
/* template_header.inc.php - HTML Header									 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 15 Mrch 2002													 */
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

if (!isset($s)) $s = '';

// Get game time-dependent background colours

$html_background_80 = BackgroundColour(80);
$html_background_75 = BackgroundColour(75);
$html_background_70 = BackgroundColour(70);
$html_background_65 = BackgroundColour(65);
$html_background_60 = BackgroundColour(60);
$html_background_55 = BackgroundColour(55);
$html_background_50 = BackgroundColour(50);
$html_background_45 = BackgroundColour(45);
$html_background_40 = BackgroundColour(40);
$html_background_35 = BackgroundColour(35);

// Ensures content is dynamic, uncached
header("Expires: Mon, 6 Dec 1977 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

echo '
<html>
<head>
<title>ph&#39;@ntasy :: a phpRPG production</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="Description" content="phpRPG is an web browser based multiplayer fantasy roleplaying game. Enter and experience the realm of fantasy RPG!">
<meta name="Author" content="Timothy TS Chung">
<meta name="Keywords" content="PHANTASY, FANTASY, PHPRPG, PHP, RPG, MYSQL, ROLE, PLAYING, GAME, MULTI, PLAYER, ONLINE, WEB, BROWSER, OPEN, SOURCE, BETA, FREE, PROJECT, MULTIPLAYER, SPELL, CASTING, FIGHTER, MAGE, WIZARD, BARD, PSIONIC, ALCHEMIST, PRIEST, SAGE, VAMPIRE, UNDEAD, SPIRIT, SOUL">

<style type="text/css">
<!--
body
{
	font: 12px arial, helvetica; color: #bbbbbb; background: ' . $html_background_35 . '; text-align: justify; margin: 0px;
	scrollbar-face-color : #212121;
	scrollbar-shadow-color : #c1c1c1;
	scrollbar-highlight-color : #c1c1c1;
	scrollbar-3dlight-color : #212121;
	scrollbar-darkshadow-color : #212121;
	scrollbar-track-color: #212121;
	scrollbar-arrow-color : #c1c1c1;
}
table
{
	font: 12px arial, helvetica; color: #bbbbbb; text-align: justify; margin: 0px;
}
a:link, A:visited, A:active { color: #6181b1; text-decoration: none; }
a:hover { color: #b1d1f1; text-decoration: none; }
.input
{
	font: 14px bold; color: #c1c1c1; background: #212121;
	border: 1px #a1a1a1 solid;
	margin-top: 1px; margin-bottom: 1px;
	width: 100px; height: 20px;
	clip: rect();
}
.inputtext
{
	font: 14px bold; color: #c1c1c1; background: #212121;
	border: 1px #a1a1a1 solid;
	margin-top: 1px; margin-bottom: 1px;
}
.inputbutton
{
	font: 9px bold; color: #d1d1d1; background: #212121;
	border: 1px #d1d1d1 solid;
	margin-top: 1px; margin-bottom: 1px;
	height: 20px;
	clip: rect();
}
.menu {
  background-color: #40a0c0;
  border-style: solid;
  border-width: 2px;
  position: absolute;
  left: 0px;
  top: 0px;
  display:none;
}

.menuTitle {
  color: #ffffff;
  display: block;
  font-family: MS Sans Serif, Arial, Tahoma,sans-serif;
  font-size: 10pt;
  font-weight: bold;
  padding: 2px 12px 2px 8px;
  text-decoration: none;
}

a.menuItem {
  color: #ffffff;
  cursor: default;
  display: block;
  font-family: MS Sans Serif, Arial, Tahoma,sans-serif;
  font-size: 8pt;
  font-weight: bold;
  padding: 2px 12px 2px 8px;
  text-decoration: none;
}

a.menuItem:hover {
  background-color: #004060;
  color: #ffff00;
}

a.menuItem:visited {
  color: #ffffff;
}

// -->
</style>
<base href="' . PHPRPG_BASE . '">
<link rel="SHORTCUT ICON" href="' . PHPRPG_BASE . 'favicon.ico">


<script language="Javascript">
<!--

function contains(a, b) {

  // Return true if node a contains node b.

  while (b.parentNode)
    if ((b = b.parentNode) == a)
      return true;
  return false;
}

function parseEvent(evt) {
	evt = (evt) ? evt : ((event) ? event : null);
	if(evt && !evt.srcElement) evt.srcElement = evt.target;
	if (window.event)
	{
		evt.X = evt.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		evt.Y = evt.clientY + document.documentElement.scrollTop + document.body.scrollTop;
	} else {
		evt.X = evt.clientX + window.scrollX;
		evt.Y = evt.clientY + window.scrollY;
	}
	return evt;
}

function eventFalse(evt) {
	switch(evt.type) {
		case "submit":
		case "contextmenu":
			return false;
			break;
		default:
			return true;
			break;
	}
}

function modify_menu(se)
{
	menu = se.getAttribute("menu");

	switch(menu)
	{
	case "friend":
		Gen_friend_menu(se);
		break;
	case "items":
	case "battle":
	case "magic":
	default:
	}
}

function SetText(span, text)
{
	while(span.hasChildNodes()) {
		span.removeChild(span.firstChild);
	}
	span.appendChild(document.createTextNode(text));
}

function actionClick(evt, menu, character) {
	hideMenus(evt);

	evt = parseEvent(evt);

	se = evt.srcElement;

	menu = se.getAttribute("menu");
	character = se.getAttribute("pname");
	friend = se.getAttribute("friend");

	if (!menu)
		return true;

	modify_menu(se);

	if(evt.type == "contextmenu") {
		oElem = document.getElementById("cmenu_"+menu);
		oElem.style.left = evt.X;
		oElem.style.top = evt.Y;
		SetText(document.getElementById("cmenu_"+menu+"_title"), menu+" menu for "+character);
		oElem.style.display = "block";
		evt.cancelBubble = true;
		return eventFalse(evt);
	}
}

function hideMenus(evt) {

	evt = parseEvent(evt);

	blockClick = false;
	oDivs = document.getElementsByTagName("DIV");
	for(i=0;i<oDivs.length;i++) {
		if(oDivs[i].className == "menu" && oDivs[i].style.display == "block") {
			if (!contains(oDivs[i], evt.srcElement))
			{
				oDivs[i].style.display = "none";
				blockClick = true;
			}
		}
	}
	return !blockClick;
}

function Gen_friend_menu(se)
{
	friend = se.getAttribute("friend");
	userid = se.getAttribute("userid");
	pname = se.getAttribute("pname");

	addto = "Add " + pname + " to ";
	remfr = "Remove " + pname + " from ";

	se = document.getElementById("phprpg");

	s = se.getAttribute("session");
	phpself = se.getAttribute("self");

	for (ar = 0; ar < 2; ar++)
	{
		option = (friend.charAt(ar) == "1") ? "remove" : "add";
		if (ar == 0)
		{
			document.getElementById("cmenu_friend_fr_href").setAttribute("href", "' . PHPRPG_BASE . '" + phpself + "?s=" + s + "&option=" + option + "&type=friends" + "&target=" + userid)
			SetText(document.getElementById("cmenu_friend_fr"), (friend.charAt(0) == "1") ? remfr : addto);
		} else if (ar == 1) {
			document.getElementById("cmenu_friend_en_href").setAttribute("href", "' . PHPRPG_BASE . '" + phpself + "?s=" + s + "&option=" + option + "&type=enemies" + "&target=" + userid)
			SetText(document.getElementById("cmenu_friend_en"), (friend.charAt(1) == "1") ? remfr : addto);
		}
	}
}
-->
</script>

</head>

<body bgcolor="' . $html_background_35 . '" text="#bbbbbb" link="#6181b1" vlink="#6181b1" alink="#6181b1" onclick="return hideMenus(event);" oncontextmenu="return actionClick(event)">

<div id="phprpg" session="' . $s . '" self="' . PHP_SELF . '"></div>

<div id="cmenu_friend" class="menu">
<span id="cmenu_friend_title" class="menuTitle">&nbsp;</span>
<a id="cmenu_friend_fr_href" class="menuItem" href="#"><span id="cmenu_friend_fr"></span>Friends</a>
<a id="cmenu_friend_en_href" class="menuItem" href="#"><span id="cmenu_friend_en"></span>Enemies</a>
</div>

<div id="cmenu_battle" class="menu">
<span id="cmenu_battle_title" class="menuTitle">&nbsp;</span>
<a class="menuItem" href="#">Attack Player</a>
<a class="menuItem" href="#">Heal Player</a>
<a class="menuItem" href="#">Use item on Player</a>
<a class="menuItem" href="#">Give item to Player</a>
</div>

<div id="cmenu_magic" class="menu">
<span id="cmenu_magic_title" class="menuTitle">&nbsp;</span>
<a class="menuItem" href="#">Cast Spell</a>
<a class="menuItem" href="#">Charge wand with Spell</a>
<a class="menuItem" href="#">Memorize Spell</a>
</div>

<div id="cmenu_items" class="menu">
<span id="cmenu_items_title" class="menuTitle">&nbsp;</span>
<a class="menuItem" href="#">Equip Item</a>
<a class="menuItem" href="#">Use Item</a>
<a class="menuItem" href="#">Throw Item</a>
<a class="menuItem" href="#">Drop Item</a>
</div>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr><td bgcolor="' . $html_background_80 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_75 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_70 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_65 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_60 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_55 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_50 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_45 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_40 . '" width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="4" border="0" alt=""></td></tr>
<tr><td bgcolor="' . $html_background_35 . '" width="100%">
<div align="center">
';

if (PHPRPG_DEBUG)
{
	$start_time = StartTiming();
}


?>