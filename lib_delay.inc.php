<?php
/******************************************************************************/
/*																			*/
/* lib_delay.inc.php - Manages Task and Event Delays						 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 17 April 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Attempt to access an included file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

require_once('lib_session.inc.php');

if ($user_time < $char['delay'])
{
	// Delay exists -> send to local.php to provide reason if ref doesn't exist

	if (empty($ref)) $ref = 'local.php';
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Delay exists!<br>';
		echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '&reason=delay">Click to continue</a>';
	} else {
		header("Location: $ref?s=$s&reason=delay");
	}
	exit;
}

// Otherwise proceed as usual


?>