<?php

/******************************************************************************/
/*																			  */
/* action.php - Handles Inter-player Interactions							  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 21 April 2002													  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/



error_reporting (E_ALL);


require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_interactions.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$option = Retrieve_var('option');
$id = Retrieve_var('id');
$ref = Retrieve_var('ref');

if (!empty($option) && !empty($ref))
{
	include('lib_delay.inc.php');

	srand((double) microtime() * 1000000);

	switch ($option)
	{
	// Attack player
	case 'attack':
		// Calls the local attact function once
		AttackLocal($user_id, $id, 'attack');
		AttackLocal($id, $user_id, 'counter');
		// .... ugh....
		break;
	}
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Action successful.<BR>';
		echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '">Click to continue</a>';
	} else {
		header("Location: $ref?s=$s");
	}
} else {
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'No referred file!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
}


/*																			*/
/* AttackLocal($attackerId, $targetId, $attackType) - Processes local attacks */
/*										$attackerId - the attacker's user_id */
/*										$targetId - the target's user_id	*/
/*										$attackType - they type of attack	*/

function AttackLocal($attackerId, $targetId, $attackType)
{
	// Fetch info of the attacker
	$result_attacker = mysql_query("SELECT name, HP, HP_MAX, STM, EXP, STR, DEX, SPD, CW, CC, map_name, map_xpos, map_ypos, race FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$attackerId LIMIT 1");
	if (mysql_num_rows($result_attacker) != 1)
	{
		return;
	}
	else
	{
		$attacker = mysql_fetch_array($result_attacker);
	}

	// Fetch info of the target player

	//*** TEMPORARY GAME SYSTEM
	//***	$result_target = mysql_query("SELECT name, HP, DEX FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$targetId AND map_name='" . $attacker['map_name'] . "' AND map_xpos=" . $attacker['map_xpos'] . " AND map_ypos=" . $attacker['map_ypos'] . " LIMIT 1");
	$result_target = mysql_query("SELECT name, HP, HP_MAX, EXP, DEX, map_name, map_xpos, map_ypos FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$targetId AND map_name='" . $attacker['map_name'] . "' AND map_xpos=" . $attacker['map_xpos'] . " AND map_ypos=" . $attacker['map_ypos'] . " LIMIT 1");
	//*** END TEMPORARY GAME SYSTEM

	if (mysql_num_rows($result_target) != 1)
	{
		if ($attackType != 'counter')
		{
			$result = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_battles SET type='$attackType', attacker_id=$attackerId, target_id=0, map_name='" . $attacker['map_name'] . "', map_xpos=" . $attacker['map_xpos'] . ", map_ypos=" . $attacker['map_ypos'] . ", contents='Your attack was failed as your target is not in sight!', post_time=" . time());
		}
	}
	else
	{
		if ($attackType != 'counter')
		{
			Add_attacker($targetId, $attackerId);
		}

		$target = mysql_fetch_array($result_target);

		// Get weapons data of attacker - Race dependent
		switch ($attacker['race'])
		{
			case 'Soul':
				$random_weapon = rand(1, 2);

				// Select weapons
				$result_used_items_right = mysql_query("SELECT ref_id, ident, indx, deviation, mode, weight FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$attackerId AND used='weapon_right' LIMIT 1");
				$result_used_items_left = mysql_query("SELECT ref_id, ident, indx, deviation, mode, weight FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$attackerId AND used='weapon_left' LIMIT 1");

				if (mysql_num_rows($result_used_items_right) == 1 && ($random_weapon == 1 || mysql_num_rows($result_used_items_left) == 0))
				{
					// if $random_weapon == 1 or when the other hand is empty
					$result_used_items = $result_used_items_right;
				}
				elseif (mysql_num_rows($result_used_items_left) == 1 && ($random_weapon == 2 || mysql_num_rows($result_used_items_right) == 0))
				{
					// if $random_weapon == 2 or when the other hand is empty
					$result_used_items = $result_used_items_left;
				}
				break;
		}

		if (empty($result_used_items))
		{
			// No weapons armed! -> using bare hands - depends on user skills and strength
			$weapon = array(
				'ref_id' => '0',
				'ident' => '',
				'indx' => ($attacker['STR'] - 2),
				'deviation' => 1,
				'mode' => 'punches',
				'weight' => 0
			);
			$damage = rand($weapon['indx'] - $weapon['deviation'], $weapon['indx'] + $weapon['deviation']);
			$result = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_battles SET type='$attackType', attacker_id=$attackerId, target_id=$targetId, map_name='" . $attacker['map_name'] . "', map_xpos=" . $attacker['map_xpos'] . ", map_ypos=" . $attacker['map_ypos'] . ", contents='" . $attacker['name'] . ' ' . $weapon['mode'] . ' ' . $target['name'] . ' inflicting ' . $damage . " units of damage!', post_time=" . time());
		}
		else
		{
			$weapon = mysql_fetch_array($result_used_items);
			$damage = rand($weapon['indx'] - $weapon['deviation'], $weapon['indx'] + $weapon['deviation']);
			$result = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_battles SET type='$attackType', attacker_id=$attackerId, target_id=$targetId, map_name='" . $attacker['map_name'] . "', map_xpos=" . $attacker['map_xpos'] . ", map_ypos=" . $attacker['map_ypos'] . ", contents='" . $attacker['name'] . ' ' . $weapon['mode'] . ' ' . $weapon['ident'] . ' at ' . $target['name'] . ' inflicting ' . $damage . " units of damage!', post_time=" . time());
		}

		// Modify player stats
		$target['HP'] = $target['HP'] - $damage;
		if ($target['HP'] < 0)
		{
			$target['HP'] = 0;
			$result = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_battles SET attacker_id=$attackerId, target_id=$targetId, map_name='" . $attacker['map_name'] . "', map_xpos=" . $attacker['map_xpos'] . ", map_ypos=" . $attacker['map_ypos'] . ", contents='<font color=\"#eeeeee\">" . $target['name'] . " has been knocked unconscious!</font>', post_time=" . time());

//***			You cannot move as you are dead. You need to find someone to resurrect you or wait

			//*** TEMPORARY GAME SYSTEM
			//*** player is dead! -> drop items, heal to max and send to (4,4), attacker gains xp!

			$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET user_id=0, map_name='" . $target['map_name'] . "', map_xpos=" . $target['map_xpos'] . ", map_ypos=" . $target['map_ypos'] . ", used='' WHERE user_id=$targetId");

			$result_target = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET HP=" . $target['HP_MAX'] . ", CC=0, map_name='Realm of Souls', map_xpos=4, map_ypos=4 WHERE user_id=$targetId");

			if ($attackerId != 10001 || $attackerId != 10002 || $attackerId != 10003) // Target One/Two/Three - always remain weakest (doesn't go up)
			{
				$result_attacker = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET HP_MAX=" . ($attacker['HP_MAX'] + 10) . ", EXP=" . ($attacker['EXP'] + round($target['EXP'] / 10) + 100) . " WHERE user_id=$attackerId");
			}

			//*** END TEMPORARY GAME SYSTEM

		}
		else
		{
			// Update player stats
			$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET HP=" . $target['HP'] . " WHERE user_id=$targetId");
		}
		// ** what about times YOU MISSES!!
	}
}
?>