<?php
/******************************************************************************/
/*																			*/
/* logout.php - Terminate current session and log out						*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 17 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_template.inc.php');
require_once('lib_session.inc.php');
require_once('lib_events.inc.php');

DbConnect();

if (empty($_SESSION['user_id']))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'No session to log out from!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

// Set last_active = '0' to indicate logged out
$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET last_active='0' WHERE user_id=$user_id LIMIT 1");

// Destroys current session and variables
session_unset();
session_destroy();

require_once('template_header.inc.php');

OpenTable('title', '500');
echo 'p h &#39; @ n t a s y';

OpenTable('content');
echo '
<div align="justify">
You have successfully logged out...<br>
<br>
<a href="index.php">Return to login</a>
</div>
';
OpenTable('close');

require_once('template_footer.inc.php');

?>