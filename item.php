<?php
/******************************************************************************/
/*																			  */
/* item.php - Handles Items Manipulation									  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 21 April 2002													  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$ref = Retrieve_var('ref');
$option = Retrieve_var('option');
$sub = Retrieve_var('sub');
$id = Retrieve_var('id');

if (!empty($option) && !empty($ref))
{
	switch ($option)
	{

	// Equip Item - Race dependent
	case 'equip':
		// 'Tis easier to drop than take... :)
		include('lib_delay.inc.php');

		// Get info of current item
		$result_items = mysql_query("SELECT weight, curse FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$id AND user_id=$user_id LIMIT 1");
		list($weight, $curse) = mysql_fetch_row($result_items);

		switch ($char['race'])
		{
			case 'Soul':
				switch ($sub)
				{
					case 'weapon_right':
						// Check weapon space
						$result_used_items = mysql_query("SELECT id FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$user_id AND used='weapon_right' LIMIT 1");
						if (list($used_item_id) = mysql_fetch_row($result_used_items))
						{
							// Remove occupied item
							$update_used_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET used='' WHERE id=$used_item_id AND user_id=$user_id AND used='weapon_right' LIMIT 1");
						}
						// Arm weapon!! :)
						$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET used='weapon_right' WHERE id=$id AND user_id=$user_id LIMIT 1");

						// *** Execute curse here...!! ***

						// Equip penalty
						$update_users = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET delay=($user_time + $weight), delay_reason='arm_penalty' WHERE user_id=$user_id LIMIT 1");
						break;
					case 'weapon_left':
						$result_used_items = mysql_query("SELECT id FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$user_id AND used='weapon_left' LIMIT 1");
						if (list($used_item_id) = mysql_fetch_row($result_used_items))
						{
							$update_used_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET used='' WHERE id=$used_item_id AND user_id=$user_id AND used='weapon_left' LIMIT 1");
						}
						$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET used='weapon_left' WHERE id=$id AND user_id=$user_id LIMIT 1");
						// *** Execute curse here...!! ***
						$update_users = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET delay=($user_time + $weight), delay_reason='arm_penalty' WHERE user_id=$user_id LIMIT 1");
						break;
				}
				break;
		}
		break;

	// Unequip Item
	case 'unequip':
		// 'Tis easier to drop than take... :)
		include('lib_delay.inc.php');

		// Get info of current item
		$result_items = mysql_query("SELECT weight, curse FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$id AND user_id=$user_id LIMIT 1");
		list($weight, $curse) = mysql_fetch_row($result_items);

		// *** If no curse, proceed... ***

		// Remove item
		$update_used_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET used='' WHERE id=$id AND user_id=$user_id AND used='$sub' LIMIT 1");

		// Disarm penalty
		$update_users = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET delay=($user_time + $weight / 2), delay_reason='disarm_penalty' WHERE user_id=$user_id LIMIT 1");
		break;

	// Drop Item
	case 'drop':
		// Get info of current item
		$result_items = mysql_query("SELECT weight, curse FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$id AND user_id=$user_id AND used='' LIMIT 1");
		list($weight, $curse) = mysql_fetch_row($result_items);

		// *** If not curse... proceed... ***

		// Weight adjustment
		$update_users = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET CW=(CW - $weight) WHERE user_id=$user_id LIMIT 1");

		// Drop item!
		$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET user_id=0, map_name='" . $char['map_name'] . "', map_xpos=" . $char['map_xpos'] . ", map_ypos=" . $char['map_ypos'] . " WHERE id=$id AND user_id=$user_id AND used='' LIMIT 1");
		break;

	// Take Item
	case 'take':
		// 'Tis easier to drop than take... :)
		include('lib_delay.inc.php');

		// Get info of current item
		$result_items = mysql_query("SELECT ref_id, weight, curse FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$id AND user_id=0 AND map_name='" . $char['map_name'] . "' AND map_xpos=" . $char['map_xpos'] . " AND map_ypos=" . $char['map_ypos'] . " LIMIT 1");
		list($ref_id, $weight, $curse) = mysql_fetch_row($result_items);

		// Retrieve info of item from factsheet
		$result_items_factsheet = mysql_query("SELECT name, type, quantity FROM " . PHPRPG_DB_PREFIX . "_items_factsheet WHERE id=$ref_id LIMIT 1");
		list($name, $type, $quantity) = mysql_fetch_row($result_items_factsheet);

		// Get base statistics INT
		$result_base = mysql_query("SELECT NTL FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$user_id LIMIT 1");
		list($NTL) = mysql_fetch_row($result_base);

		$recognition = 99; // *** TEMPORARY attribute $recognition ***

		srand((double) microtime() * 1000000);
		$ident_probability = (100 - $quantity / 100) - (rand(0, $NTL) / 3);
		if ($recognition < $ident_probability)
		{
			// Item remain un-identified
			$name = '?' . $type;
		}

		// *** Curse stuff here... ***

		// Take item!
		$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET user_id=$user_id, ident='$name', map_name='', map_xpos='', map_ypos='' WHERE id=$id AND map_name='" . $char['map_name'] . "' AND map_xpos=" . $char['map_xpos'] . " AND map_ypos=" . $char['map_ypos'] . " LIMIT 1");

		// Weight adjustment and delay penalty
		$delay = $user_time + $weight;
		$update_users = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET CW=(CW + $weight), delay=$delay, delay_reason='take_penalty' WHERE user_id=$user_id LIMIT 1");
		break;

	// Identify Item
	case 'identify':
		// Get info of current item
		$result_items = mysql_query("SELECT ref_id, ident FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$id AND user_id=$user_id LIMIT 1");
		list($ref_id, $ident) = mysql_fetch_row($result_items);

		// Attempt to recognise item if it is not already identified
		if (substr($ident, 0, 1) == '?')
		{
			// Retrieve info of item from factsheet
			$result_items_factsheet = mysql_query("SELECT name, type, quantity FROM " . PHPRPG_DB_PREFIX . "_items_factsheet WHERE id=$ref_id LIMIT 1");
			list($name, $type, $quantity) = mysql_fetch_row($result_items_factsheet);

			// Get base statistics INT
			$result_base = mysql_query("SELECT NTL FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$user_id LIMIT 1");
			list($NTL) = mysql_fetch_row($result_base);

			$recognition = 99; // *** TEMPORARY attribute $recognition ***

			srand((double) microtime() * 1000000);
			$ident_probability = (100 - $quantity / 100) - (rand(0, $NTL) / 3);

			if ($recognition >= $ident_probability)
			{
				// Item is identified
				$update_items = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_items SET ident='$name' WHERE id=$id AND user_id=$user_id LIMIT 1");
			}
		}

		// Redirect to display item info
		if (PHPRPG_DEBUG_AUTOREDIRECT)
		{
			echo 'Item Identification<BR>';
			echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '&identify_id=' . $id . '">Click to 	continue</a>';
		} else {
			header("Location: $ref?s=$s&identify_id=$id");
		}
		exit;
		break;

	}

	// Return to referred file
	if (empty($ref))
	{
		if (PHPRPG_DEBUG_AUTOREDIRECT)
		{
			echo 'No referred file!<br>';
			echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
		} else {
			header("Location: index.php");
		}
	}
	else
	{
		if (PHPRPG_DEBUG_AUTOREDIRECT)
		{
			echo 'Item page success!<br>';
			echo '<a href="' . PHPRPG_BASE . $ref . '?s=' . $s . '">Click to continue</a>';
		} else {
			header("Location: $ref?s=$s");
		}
	}
}

?>