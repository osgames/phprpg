<?php
/******************************************************************************/
/*																			*/
/* status.php - Player Status Screen										 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 20 April 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

if (@$_REQUEST['option'] == 'time_calc')
{
	$rvblock = "True";
}

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_events.inc.php');
require_once('lib_template.inc.php');
require_once('lib_session.inc.php');

$option = Retrieve_var('option');
$task = Retrieve_var('task');
$priority = Retrieve_var('priority');
$frequency = Retrieve_var('frequency');
$time = Retrieve_var('time');

require_once('template_header.inc.php');

echo '<script language="Javascript">
<!--

function OpenCalcWindow(eventId)
{
	event_time = document.getElementById(eventId).value;

	calc_win = window.open("' . PHPRPG_BASE . 'events.php?s=' . $s . '&option=time_calc&task=" + eventId + "&time=" + event_time, "Time_Calculator", "width=320, height=300, scrollbars=no, resizable=no");

	if (!calc_win)
	{
		document.getElementById("popup_note").style.display = "block";
	}
	else
	{
		document.getElementById("popup_note").style.display = "none";
		calc_win.focus();
	}
}
-->
</script>';

if ($char['admin_level'] < ADMIN_EVENTS)
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Not a high enough admin to access this page!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

if ($option != 'time_calc')
{
	OpenTable('title', '100%');
	echo 'p h &#39; @ n t a s y';

	OpenTable('content');
	echo '<div align="justify"> Events Menu :: ' . $char['name'] . '<br>';

	include('template_menu.inc.php');

	echo '<br>';
} else {
	OpenTable('title', '100%');
	echo 'Time Calculator';

	OpenTable('content');
}

switch($option)
{
case 'time_calc':
echo '
	<script language="Javascript">

	function TimeCalc()
	{
		cal = document.forms["calc"];

		days = cal.days.value;
		hours = cal.hours.value;
		minutes = cal.minutes.value;
		seconds = cal.seconds.value;

		if (isNaN(days) || isNaN(hours) || isNaN(minutes) || isNaN(seconds))
		{
			cal.answer.value = "Not a #!";
		} else {
			cal.answer.value = (days * 86400) + (hours * 3600) + (minutes * 60) + seconds * 1;
		}
	}

	function Update()
	{
		ans = document.forms["calc"].answer;

		window.opener.document.getElementById("' . $task . '").value = ans.value;
		window.close();
	}
	-->
	</script>';
	$cal_time = $time;

	$days = floor($cal_time/86400);
	$cal_time -= $days * 86400;

	$hours =  floor($cal_time/3600);
	$cal_time -= $hours * 3600;

	$minutes = floor($cal_time/60);
	$cal_time -= $minutes * 60;

	$seconds = $cal_time;

	echo '<form id="calc" name="calc" autocomplete="off">';
	echo '<table>';
	echo '<tr><td>Days: </td>';
	echo '<td><input type="text" class="inputtext" name="days" value="' . $days . '" size="10" maxlength="10" onChange="TimeCalc();"></td></tr>';
	echo '<tr><td>Hours: </td>';
	echo '<td><input type="text" class="inputtext" name="hours" value="' . $hours . '" size="10" maxlength="10" onChange="TimeCalc();"></td></tr>';
	echo '<tr><td>Minutes: </td>';
	echo '<td><input type="text" class="inputtext" name="minutes" value="' . $minutes . '" size="10" maxlength="10" onChange="TimeCalc();"></td></tr>';
	echo '<tr><td>Seconds: </td>';
	echo '<td><input type="text" class="inputtext" name="seconds" value="' . $seconds . '" size="10" maxlength="10" onChange="TimeCalc();"></td></tr>';
	echo '<tr><td>Answer: </td>';
	echo '<td><input type="text" class="inputtext" name="answer" value="' . $time . '" size="10" maxlength="10"></td></tr>';
	echo '<tr><td><input class="inputbutton" type="button" value="Update" onClick="Update();"></td>';
	echo '<td><input class="inputbutton" type="button" value="Cancel" onClick="window.close();"></td></tr>';
	echo '</table>';
	echo '</form>';
	OpenTable('close');
	OpenTable('close');


	exit;
case 'update':
	for ($i = 0; $i < count($task); $i++)
	{
		$result = mysql_query('UPDATE ' . PHPRPG_DB_PREFIX . '_events SET priority="' . $priority[$i] . '", frequency="' . $frequency[$i] . '" WHERE task="' . $task[$i] . '" LIMIT 1');
	}
	break;
case 'new':
	if (!empty($task))
	{
		if (!isset($priority))
		{
			$priority = 5;
		}
		if (!isset($frequency))
		{
			$frequency = 0;
		}
		$result = mysql_query('INSERT ' . PHPRPG_DB_PREFIX . '_events SET task="' . $task . '", priority="' . $priority . '", requested="0", due="0", frequency="' . $frequency . '", assigned_id="0"');
		echo '<BR><font size="4" color="FFFFFF">Task ' . $task . ' added with priority of ' . $priority . ' and frequency of every ' . $frequency . ' seconds.</font><BR>';
	}
	break;
case 'delet':
	if (!empty($task))
	{
		echo '<BR><font size="4" color="FFFFFF">Task: ' . $task . '</font><br>';
		echo '<BR>Are you sure you wish do delete this task?';
		echo '<table cellpadding="10"><tr><td>';
		echo '<form action="events.php?s=' . $s . '&option=delete" name="yes" method="post">';
		echo '<input type="hidden" name="task" value="' . $task . '">';
		echo '<input class="inputbutton" type="submit" value="Yes"></form></td>';
		echo '<td><form action="events.php?s=' . $s . '" name="no" method="post">';
		echo '<input type=submit value="No" class="inputbutton">';
		echo '</form></td></tr></table>';
		exit;
	}
	break;
case 'delete':
	if (!empty($task))
	{
		$result = mysql_query('DELETE FROM ' . PHPRPG_DB_PREFIX . '_events WHERE task="' . $task . '" LIMIT 1');
		echo '<BR><font size="4" color="ffffff">Task ' . $task . ' has been deleted.</font><BR>';
	}
	break;
case 'force':
	echo '<br>Forcing: ' . $task . '<br>';

	if (!empty($task))
	{
		$result = mysql_query('UPDATE ' . PHPRPG_DB_PREFIX . '_events SET due=0 WHERE task="' . $task . '" LIMIT 1');
	}
	require('lib_events.inc.php');
	break;
default:
}

$result = mysql_query("SELECT task, priority, requested, due, frequency FROM " . PHPRPG_DB_PREFIX . "_events ORDER BY priority, frequency, requested");

echo '
	<br>
		<form action="events.php?s=' . $s . '&option=new" method="post" name="new" autocomplete="no">
		<table>
		<tr>
		<td>New Task Name</td>
		<td>Priority</td>
		<td>Frequency</td>
		</tr>

		<tr>
		<td><input type="text" class="inputtext" name="task" value="" maxlength="25" size="25"></td>
		<td><input type="text" class="inputtext" name="priority" value="5" maxlength="3" size="3"></td>
		<td><input type="text" class="inputtext" name="frequency" value="0" maxlength="7" size="7"></td>
		<td><input type="submit" value="Add Event" class="inputbutton"></td>
		</tr>
		</table>
		</form>
';


echo '
	<span id="popup_note" style="display: none;">
	In order to use the pop-up calculator you<br>
	have to allow pop-ups to occur. Right now we<br>
	have no advertising that opens pop-up windows<br>
	so it should be safe to allow the phprpg.org<br>
	site to open pop-ups.<br>
	</span>
		<form action="events.php?s=' . $s . '&option=update" method="post" name="events" autocomplete="off">
		<table cellpadding="5px" border="0">
		<tr>
		<th>Task</th>
		<th>Priority</th>
		<th>Requested</th>
		<th>Due</th>
		<th>Frequency</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		</tr>
';

while ($event = mysql_fetch_array($result))
{
	$task_name[] = $event['task'];

	echo '
		<tr>
		<td><input type="hidden" name="task[]" value="' . $event['task'] . '"><font size="+0"><b>' . $event['task'] . '</b></font></td>
			<td><input type="text" class="inputtext" maxlength="3" size="3" name="priority[]" value="' . $event['priority'] . '"></td>
		<td>' . date('m/d/Y h:i:s', $event['requested']) . '</td>
		<td>' . date('m/d/Y h:i:s', $event['due']) . '</td>
			<td><input type="text" class="inputtext" id="' . $event['task'] . '" maxlength="7" size="7" name="frequency[]" value="' . $event['frequency'] . '"></td>
			<td><input type="button" onClick="OpenCalcWindow(\'' . $event['task'] . '\');" class="inputbutton" value="Use Calc"></td>
		<td><a href="events.php?s=' . $s . '&option=force&task=' . $event['task'] . '">Run</a></td>
		</tr>
	';
}

echo '
		</form>

		<form action="events.php?s=' . $s . '&option=delet" method="post" name="delete" autocomplete="off">
		<tr>
		<td colspan="5">
			<select name="task" class="inputtext">';
for ($i = 0; $i < count($task_name); $i++)
{
	echo '<option value="' . $task_name[$i] . '">' . $task_name[$i] . '</option>';
}
echo '
			</select>
			<input type="submit" value="Delete Event" class="inputbutton">
		</td>
		<td align="right"><input type="submit" value="Update" class="inputbutton"></td>
		</tr>
		</table>
		</form>
	';

OpenTable('close');

require('template_footer.inc.php');

?>