<?php
/******************************************************************************/
/*																			*/
/* lib_template.inc.php - HTML Layout Function Library							*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 9 June 2001														*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


/*																			*/
/* OpenTable($openTableParam, $openTableWidth) - Opens up a standard table	*/
/*																			*/
/* $openTableParam : 'title' opens the title bar							 */
/*					'content' starts the content table						*/
/*					'close' closes the table								*/
/* $openTableWidth : to specify the width attribute of table				 */

function OpenTable($openTableParam, $openTableWidth = '')
{

	// Border colour changes according to file
	if (PHP_SELF == 'battle.php')
	{
		$open_table_border = 'ae3c71';
	}
	elseif (PHP_SELF == 'status.php')
	{
		$open_table_border = 'deee31';
	}
	elseif (PHP_SELF == 'friends.php')
	{
		$open_table_border = 'cccccc';
	}
	else
	{
		$open_table_border = '318cce';
	}

	if ($openTableParam == 'title')
	{
		if ($openTableWidth)
		{
				$openTableWidth = ' width="' . $openTableWidth . '"';
		}

		echo '
<table cellpadding="1" cellspacing="0"' . $openTableWidth . ' border="0">
<tr>
<td bgcolor="#000000">
<table cellpadding="1" cellspacing="0" width="100%" border="0">
<tr>
<td bgcolor="#' . $open_table_border . '">
<table cellpadding="1" cellspacing="0" width="100%" border="0">
<tr>
<td bgcolor="#000000">
<table cellpadding="6" cellspacing="0" width="100%" border="0">
<tr>
<td background="' . PHPRPG_IMG . 'box_bg.png" bgcolor="#424242">
<div align="right">
<font face="arial" size="3" color="#f3f3f3">
<b>
		';

	}
	elseif ($openTableParam == 'content')
	{

		echo '
</b>
</font>
</div>
<font size="2" color="#bbbbbb">
		';

	}
	elseif ($openTableParam == 'close')
	{

		echo '
</font>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
	';

	}
	else
	{
		echo 'function OpenTable Syntax error!';
	}

}


/*																			*/
/* QuoteTable($quoteTableParam, $quoteTableWidth) - Opens up a quote table	*/
/*																			*/
/* $quoteTableParam : 'open' opens and 'close' closes the table				*/
/* $quoteTableWidth : to specify the width attribute of table				*/

function QuoteTable($quoteTableParam, $quoteTableWidth = '')
{

	if ($quoteTableParam == 'open')
	{
		if ($quoteTableWidth)
		{
				$quoteTableWidth = ' width="' . $quoteTableWidth . '"';
		}

		echo '
<table cellpadding="0" cellspacing="0"' . $quoteTableWidth . ' border="0">
<tr>
<td><img src="' . PHPRPG_IMG . 'quote_ul.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_tp.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_ur.png" width="15" height="7" border="0" alt=""></td>
</tr>
<tr>
<td background="' . PHPRPG_IMG . 'quote_lt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td>
		';

	}
	elseif ($quoteTableParam == 'close')
	{

		echo '
</td>
<td background="' . PHPRPG_IMG . 'quote_rt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
</tr>
<tr>
<td><img src="' . PHPRPG_IMG . 'quote_dl.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_bt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_dr.png" width="15" height="7" border="0" alt=""></td>
</tr>
</table>
		';

	}
	else
	{
		echo 'function QuoteTable Syntax error!';
	}

}


/*																			*/
/* string BackgroundColour($per) - Returns colour code according to game time */
/*								$per specifies the saturation percentage	*/

function BackgroundColour($per)
{
	$date_seconds = time();

	// Theme colour depends on which game cycle
	switch (GameDate($date_seconds, 'game_cycle'))
	{
		case 1:
			$theme_R = 124; $theme_G = 44; $theme_B = 64;
			break;
		case 2:
			$theme_R = 202; $theme_G = 193; $theme_B = 41;
			break;
		case 3:
			$theme_R = 46; $theme_G = 120; $theme_B = 56;
			break;
		case 4:
			$theme_R = 44; $theme_G = 43; $theme_B = 129;
			break;
	}

	// If $per is negative, create theme with negative colours
	if ($per < 0)
	{
		$theme_R = 255 - $theme_R; $theme_G = 255 - $theme_G; $theme_B = 255 - $theme_B;
		$per = abs($per);
	}

	// Portion through the day
	$day_fraction = (GameDate($date_seconds, 'game_seconds_into_game_cycle') / (60 * 60 * 24));
	if ($day_fraction > 0.5) { $day_fraction = 1 - $day_fraction; }
	$day_fraction = $day_fraction * 2;

	$fade_R = 240 * $day_fraction;
	$bg_R = $fade_R - (($fade_R - $theme_R) * $per / 100);
	$bg_R_hex = dechex(round($bg_R));
	$bg_R_hex = str_repeat('0', 2 - strlen($bg_R_hex)) . $bg_R_hex;

	$fade_G = 240 * $day_fraction;
	$bg_G = $fade_G - (($fade_G - $theme_G) * $per / 100);
	$bg_G_hex = dechex(round($bg_G));
	$bg_G_hex = str_repeat('0', 2 - strlen($bg_G_hex)) . $bg_G_hex;

	$fade_B = 240 * $day_fraction;
	$bg_B = $fade_B - (($fade_B - $theme_B) * $per / 100);
	$bg_B_hex = dechex(round($bg_B));
	$bg_B_hex = str_repeat('0', 2 - strlen($bg_B_hex)) . $bg_B_hex;

	return '#' . $bg_R_hex . $bg_G_hex . $bg_B_hex;
}


/*																			 */
/* WeatherBox - Display weather statistics									 */

function WeatherBox()
{
	global $char;

	$result = mysql_query("SELECT w.* FROM " . PHPRPG_DB_PREFIX . "_map m, " . PHPRPG_DB_PREFIX . "_weather w WHERE m.name = '".$char['map_name']."' AND m.xpos = '".$char['map_xpos']."' AND m.ypos = '".$char['map_ypos']."' AND w.region = m.region");

	if ($result)
	{
		echo '<table width="100%" cellspacing="0">';
		echo '<tr>';
		echo '<td colspan="2" align="center">';
		$weather = mysql_fetch_array($result);
		switch ($weather['currently']) {
			case 'Sunny':
				echo '<img src="' . PHPRPG_IMG . 'weather/day_sunny.gif">';
				break;
			case 'Partly Cloudy':
				echo '<img src="' . PHPRPG_IMG . 'weather/day_partlycloudy.gif">';
				break;
			case 'Mostly Cloudy':
				echo '<img src="' . PHPRPG_IMG . 'weather/day_mostlycloudy.gif">';
				break;
			case 'Rain':
				echo '<img src="' . PHPRPG_IMG . 'weather/day_lotsrain.gif">';
				break;
			case 'Fog':
				echo '<img src="' . PHPRPG_IMG . 'weather/day_haze.gif">';
				break;
		}
		echo '</td>';
		echo '</tr>';
		echo '<tr><td>Currently: </td><td> ' . $weather['currently'] . ' </td></tr>';
		echo '<tr><td>High:      </td><td> ' . $weather['high'] . ' </td></tr>';
		echo '<tr><td>Low:       </td><td> ' . $weather['low'] . ' </td></tr>';
		echo '<tr><td>Wind:      </td><td> ' . strtr($weather['wind_dir'], "nesw", "NESW") . ' ' . $weather['wind'] . ' mph </td></tr>';
		echo '</table>';
	}
}

?>