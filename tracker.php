<?php

/******************************************************************************/
/*																			*/
/* tracker.php - Analyser for the visitor log								*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 23 June 2001													 */
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require('config.inc.php');
include('lib.inc.php');
include('lib_template.inc.php');

require('lib_session.inc.php');

require('lib_events.inc.php');

header("Expires: Mon, 6 Dec 1977 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

if ($char['admin_level'] <= 2)
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Not a high enough admin to access tracker!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}
else
{

$type = Retrieve_var('type');

require('template_header.inc.php');

OpenTable('title', '98%');
echo 'phpRPG tracker';
OpenTable('content');

echo '
<font size="1"><a href="' . PHP_SELF . '?s=' . $s . '">[Total]</a> <a href="' . PHP_SELF . '?s=' . $s . '&type=unique">[Unique]</a> <a href="' . PHP_SELF . '?s=' . $s . '&type=toprefer">[Top Referrer]</a> <a href="' . PHP_SELF . '?s=' . $s . '&type=host">[Host Stats]</a> <a href="' . PHP_SELF . '?s=' . $s . '&type=agent">[Agent Stats]</a> <a href="' . PHP_SELF . '?s=' . $s . '&type=daily">[Daily Hits]</a> <a href="local.php?s=' . $s . '">[Local View]</a></form><br>
';

switch ($type) {

	case 'daily':
		$result = mysql_query("SELECT DISTINCT day, COUNT(*) AS refcount FROM " . PHPRPG_DB_PREFIX . "_activity GROUP BY day ORDER BY day DESC");
		echo '
Daily Hits<br>
<table cellspacing="5" cellpadding="0" border="0">
<tr><td><font size="2" color="#eeeeee">Date</font></td><td><font size="2" color="#eeeeee"><div align="right">Hits</div></font></td></tr>
		';
		$i = 0;
		while ($report = mysql_fetch_array($result))
		{
			if ($i == 0)
			{
				echo '<tr><td><font size="1" color="#dddddd">' . $report['day'] . '</font></td><td><font size="1" color="#dddddd"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 1;
			}
			elseif ($i == 1)
			{
				echo '<tr><td><font size="1" color="#999999">' . $report['day'] . '</font></td><td><font size="1" color="#999999"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 0;
			}
		}
		echo '</table>';
		break;

	case 'agent':
		$result = mysql_query("SELECT DISTINCT agent, COUNT(*) AS refcount FROM " . PHPRPG_DB_PREFIX . "_activity GROUP BY host ORDER BY refcount DESC");
		echo '
Agent Stats<br>
<table cellspacing="5" cellpadding="0" border="0">
<tr><td><font size="2" color="#eeeeee">Agent</font></td><td><font size="2" color="#eeeeee"><div align="right">Hits</div></font></td></tr>
		';
		$i = 0;
		while ($report = mysql_fetch_array($result))
		{
			if ($i == 0)
			{
				echo '<tr><td><font size="1" color="#dddddd">' . $report['agent'] . '</font></td><td><font size="1" color="#dddddd"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 1;
			}
			elseif ($i == 1)
			{
				echo '<tr><td><font size="1" color="#999999">' . $report['agent'] . '</font></td><td><font size="1" color="#999999"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 0;
			}
		}
		echo '</table>';
		break;

	case 'host':
		$result = mysql_query("SELECT DISTINCT host, COUNT(*) AS refcount FROM " . PHPRPG_DB_PREFIX . "_activity GROUP BY host ORDER BY refcount DESC");
		echo '
Host Stats<br>
<table cellspacing="5" cellpadding="0" border="0">
<tr><td><font size="2" color="#eeeeee">Host</font></td><td><font size="2" color="#eeeeee"><div align="right">Hits</div></font></td></tr>
		';
		$i = 0;
		while ($report = mysql_fetch_array($result))
		{
			if ($i == 0)
			{
				echo '<tr><td><font size="1" color="#dddddd">' . $report['host'] . '</font></td><td><font size="1" color="#dddddd"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 1;
			}
			elseif ($i == 1)
			{
				echo '<tr><td><font size="1" color="#999999">' . $report['host'] . '</font></td><td><font size="1" color="#999999"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 0;
			}
		}
		echo '</table>';
		break;

	case 'toprefer':
		$result = mysql_query("SELECT DISTINCT ref, COUNT(*) AS refcount FROM " . PHPRPG_DB_PREFIX . "_activity GROUP BY ref ORDER BY refcount DESC, ref");
		echo '
Top Referrers<br>
<table cellspacing="5" cellpadding="0" border="0">
<tr><td><font size="2" color="#eeeeee">Referrer</font></td><td><font size="2" color="#eeeeee"><div align="right">Hits</div></font></td></tr>
		';
		$i = 0;
		while ($report = mysql_fetch_array($result))
		{
			if ($report['ref'] != '')
			{

			if ($i == 0)
			{
				echo '<tr><td><font size="1" color="#dddddd">' . $report['ref'] . '</font></td><td><font size="1" color="#dddddd"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 1;
			}
			elseif ($i == 1)
			{
				echo '<tr><td><font size="1" color="#999999">' . $report['ref'] . '</font></td><td><font size="1" color="#999999"><div align="right">' . $report['refcount'] . '</div></font></td></tr>';
				$i = 0;
			}

			}
		}
		echo '</table>';
		break;

	case 'unique':
		$result = mysql_query("SELECT DISTINCT * FROM " . PHPRPG_DB_PREFIX . "_activity ORDER BY day, hour");
		$count = mysql_num_rows($result);
		echo "Unique Hits: $count<br><br>";
		$result = mysql_query("SELECT DISTINCT * FROM " . PHPRPG_DB_PREFIX . "_activity ORDER BY day DESC, hour DESC LIMIT 30");
		// Fall through as the display format of 'unique' is the same as default

	default:
		if ($type != 'unique')
		{

		$result = mysql_query("SELECT COUNT(*) AS hits FROM " . PHPRPG_DB_PREFIX . "_activity");
		$countarray = mysql_fetch_array($result);
		$count = $countarray['hits'];
		echo "Total Hits: $count<br><br>";
		$result = mysql_query("SELECT * FROM " . PHPRPG_DB_PREFIX . "_activity ORDER BY day DESC, hour DESC LIMIT 30");

		}

echo '
Last 30 Hits<br>
<table cellspacing="5" cellpadding="0" border="0">
<tr>
<td width="70"><font size="1" color="#eeeeee">Date</font></td>
<td width="50"><font size="1" color="#eeeeee">Hour</font></td>
<td width="60"><font size="1" color="#eeeeee">Platform</font></font></td>
<td width="50"><font size="1" color="#eeeeee">Type</font></td>
<td width="100"><font size="1" color="#eeeeee">Agent</font></td>
<td width="50"><font size="1" color="#eeeeee"><div align="center">Version</div></font></td>
<td width="230"><font size="1" color="#eeeeee">Host</font></td>
</tr>
';

//<td width="230"><font size="2" color="#eeeeee">Referrer</font></td>

$i = 0;
while ($report = mysql_fetch_array($result))
{
	// Analyse Browser Agent
	GetBrowser($report['agent']);
	GetPlatform($report['agent']);

	// Analyse Date and Time
	$analyseDate = explode("-", $report['day']);
	$reportDay = $analyseDate[2] . ' ';
	// Corresponding Month
	switch ($analyseDate[1])
	{
		case '01':
			$reportDay .= 'Jan'; break;
		case '02':
			$reportDay .= 'Feb'; break;
		case '03':
			$reportDay .= 'Mar'; break;
		case '04':
			$reportDay .= 'Apr'; break;
		case '05':
			$reportDay .= 'May'; break;
		case '06':
			$reportDay .= 'Jun'; break;
		case '07':
			$reportDay .= 'Jul'; break;
		case '08':
			$reportDay .= 'Aug'; break;
		case '09':
			$reportDay .= 'Sep'; break;
		case '10':
			$reportDay .= 'Oct'; break;
		case '11':
			$reportDay .= 'Nov'; break;
		case '12':
			$reportDay .= 'Dec'; break;
	}
	$reportDay .= ' ' . $analyseDate[0];
	if ($report['hour'] > 12)
	{
		$reportHour = $report['hour'] - 12;
		$reportHour .= ' pm';
	}
	else
	{
		if ($report['hour'] == 0)
		{
			$reportHour = '12 am';
		}
		elseif ($report['hour'] == 12)
		{
			$reportHour = '12 pm';
		}
		else
		{
			$reportHour = $report['hour'] . ' am';
		}
	}

	if ($i == 0)
	{
		echo '<tr><td><font size="1" color="#dddddd">' . $reportDay . '</font></td><td><font size="1" color="#dddddd">' . $reportHour . '</font></td><td><font size="1" color="#dddddd">' . $BROWSER_PLATFORM . '</font></td><td><font size="1" color="#dddddd">' . $BROWSER_TYPE . '</font></td><td><font size="1" color="#dddddd">' . $BROWSER_AGENT . '</font></td><td><font size="1" color="#dddddd"><div align="center">' . $BROWSER_VER . '</div></font></td><td><font size="1" color="#dddddd">' . $report['host'] . '</font></td></tr>';
		//<td><font size="1" color="#999999">' . $report['ref'] . '</font></td>
		$i = 1;
	}
	elseif ($i == 1)
	{
		//<td><font size="1" color="#999999">' . $report['ref'] . '</font></td>
		echo '<tr><td><font size="1" color="#999999">' . $reportDay . '</font></td><td><font size="1" color="#999999">' . $reportHour . '</font></td><td><font size="1" color="#999999">' . $BROWSER_PLATFORM . '</font></td><td><font size="1" color="#999999">' . $BROWSER_TYPE . '</font></td><td><font size="1" color="#999999">' . $BROWSER_AGENT . '</font></td><td><font size="1" color="#999999"><div align="center">' . $BROWSER_VER . '</div></font></td><td><font size="1" color="#999999">' . $report['host'] . '</font></td></tr>';
		$i = 0;
	}
}
echo '</table>';

}

OpenTable('close');

require('template_footer.inc.php');
}



function GetBrowser($HTTP_USER_AGENT)
{
	global $BROWSER_VER, $BROWSER_AGENT, $BROWSER_TYPE;

	// Determine browser and version
	if (ereg('MSIE ([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Internet Explorer';
		$BROWSER_TYPE = 'Browser';
	} elseif (ereg( 'Opera/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Opera';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('Netscape6/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Netscape';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('Netscape', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = '';
		$BROWSER_AGENT = 'Netscape';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('Nav', $HTTP_USER_AGENT) || ereg('Gold', $HTTP_USER_AGENT) || ereg('X11', $HTTP_USER_AGENT)) {
		$BROWSER_VER = 'Other';
		$BROWSER_AGENT = "Netscape";
		$BROWSER_TYPE = 'Browser';
	} elseif (ereg( 'Mozilla/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Mozilla';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('Lynx/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Lynx';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('WebTV', $HTTP_USER_AGENT)) {
		$BROWSER_VER = '';
		$BROWSER_AGENT = 'WebTV';
		$BROWSER_TYPE = 'Browser';
	} else if (ereg('Konqueror', $HTTP_USER_AGENT)) {
		$BROWSER_VER = '';
		$BROWSER_AGENT = 'Konqueror';
		$BROWSER_TYPE = 'Browser';
	} elseif (ereg( 'QWK.Mon/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Qwkmon.com';
		$BROWSER_TYPE = 'Bot';
	} elseif (ereg( 'DeckIt/([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
		$BROWSER_VER = $log_version[1];
		$BROWSER_AGENT = 'Deck-It';
		$BROWSER_TYPE = 'Browser';
	} else if (eregi('Acoon', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Acoon.de'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('AltaVista', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'AltaVista'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Anzwers', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Anzwers.com.au'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('euroseek', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'EuroSeek.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('ArchitextSpider', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Excite'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('fido', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'PlanetSearch.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Fireball', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Fireball.de'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('GAIS', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Seed.net.tw'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Google', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Googlebot.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Gulliver', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'NortherLight.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Infoseek', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Infoseek'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('lwp-trivial', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Search4Free.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Lycos', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Lycos'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Nokia8110', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Nokia 7110'; $BROWSER_TYPE = 'WAP';
	} else if (eregi('Scooter', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'AltaVista'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Search.at', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Search.at'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Slurp', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Hotbot.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Fireball', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'SwissSearch (Search.ch)'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Informant', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'The Informant (informant.dartmouth.edu)'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('Ultraseek', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Infoseek'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('WebCrawler', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'WebCrawler.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('WiseWire', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'WiseWire.com'; $BROWSER_TYPE = 'Bot';
	} else if (eregi('bot', $HTTP_USER_AGENT) || eregi('Spider', $HTTP_USER_AGENT)) { $BROWSER_VER = ''; $BROWSER_AGENT = 'Bot';
	} else { $BROWSER_VER = ''; $BROWSER_AGENT = 'Unknown'; $BROWSER_TYPE = 'Unknown';
	}

}

function GetPlatform($HTTP_USER_AGENT)
{

	global $BROWSER_PLATFORM;

	// Determines platform
	if (strstr($HTTP_USER_AGENT, 'Win')) {
		$BROWSER_PLATFORM = 'Windows';
	} else if (strstr($HTTP_USER_AGENT, 'Mac') || strstr($HTTP_USER_AGENT, 'PCC')) {
		$BROWSER_PLATFORM = 'Mac';
	} else if (strstr($HTTP_USER_AGENT,'Linux')) {
		$BROWSER_PLATFORM='Linux';
	} else if (strstr($HTTP_USER_AGENT,'Unix')) {
		$BROWSER_PLATFORM = 'Unix';
	} else if (strstr($HTTP_USER_AGENT,'FreeBSD')) {
		$BROWSER_PLATFORM = 'FreeBSD';
	} else if (strstr($HTTP_USER_AGENT,'SunOS')) {
		$BROWSER_PLATFORM = 'SunOS';
	} else if (strstr($HTTP_USER_AGENT,'IRIX')) {
		$BROWSER_PLATFORM = 'IRIX';
	} else if (strstr($HTTP_USER_AGENT,'BeOS')) {
		$BROWSER_PLATFORM = 'BeOS';
	} else if (strstr($HTTP_USER_AGENT,'OS/2')) {
		$BROWSER_PLATFORM = 'OS/2';
	} else if (strstr($HTTP_USER_AGENT,'AIX')) {
		$BROWSER_PLATFORM = 'AIX';
	} else {
		$BROWSER_PLATFORM='Other';
	}

}
?>