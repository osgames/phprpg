<?php

if (eregi('.inc.php', $_SERVER['PHP_SELF']))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}
// Basic menu
echo '
<font size="1">
<a href="local.php?s=' . $s . '">[Local View]</a>
<a href="battle.php?s=' . $s . '">[Battle Mode]</a>
<a href="status.php?s=' . $s . '">[Player Status]</a>
<a href="online.php?s=' . $s . '">[Online Players]</a>
<a href="friends.php?s=' . $s . '">[Friends]</a>
[Options]
<a href="logout.php?s=' . $s . '">[Logout]</a>
<br>
';
//<a href="options.php?s=' . $s . '">[Options]</a>

// Display admin options according to admin levels
if ($char['admin_level'] >= ADMIN_TRACKER)
{
	echo '
<a href="tracker.php?s=' . $s . '">[Hits Tracker]</a>
	';
}
if ($char['admin_level'] >= ADMIN_MAP_EDITOR)
{
	echo '
<a href="map_editor.php?s=' . $s . '">[Map Editor]</a>
	';
}
if ($char['admin_level'] >= ADMIN_EVENTS)
{
	echo '
<a href="events.php?s=' . $s . '">[Events]</a>
	';
}
if ($char['admin_level'] >= ADMIN_TELEPORT)
{
	echo '
[Teleport]';
}
if ($char['admin_level'] >= ADMIN_HALT_GAME)
{
	echo '[Halt Game]';
}

echo '</font>
';

?>