<?php
/******************************************************************************/
/*																			*/
/* template_nav.inc.php - HTML Navigation Window							 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 2 April 2002													 */
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

DbConnect();

// Extract information for map
$map = $char['map_name'];
$xpos = $char['map_xpos'];
$ypos = $char['map_ypos'];
if ($xpos % 2 == 0)
{
	// Even numbered columns -> adjacent grids retain original $ypos
	$ynew = $ypos;
}
else
{
	// Odd numbered columns -> adjacent grids is $ypos - 1
	$ynew = $ypos - 1;
}

// Eliminate white spaces in $map by replacing with '+' (for GET link)
$map_selected = str_replace(' ', '+', $map);

// Retrieve information regarding current location
$result = mysql_query("SELECT move_up, move_ur, move_dr, move_dn, move_dl, move_ul, type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='" . $map . "' AND xpos='" . $xpos . "' AND ypos='" . $ypos . "'");
$map_now = mysql_fetch_array($result);

echo '
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td><img src="' . PHPRPG_IMG . 'quote_ul.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_tp.png" colspan="3"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_ur.png" width="15" height="7" border="0" alt=""></td>
</tr>
<tr>
<td background="' . PHPRPG_IMG . 'quote_lt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td>
<img src="' . PHPRPG_IMG . 'x.png" width="32" height="16" border="0"><br>
';

// String to be used for javascript navigation
$javascript_string = '';

// Top left tile
if ($map_now['move_ul'] != 'N')
{
	$x = $xpos - 1; $y = $ynew;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_ul'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=ul&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "4":
			url = "move.php?s=' . $s . '&move=ul&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

// Bottom left tile
if ($map_now['move_dl'] != 'N')
{
	$x = $xpos - 1; $y = $ynew +  1;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_dl'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=dl&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "1":
			url = "move.php?s=' . $s . '&move=dl&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="32" height="16" border="0"><br>
</td>
<td>
';

// Top tile
if ($map_now['move_up'] != 'N')
{
	$x = $xpos; $y = $ypos - 1;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_up'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=up&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "5":
			url = "move.php?s=' . $s . '&move=up&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

// Current tile
echo '<img src="' . PHPRPG_IMG . 'tile_' . $map_now['type'] . $map_now['subtype'] . '.png" width="32" height="32" border="0" alt=""><br>';

// Bottom tile
if ($map_now['move_dn'] != 'N')
{
	$x = $xpos; $y = $ypos + 1;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_dn'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=dn&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "2":
			url = "move.php?s=' . $s . '&move=dn&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

echo '
</td>
<td>
<img src="' . PHPRPG_IMG . 'x.png" width="32" height="16" border="0"><br>
';

// Top right tile
if ($map_now['move_ur'] != 'N')
{
	$x = $xpos + 1; $y = $ynew;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_ur'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=ur&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "6":
			url = "move.php?s=' . $s . '&move=ur&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

// Bottom right tile
if ($map_now['move_dr'] != 'N')
{
	$x = $xpos + 1; $y = $ynew + 1;

	$result = mysql_query("SELECT type, subtype FROM " . PHPRPG_DB_PREFIX . "_map WHERE name='$map' AND xpos='$x' AND ypos='$y'");
	$map_adj = mysql_fetch_array($result);
	mysql_free_result($result);

	$tile_image = PHPRPG_IMG . 'tile_' . $map_adj['type'] . $map_adj['subtype'] . '.png';
	if ($map_now['move_dr'] == '6')
	{
		echo '<img src="' . $tile_image . '" width="32" height="32" border="0" alt=""><br>';
	}
	else
	{
		echo '<a href="move.php?s=' . $s . '&move=dr&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '"><img src="' . $tile_image . '" width="32" height="32" border="0" alt="(' . $x . ', ' . $y . ')"></a><br>';
		$javascript_string .= '
		case "3":
			url = "move.php?s=' . $s . '&move=dr&current=' . $map_selected . '&x=' . $xpos . '&y=' . $ypos . '&ref=' . PHP_SELF . '";
			break;
		';
	}
}
else
{
	echo '<img src="' . PHPRPG_IMG . 'x.png" width="32" height="32" border="0"><br>';
}

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="32" height="16" border="0"><br>
</td>
<td background="' . PHPRPG_IMG . 'quote_rt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
</tr>
<tr>
<td><img src="' . PHPRPG_IMG . 'quote_dl.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_bt.png" colspan="3"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_dr.png" width="15" height="7" border="0" alt=""></td>
</tr>
</table>
<br>
<font face="arial" size="2">position [<font color="#eeeeee">' . $xpos . '</font>, <font color="#eeeeee">' . $ypos . '</font>]</font><br>
';

// Javascript keypress event handler for navigation
echo '
<script language="JavaScript">
<!--
IE4 = (document.all);
NS4 = (document.layers);

textSwitch = 0;

// Event Capture
if (NS4) document.captureEvents(Event.KEYPRESS);
document.onkeypress = KeyCapture;

// Keypress triggers this function
function KeyCapture(e)
{

asciiCode = (NS4) ? e.which : event.keyCode;
switch (asciiCode)
{

case 27:
    ShowMenu();
	if (textSwitch == 1)
	{
		document.chatbox.voice.blur();
		textSwitch = 0;
	} else if (textSwitch == 2)
	{
		document.searchbox.search.blur();
		textSwitch = 0;
	}
	break;
case 13:
	if (textSwitch != 2)
	{
		document.chatbox.voice.focus();
		textSwitch = 1;
	}
	break;
default:
	keyPressed = String.fromCharCode(asciiCode).toLowerCase();
	url = "";
	if (textSwitch == 0)
	{

	switch (keyPressed)
	{
' . $javascript_string .
'
	}
	if (url != "")
	{
		window.location = url;
	}

	}

}

return;
}
//-->
</script>
';


?>