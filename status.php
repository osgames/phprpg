<?php
/******************************************************************************/
/*																			*/
/* status.php - Player Status Screen										 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 20 April 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib_events.inc.php');
require_once('lib.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$reason = Retrieve_var('reason');
$identify_id = Retrieve_var('identify_id');

require_once('template_header.inc.php');

DbConnect();

echo '
<table cellpadding="0" cellspacing="10" width="100%" border="0">
<tr><td width="100%" valign="top">
';

OpenTable('title', '100%');
echo 'p h &#39; @ n t a s y';

OpenTable('content');
echo '
<div align="justify">
Player Status :: ' . $char['name'] . '<br>
';

require_once('template_menu.inc.php');

require_once('template_reason.inc.php');

echo '
<table cellpadding="15" cellspacing="0" border="0">
<tr>
<td>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td valign="top">
';

// Grid Layout
if ($char['race'] == 'Soul')
{

echo '
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="380" height="261" background="' . PHPRPG_IMG . 'status_grid.png" bgcolor="#000000">

<div align="center" valign="center">
<img src="' . PHPRPG_IMG . 'x.png" width="380" height="0" border="0"><br>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
';

QuoteTable('open');

$result_used_items = mysql_query("SELECT id, ident, img FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$user_id AND used='weapon_right' LIMIT 1");
if (mysql_num_rows($result_used_items) > 0)
{
	list($item_id, $ident, $img) = mysql_fetch_row($result_used_items);
	echo '
<a href="item.php?s=' . $s . '&option=unequip&sub=weapon_right&id=' . $item_id . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'item_' . $img . '.png" width="60" height="60" border="0" alt="' . $ident . '"></a><br>
	';
}
else
{
	echo '
<img src="' . PHPRPG_IMG . 'x.png" width="60" height="60" border="0" alt=""><br>
	';
}

QuoteTable('close');

echo '
</td>
<td>
<img src="' . PHPRPG_IMG . 'avatar_' . $char['avatar'] . '.png" width="98" height="98" border="0" alt="' . $char['name'] . '"><br>
</td>
<td>
';

QuoteTable('open');

$result_used_items = mysql_query("SELECT id, ident, img FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$user_id AND used='weapon_left' LIMIT 1");
if (mysql_num_rows($result_used_items) > 0)
{
	list($item_id, $ident, $img) = mysql_fetch_row($result_used_items);
	echo '
<a href="item.php?s=' . $s . '&option=unequip&sub=weapon_left&id=' . $item_id . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'item_' . $img . '.png" width="60" height="60" border="0" alt="' . $ident . '"></a><br>
	';
}
else
{
	echo '
<img src="' . PHPRPG_IMG . 'x.png" width="60" height="60" border="0" alt=""><br>
	';
}

QuoteTable('close');

echo '
</td>
</tr>
</table>
<img src="' . PHPRPG_IMG . 'x.png" width="380" height="0" border="0"><br>
</div>
</td>
</tr>
</table>
</td>
<td valign="top">
';

// Base Statistics
$result_base = mysql_query("SELECT STR, NTL, PIE, VIT, DEX, SPD, CW, CC FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$user_id LIMIT 1");

list($STR, $NTL, $PIE, $VIT, $DEX, $SPD, $CW, $CC) = mysql_fetch_row($result_base);

QuoteTable('open');

echo '
<font color="#eeeeee">Base Statistics</font><br>
<table cellpadding="0" cellspacing="2" border="0">
<tr><td>Race</td><td><img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="15" border="0"></td><td><div align="right">' . $char['race'] . '</div></td></tr>
<tr><td>STR</td><td></td><td><div align="right">' . $STR . '</div></td></tr>
<tr><td>INT</td><td></td><td><div align="right">' . $NTL . '</div></td></tr>
<tr><td>PIE</td><td></td><td><div align="right">' . $PIE . '</div></td></tr>
<tr><td>VIT</td><td></td><td><div align="right">' . $VIT . '</div></td></tr>
<tr><td>DEX</td><td></td><td><div align="right">' . $DEX . '</div></td></tr>
<tr><td>SPD</td><td></td><td><div align="right">' . $SPD . '</div></td></tr>
<tr><td>CC</td><td></td><td>' . $CW . '/' . $CC . '</td></tr>
</table>
';

QuoteTable('close');

echo '
</td>
</tr>
</table>
';
}

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="5" border="0"><br>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td valign="top">
';

// Player's items
$result_items = mysql_query("SELECT id, ident, img FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=$user_id AND used='' ORDER BY ident");
if (mysql_num_rows($result_items) > 0)
{
	QuoteTable('open');
	echo '
<div align="left">
<font color="#eeeeee">Your Items</font><br>
<table cellpadding="0" cellspacing="4" border="0">
	';

	while ($items = mysql_fetch_array($result_items))
	{
		echo '
<tr>
<td><img src="' . PHPRPG_IMG . 'item_' . $items['img'] . '.png" width="30" height="30" border="0" alt=""></td>
<td>'. $items['ident'] .'</td><td><img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="5" border="0"></td>
<td>[<i><a href="item.php?s=' . $s . '&option=equip&sub=weapon_right&id=' . $items['id'] . '&ref=' . PHP_SELF . '">equip&nbsp;right</a></i>]</td>
<td>[<i><a href="item.php?s=' . $s . '&option=equip&sub=weapon_left&id=' . $items['id'] . '&ref=' . PHP_SELF . '">equip&nbsp;left</a></i>]</td>
<td>[<i><a href="item.php?s=' . $s . '&option=drop&id=' . $items['id'] . '&ref=' . PHP_SELF . '">drop</a></i>]</td>
<td>[<i><a href="item.php?s=' . $s . '&option=identify&id=' . $items['id'] . '&ref=' . PHP_SELF . '">identify</a></i>]</td>
</tr>
		';
	}

	echo '
</table>
</div>
	';
	QuoteTable('close');
}

// List of items on the ground
$result_items = mysql_query("SELECT id, img, ident FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id=0 AND map_name='" . $char['map_name'] . "' AND map_xpos=" . $char['map_xpos'] . " AND map_ypos=" . $char['map_ypos'] . "  ORDER BY id");
if (mysql_num_rows($result_items) > 0)
{
	echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="5" border="0"><br>
	';
	QuoteTable('open');
	echo '
<div align="left">
<font color="#eeeeee">Items On The Ground</font><br>
<table cellpadding="0" cellspacing="4" border="0">
<tr>
	';

	while ($items = mysql_fetch_array($result_items))
	{
		echo '
<td><a href="item.php?s=' . $s . '&option=take&id=' . $items['id'] . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'item_' . $items['img'] . '.png" width="30" height="30" border="0" alt="' . $items['ident'] . '"></td>
		';
	}

	echo '
</tr>
</table>
</div>
	';
	QuoteTable('close');
}

echo '
</td>
<td valign="top">
';

// Display item info (if it is recognised)
if (!empty($identify_id))
{
	QuoteTable('open');

	// Get info of current item
	$result_items = mysql_query("SELECT ref_id, ident, indx, deviation, weight, curse, img FROM " . PHPRPG_DB_PREFIX . "_items WHERE id=$identify_id AND user_id=$user_id LIMIT 1");
	$items = mysql_fetch_array($result_items);

	if ((mysql_num_rows($result_items) == 1) && (substr($items['ident'], 0, 1) != '?'))
	{
		// Retrieve info of item from factsheet
		$result_items_factsheet = mysql_query("SELECT type, indx, deviation, weight, race FROM " . PHPRPG_DB_PREFIX . "_items_factsheet WHERE id=" . $items['ref_id'] . " LIMIT 1");
		$items_factsheet = mysql_fetch_array($result_items_factsheet);

		echo '
<table cellpadding="0" cellspacing="4" border="0">
<tr>
<td valign="top">
<div align="center">
<img src="' . PHPRPG_IMG . 'item_' . $items['img'] . '.png" width="60" height="60" border="0" alt=""><br>
<font color="#eeeeee">' . $items['ident'] . '</font>
</div>
</td>
<td valign="top">
<div align="left">
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="40" border="0"><br>
type: ' . $items_factsheet['type'] . '<br>
damage: ' . $items['indx'] . '&nbsp;&plusmn;&nbsp;' . $items['deviation'] . '<br>
weight: ' . $items['weight'] . '
</div>
</td>
</tr>
</table>
		';
	}
	else
	{
		echo '<font color="#c03030"><b>* Failed to Identify *</b></font>';
	}

	QuoteTable('close');
}

echo '
</td>
</tr>
</table>
</td>
</tr>
</table>

</div>
';

OpenTable('close');

echo '
</td><td width="172" valign="top">
';

// Stats Box
require_once('template_stats.inc.php');

echo '
</td></tr></table>
';

require_once('template_footer.inc.php');
?>