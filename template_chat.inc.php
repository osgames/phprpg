<?php
/******************************************************************************/
/*																		  */
/* template_chat.inc.php - HTML Chat Box									*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 15 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		*/
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*/
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		*/
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

@$option = $_REQUEST['option'];

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
}

DbConnect();

$wca = 375;

if (isset($_SESSION['sw']))
{
	$wca = $_SESSION['sw'] - 425;
}

// First get the interactions information so we can do the rest correctly.

if (isset($option))
{
	switch($option)
	{
	case 'clearsearch':
		if (isset($_SESSION['search']))
		{
			unset($_SESSION['search']);
		}
		if (isset($_SESSION['page']))
		{
			unset($_SESSION['page']);
		}
		if (isset($search))
		{
			unset($search);
		}
		if (isset($page))
		{
			unset($page);
		}
		break;
	default:
		break;
	}
}

if (isset($option) && isset($type) && isset($target))
{
	$text = Modify_interactions($target, $user_id, $option, $type);
	echo '<br><font color="#eeeeee">' . $text . '</font>';
}

if (PHP_SELF == 'friends.php')
{
	$interactions = Get_interaction_list($user_id);
}
else
{
	$interactions = Get_interaction_list($user_id, $char['map_xpos'], $char['map_ypos']);
}

$player_list = array();

if (PHP_SELF != 'friends.php')
{
	// Display chat input box
	echo '
<form action="' . PHP_SELF . '?s=' . $s . '" method="post" name="chatbox">
<img src="' . PHPRPG_IMG . 'babble.png" width="10" height="10" border="0" alt="chat">
<input type="hidden" name="option" value="chat">
<input type="text" name="voice" maxlength="100" size="30" class="input" onFocus="javascript:textSwitch=1;">
&nbsp;[<a href="' . PHP_SELF . '?s=' . $s . '">Refresh</a>]
</form>
';


	// Select the users who have said something in this sector, sorted by the last posting time
	$player_list = array();
	$result = mysql_query("SELECT name, MAX(post_time) AS max_time FROM " . PHPRPG_DB_PREFIX . "_chat WHERE map_name='" . $char['map_name'] . "' AND map_xpos='" . $char['map_xpos'] . "' AND map_ypos='" . $char['map_ypos'] . "' GROUP BY name ORDER BY max_time DESC") or die('Database Error: ' . mysql_error() . '<br>');
	if (mysql_num_rows($result) > 0)
	{
		while ($player = mysql_fetch_array($result))
		{
			// See if player is within the sector
			$result_check = mysql_query("SELECT user_id, avatar FROM " . PHPRPG_DB_PREFIX . "_users WHERE name='" . $player['name'] . "' AND map_name='" . $char['map_name'] . "' AND map_xpos='" . $char['map_xpos'] . "' AND map_ypos='" . $char['map_ypos'] . "' LIMIT 1");

			if (mysql_num_rows($result_check) == 1)
			{
				// Yes, player is still within the sector, add to list of players
				$player_list[] = $player['name'];
				$player_avatar = mysql_fetch_array($result_check);

				// Display the latest chat messages of the particular player
				$result_messages = mysql_query("SELECT contents FROM " . PHPRPG_DB_PREFIX . "_chat WHERE name='" . $player['name'] . "' AND map_name='" . $char['map_name'] . "' AND map_xpos='" . $char['map_xpos'] . "' AND map_ypos='" . $char['map_ypos'] . "' ORDER BY post_time DESC LIMIT 6");

				echo '
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td></td>
<td><img src="' . PHPRPG_IMG . 'quote_ul.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_tp.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_ur.png" width="15" height="7" border="0" alt=""></td>
</tr>
<tr>
';

				$type_flag = '';

				$found_it = Search_sub_arrays($player_avatar['user_id'], $interactions);

				if (!($found_it === NULL))
				{
					$type_flag = $interactions[$found_it]['type_flag'];
					$friend = (($type_flag & INTERACTION_FRIEND) ? "1" : "0") . (($type_flag & INTERACTION_ENEMY) ? "1" : "0") . (($type_flag & INTERACTION_ATTACKER) ? "1" : "0");
				} else {
					$friend = "000";
				}


				$chat_player_string = '<td><table cellpadding="0" cellspacing="2" border="0"><tr><td><div align="center"><img src="' . PHPRPG_IMG . 'avatar_' . $player_avatar['avatar'] . '.png" width="49" height="49" border="0" alt="' . $player['name'] . '" menu="friend" pname="' . $player['name'] . '" userid="' . $player_avatar['user_id'] . '" friend="' . $friend . '"
				><br><font size="1">' . $player['name'] . '</div></td>';

				$found_it = Search_sub_arrays($player_avatar['user_id'], $interactions);
				if ($type_flag != '')
				{
					$chat_player_string .= '<td valign="top">';
					if ($type_flag & INTERACTION_FRIEND)
					{
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'is_friend.png" width="20" height="20" border="0" alt="Friend"><br>';
					} else {
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br>';
					}
					if ($type_flag & INTERACTION_ENEMY)
					{
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'is_enemy.png" width="20" height="20" border="0" alt="Enemy"><br>';
					} else {
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br>';
					}
					if ($type_flag & INTERACTION_ATTACKER)
					{
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'is_attacker.png" width="20" height="20" border="0" alt="Attacker"><br></td>';
					} else {
						$chat_player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br></td>';
					}
				}

				if (PHP_SELF == 'battle.php' && $player_avatar['user_id'] != $user_id)
				{
				$chat_player_string .= '
<td valign="top">
<a href="action.php?s=' . $s . '&option=attack&id=' . $player_avatar['user_id'] . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'action_attack.png" width="20" height="20" border="0" alt="Attack"></a><br>
<img src="' . PHPRPG_IMG . 'action_heal.png" width="20" height="20" border="0" alt="Heal"><br>
<img src="' . PHPRPG_IMG . 'action_use.png" width="20" height="20" border="0" alt="Use"><br></td>';
				}
				$chat_player_string .= '</tr></table></td>';
				echo $chat_player_string;

				echo '
<td background="' . PHPRPG_IMG . 'quote_lt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td>';

				$i = 0;
				while ($player_messages = mysql_fetch_array($result_messages))
				{
					switch ($i)
					{
					case 0:
						$font_color = 'eeeeee';
						break;
					case 1:
						$font_color = 'c6c6c6';
						break;
					case 2:
						$font_color = 'bbbbbb';
						break;
					case 3:
						$font_color = '969696';
						break;
					case 4:
						$font_color = '888888';
						break;
					case 5:
						$font_color = '666666';
						break;
					case 6:
						$font_color = '555555';
						break;
					}
					$i++;
					echo '<font size="1" color="#' . $font_color . '">&gt; ' . $player_messages['contents'] . '</font><br>';
				}
				mysql_free_result($result_messages);

				echo '
</td>
<td background="' . PHPRPG_IMG . 'quote_rt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
</tr>
<tr>
<td></td>
<td><img src="' . PHPRPG_IMG . 'quote_dl.png" width="15" height="7" border="0" alt=""></td>
<td background="' . PHPRPG_IMG . 'quote_bt.png"><img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" border="0"></td>
<td><img src="' . PHPRPG_IMG . 'quote_dr.png" width="15" height="7" border="0" alt=""></td>
</tr>
</table>
<img src="' . PHPRPG_IMG . 'x.png" width="1" height="1" vspace="5" border="0"><br>';

				}

			mysql_free_result($result_check);

		}
	}
}

// Display the rest of the players in sector, who are not talking
$result = mysql_query("SELECT COUNT(*) FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id !=$user_id AND map_name='". $map . "' AND map_xpos='" . $xpos . "' AND map_ypos='" . $ypos . "' ");

$row = mysql_fetch_row($result);
$num_players = $row[0];

$page_string = "";
$search_string = "";
$search_query = "";

$num_search = $num_players;


if (empty($search))
{
	if (!isset($_SESSION['search']))
	{
	$search = "";
	} else {
	$search = $_SESSION['search'];
	}
} else {
	if (!isset($_SESSION['search']))
	{
	$_SESSION['search'] = $search;
	$page = 0;
	} else if ($search !=$_SESSION['search'])
	{
	$_SESSION['search'] = $search;
	$page = 0;
	}
}

if (empty($page))
{
	if (!isset($_SESSION['page']))
	{
	$page = 0;
	} else {
	$page = $_SESSION['page'];
	}
} else {
	if (!isset($_SESSION['page']))
	{
	$_SESSION['page'] = $page;
	} elseif ($page != $_SESSION['page'])
	{
	$_SESSION['page'] = $page;
	}
}

if ($num_search > MAX_USERS_PER_TILE)
{
	$page_string = " LIMIT " . $page * MAX_USERS_PER_TILE . ", " . MAX_USERS_PER_TILE;
}

if ($search != "")
{
	$search_query = " AND name LIKE '" . $search . "%' ";

	$result = mysql_query("SELECT COUNT(*) FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id !=$user_id AND map_name='". $map . "' AND map_xpos='" . $xpos . "' AND map_ypos='" . $ypos . "' " . $search_query);

	$row = mysql_fetch_row($result);
	$num_search = $row[0];
}

if ($num_players > 0)
{
	$main_loop = 0;

	while($main_loop < 2)
	{
		if ($main_loop == 0)
		{
			$row_count = count($interactions);
			if ($row_count == 0 && PHP_SELF == 'friends.php')
			{
				echo 'Your friends list is empty!<br><br>';
			}
		} else {
			if (PHP_SELF == 'friends.php')
			{
				$main_loop = 2;
				continue;
			}
			if (PHPRPG_DEBUG) echo $num_search . ' are standing here<BR>';

			$result = mysql_query("SELECT user_id, name, avatar FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id !=$user_id AND map_name='" . $map . "' AND map_xpos='" . $xpos . "' AND map_ypos='" . $ypos . "' " . $search_query . " ORDER BY name" . $page_string);
			$row_count = mysql_num_rows($result);
		}
		$cur_row = 0;
		$width_on_row = 0;
		echo '<table cellpadding="1" cellspacing="0" border="0"><tr>';
		while ($cur_row < $row_count)
		{
			if ($main_loop == 0)
			{
				$player = $interactions[$cur_row];
			} else {
				$player = mysql_fetch_array($result);
			}
			$cur_width = 0;

			if (in_array($player['name'], $player_list) == False)
			{
				$player_list[] = $player['name'];

				$type_flag = '';

				if (array_key_exists('type_flag', $player))
				{
					$type_flag = $player['type_flag'];
					$friend = (($type_flag & INTERACTION_FRIEND) ? "1" : "0") . (($type_flag & INTERACTION_ENEMY) ? "1" : "0") . (($type_flag & INTERACTION_ATTACKER) ? "1" : "0");
				} else {
					$friend = "000";
				}



				$player_string = '<td><div align="center"><img src="' . PHPRPG_IMG . 'avatar_' . $player['avatar'] . '.png" width="49" height="49" border="0" alt="' . $player['name'] . '" menu="friend" pname="' . $player['name'] . '" userid="' . $player['user_id'] . '" friend="' . $friend . '"><br><font size="1">' . $player['name'] . '</div></td>';
				$cur_width += WIDTH_AVATAR_ICON;

				if ($type_flag != '')
				{
					$player_string .= '<td valign="top">';
						if ($type_flag & INTERACTION_FRIEND)
					{
							$player_string .= '<img src="' . PHPRPG_IMG . 'is_friend.png" width="20" height="20" border="0" alt="Friend"><br>';
					} else {
						$player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br>';
					}
					if ($type_flag & INTERACTION_ENEMY)
					{
						$player_string .= '<img src="' . PHPRPG_IMG . 'is_enemy.png" width="20" height="20" border="0" alt="Enemy"><br>';
					} else {
						$player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br>';
					}
					if ($type_flag & INTERACTION_ATTACKER)
					{
						$player_string .= '<img src="' . PHPRPG_IMG . 'is_attacker.png" width="20" height="20" border="0" alt="Attacker"><br></td>';
					} else {
						$player_string .= '<img src="' . PHPRPG_IMG . 'x.png" width="20" height="20" border="0"><br>';
					}
				$cur_width += WIDTH_INTERACTION;
				}

				if (PHP_SELF == 'battle.php' && $player['user_id'] != $user_id)
				{
					$player_string = '
<td valign="top"><table cellpadding="0" cellspacing="2" border="0"><tr>' . $player_string . '
<td valign="top">
<a href="action.php?s=' . $s . '&option=attack&id=' . $player['user_id'] . '&ref=' . PHP_SELF . '"><img src="' . PHPRPG_IMG . 'action_attack.png" width="20" height="20" border="0" alt="Attack"></a><br>
<img src="' . PHPRPG_IMG . 'action_heal.png" width="20" height="20" border="0" alt="Heal"><br>
<img src="' . PHPRPG_IMG . 'action_use.png" width="20" height="20" border="0" alt="Use"><br></td>';

			$cur_width += WIDTH_BATTLE_ICON;
			$player_string .= '</tr></table></td>';
				}
				if (($cur_width + $width_on_row) >= $wca)
				{
					$player_string = '</tr></table><table cellpadding="1" cellspacing="0" border="0"><tr>' . $player_string;
					$width_on_row = 0;
				}

				echo $player_string;
				$width_on_row += $cur_width;
			}
			$cur_row++;
		}
		echo '</tr></table>';
		$main_loop++;
	} // End main loop

	if (PHP_SELF != 'friends.php')
	{
		if ($num_search > MAX_USERS_PER_TILE)
		{

			$cur_set = floor($page/10) * 10;
			$cur_page = $cur_set;
			$max_page = floor($num_search/MAX_USERS_PER_TILE) + 1;
			if ($cur_set/10 == floor($max_page/10))
			{
			$page_top = $cur_set + fmod($max_page, 10);
			} else {
				$page_top = $cur_set + 10;
			}

			echo '
<table cellpadding="0" cellspacing="0" border="0"><tr><td><font size="2">Page:&nbsp ';

			if ($cur_set/10 > 1)
			{
			echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=0">Home</a>&nbsp ';
			}
			if ($cur_set/10 > 0)
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($cur_set - 10) . '">' . ($cur_set - 9) . '-' . $cur_set . '</a>&nbsp ';
			}
			if ($page > 0)
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($page - 1) . '">Prev</a>&nbsp ';
			}
			while ($cur_page < $page_top)
			{
				if ($page == $cur_page)
				{
					echo ($page + 1) . '&nbsp ';
				} else {
					echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . $cur_page . '">' . ($cur_page+1) . '</a>&nbsp ';
				}
				$cur_page++;
			}
			if ($page < $max_page-1)
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($page + 1) . '">Next</a>&nbsp ';
			}
			if ($cur_set/10 < floor($max_page/10) && ($cur_set/10 < floor($max_page/10)-1))
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($cur_set + 10) . '">' . ($cur_set+11) . '-' . ($cur_set+20) . '</a>&nbsp ';
			}
			if ($cur_set/10 < floor($max_page/10) && ($cur_set/10 == floor($max_page/10)-1) && ($cur_set + 11 != $max_page))
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($cur_set + 10) . '">' . ($cur_set+11) . ' - ' . $max_page . '</a>&nbsp ';
			}
			if ($cur_set/10 < floor($max_page/10) && ($cur_set/10 == floor($max_page/10)-1) && ($cur_set + 11 == $max_page))
			{
				echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . ($cur_set + 10) . '">' . $max_page . '</a>&nbsp ';
			}
			if ($cur_set/10 < floor($max_page/10)-1)
			{
			echo '<a href="' . PHP_SELF . '?s=' . $s . '&page=' . floor($max_page/10) . '0">End</a>&nbsp ';
			}
			echo '</font></td></tr></table>';
		}

		if ($num_players > MAX_USERS_PER_TILE)
		{
			echo '
<form action="' . PHP_SELF . '?s=' . $s . '" method="post" name="searchbox">
Search:&nbsp
<input type="text" name="search" value="' . $search . '" class="input" maxlength="20" size="20" onFocus="javascript:textSwitch=2;">';
			if ($search != "")
			{
echo '&nbsp[<a href="' . PHP_SELF . '?s=' . $s . '&option=clearsearch">Clear the Search</a>]';
			}
echo '</form>';
		}
	}
}


?>