<?php
/******************************************************************************/
/*																			*/
/* lib_SESSION.inc.php - Validates current session and updates the database	*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 17 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

if (!empty($_SESSION['user_id'])) unset($_SESSION['user_id']);

if (empty($s)) $s = Retrieve_var('s');

session_set_save_handler('SessOpen', '', 'SessRead', 'SessWrite', 'SessDestroy', 'SessGc');
session_id($s);
session_start();

if (!empty($_SESSION['user_id']))
{
	$user_id = $_SESSION['user_id'];
	$user_time_old = $_SESSION['user_time'];
	$user_time_min = time() - PHPRPG_SESSION_EXPIRY;

	// Redirect to default page if session has expired
	if ($user_time_old < $user_time_min)
	{
		session_unset();
		session_destroy();

		if (PHPRPG_DEBUG_AUTOREDIRECT)
		{
			echo 'Session has expired!<br>';
			echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
		} else {
			header("Location: index.php");
		}
		exit;
	}

	// Continue with session
	$user_time = time();
	DbConnect();
	// Update last_active
	$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET last_active='$user_time' WHERE user_id=$user_id LIMIT 1");

// I have no idea how long it's been like this.  User_time is getting updated for
// the game_users array but not for game_sessions.  This means that after 20 minutes of
// Creating the session to begin with it times out.  Doesn't matter how busy you've been.
	$_SESSION['user_time'] = $user_time;

	// Refresh stats
	$result = mysql_query("SELECT name, HP, HP_MAX, MP, MP_MAX, STM, STM_MAX, EXP, GP, map_name, map_xpos, map_ypos, delay, delay_reason, race, avatar FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$user_id LIMIT 1");
	$char = mysql_fetch_assoc($result);

	// Check admin level
	$result = mysql_query("SELECT level FROM " . PHPRPG_DB_PREFIX . "_admins WHERE user_id=" . $user_id . " LIMIT 1");
	list($char['admin_level']) = mysql_fetch_row($result);
}
else
{
	if (PHP_SELF != 'index.php')
	{
		// Redirect to default page if session variables do not exist and we are accessing somewhere
		// besides index.php
		if (PHPRPG_DEBUG_AUTOREDIRECT)
		{
			echo 'Session variables do not exist!<br>';
			echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
		} else {
			header("Location: index.php");
		}
		exit;
	}
}

/*																			*/
/* SessOpen	-															 */
/* SessRead	- A set of functions which enables the use of MySQL as the	*/
/* SessWrite	- storage module for sessions. Functions are registered by	*/
/* SessDestroy - calling: session_set_save_handler()						 */
/* SessGc	 -															 */

function SessOpen($save_path, $sess_name)
{
	$link = mysql_connect(PHPRPG_DB_HOST, PHPRPG_DB_USER, PHPRPG_DB_PASS) or die(mysql_errno() . ' - ' . mysql_error());
	mysql_select_db(PHPRPG_DB_NAME) or die(mysql_error());

	return True;
}


function SessRead($sess_id)
{
	$result = mysql_query("SELECT data FROM " . PHPRPG_DB_PREFIX . "_sessions WHERE id = '$sess_id'") or die(mysql_error());

	if(mysql_num_rows($result) == 0)
	{
		return '';
	}
	else
	{
		$row = mysql_fetch_array($result);
		mysql_free_result($result);
		return $row["data"];
	}
}


function SessWrite($sess_id, $val)
{
	$result = mysql_query("REPLACE INTO " . PHPRPG_DB_PREFIX . "_sessions VALUES ('$sess_id', '$val', null)") or die(mysql_error());

	return true;
}


function SessDestroy($sess_id)
{
	$result = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_sessions WHERE id = '$sess_id'") or die(mysql_error());

	return True;
}


function SessGc($max_lifetime)
{
	$expiry = time() - $max_lifetime;

	$result = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_sessions WHERE UNIX_TIMESTAMP(time_stamp) < $expiry") or die(mysql_error());

	return True;
}


?>