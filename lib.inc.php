<?php
/******************************************************************************/
/*																			  */
/* lib.inc.php - Function libraries											  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 3 June 2001														  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/


/*----------------------------------------------------------------------------*/
/* Game - Functions required for game script processing						*/
/*----------------------------------------------------------------------------*/


/*																			 */
/* ParseCommands - Parse the commands in the chat if they start with a slash	*/

function ParseCommands($what) {
	// remove the slash
	if (substr($what, 0, 1) == "/")
		$what = substr($what, 1);
	// tokenize the string by spaces
	$str = explode(" ", $what);
	if ($str[0]) {
		// check all the first words
		switch ($str[0]) {
			case 'give': // give <name> [number] <object>
				if (!empty($str[1]) && !empty($str[2])) {
					$name = $str[1];
					if (empty($str[3]))
						$object = $str[2];
					else {
						$number = $str[2];
						$object = $str[3];
					}
				} else { // invalid command

				}
				break;
			case 'attack': // attack <name or creature>
				break;
			case 'steal': //
				break;
			case 'use': // use <item>
				break;
			case 'open': // open <door | chest | window | etc.>
				break;
			case 'enter': // enter <city | shop | etc.>
				break;
			// bank stuff
			case 'deposit':
				break;
			case 'withdraw':
				break;
			// shop stuff
			case 'buy':
				break;
			case 'sell':
				break;
			case 'identify':
				break;
			// admin
			case 'whereis': // whereis <playername>
				break;
			case 'teleport': // teleport <x> <y>
				break;
			// end admin
			// directions (these all fallthrough)
			case 'north':
			case 'n':
			case 'u':
			case 'up':
				break;
			case 'northeast':
			case 'ne':
			case 'ur':
			case 'upright':
				break;
			case 'southeast':
			case 'se':
			case 'dr':
			case 'downright':
				break;
			case 'south':
			case 's':
			case 'd':
			case 'down':
				break;
			case 'southwest':
			case 'sw':
			case 'dl':
			case 'downleft':
				break;
			case 'northwest':
			case 'nw':
			case 'ul':
			case 'upleft':
				break;
			// end directions
		} // switch $str[0]
	} // if $str[0]
}


/*																			*/
/* string GameDate($dateSeconds, $dateOption) -								*/
/*	Returns information relating to game time system						*/
/*	$dateSeconds is the number of seconds since Epoch, current time is	*/
/*		used if not specified											 */
/*	$dateOption include other options specified further below			 */

function GameDate($dateSeconds = '', $dateOption = '')
{

	// Game time is 4x faster than real life, with four cycles of day and night per real day

	if ($dateSeconds == '') { $dateSeconds = time(); }
	$seconds_since_epoch =  $dateSeconds - PHPRPG_EPOCH;

	$days_since_epoch = floor($seconds_since_epoch / (60 * 60 * 24));
	$seconds_into_the_day = $seconds_since_epoch % (60 * 60 * 24);

	switch ($dateOption)
	{
		case 'game_cycle':
			$game_seconds = $seconds_into_the_day * 4;
			$game_cycle = floor($game_seconds / (60 * 60 * 24)) + 1;

			return $game_cycle;
			break;

		case 'game_seconds_into_game_cycle':
			$game_seconds = $seconds_into_the_day * 4;
			$game_seconds_into_game_cycle = $game_seconds % (60 * 60 * 24);

			return $game_seconds_into_game_cycle;
			break;

		default:
			// Date is represented according to the format:
			//	e.g. 14:23 3rd cycle (green) of day 5

			$game_date = $days_since_epoch;
			$game_seconds = $seconds_into_the_day * 4;
			$game_cycle = floor($game_seconds / (60 * 60 * 24)) + 1;
			$game_seconds_into_game_cycle = $game_seconds % (60 * 60 * 24);
			$game_hour = floor($game_seconds_into_game_cycle / (60 * 60));
			$game_seconds_into_game_hour = $game_seconds_into_game_cycle % (60 * 60);
			$game_minute = floor($game_seconds_into_game_hour / 60);

			// Prepend a '0' if $game_minute is only one digit
			$print_minute = trim($game_minute);
			if (strlen($print_minute) == 1)
			{
				$print_minute = '0' . $print_minute;
			}

			// Determine the period of the day so that an appropriate sundial image can be displayed
			if ($game_hour >= 0 && $game_hour <= 6)
			{
				$img_string = 'dawn';
			}
			elseif ($game_hour >= 7 && $game_hour <= 12)
			{
				$img_string = 'day';
			}
			elseif ($game_hour >= 13 && $game_hour <= 18)
			{
				$img_string = 'dusk';
			}
			elseif ($game_hour >= 19 && $game_hour <= 24)
			{
				$img_string = 'night';
			}

			switch ($game_cycle)
			{
				case 1:
					$print_cycle = '1st Cycle';
					break;
				case 2:
					$print_cycle = '2nd Cycle';
					break;
				case 3:
					$print_cycle = '3rd Cycle';
					break;
				case 4:
					$print_cycle = '4th Cycle';
					break;
				default:
					$print_cycle = 'Invalid Cycle!';
			}
			$time_html = '
<div align="center" nowrap><img src="' . PHPRPG_IMG . 'time_' . $game_cycle . '_' . $img_string . '.png" height="25" width="86" border="0" alt=""><br><img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="4" border="0"></div>
<div align="center" nowrap><font size="1">' . $print_cycle . ' of Day ' . $game_date . ' ' . $game_hour . ':' . $print_minute . '</font></div>
			';
			return $time_html;
	}

}

// If for whatever reason you want to block the debug variables from showing
// set $rvblock to a value before your include/requires.  For example, the
// Time Calculator in events.php where if you open the calculator it shouldn't
// be shown.
// Added ability to select a certain place to retrieve from.  This way you can
// still see the debug output but force that variable to come from that location
// only.  Security purposes.
function Retrieve_var($getit, $from = '_REQUEST')
{
	global $rvblock;

	$newvar = NULL;
	switch($from)
	{
	case '_REQUEST':
		if (isset($_REQUEST[$getit]))
		{
			$newvar = $_REQUEST[$getit];
		}
		break;
	case '_POST':
		if (isset($_POST[$getit]))
		{
			$newvar = $_POST[$getit];
		}
		break;
	case '_GET':
		if (isset($_GET[$getit]))
		{
			$newvar = $_GET[$getit];
		}
		break;
	case '_COOKIE':
			if (isset($_COOKIE[$getit]))
		{
			$newvar = $_COOKIE[$getit];
		}
		break;
	default:
		break;
	}

	if (PHPRPG_DEBUG_RV_SHOW && !isset($rvblock))
	{
		echo '$' . $getit . ' = ' . (($newvar != NULL) ? $newvar : "NULL") . '<BR>';
	}

	return $newvar;
}

/* Function used to look through an array of arrays for a value. */
function Search_sub_arrays($needle, $haystack)
{
	$array_count = count($haystack);
	$walk_array = 0;
	while($walk_array < $array_count)
	{
		$array_key = array_search($needle, $haystack[$walk_array]);
		if ($array_key != NULL) return $walk_array;
		$walk_array++;
	}
	return NULL;
}


/* END of Game function library												*/
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/* System - Functions not directly related to the game code					*/
/*----------------------------------------------------------------------------*/

/*																			*/
/* DbConnect - Establish MySQL database connection if not already so		 */

function DbConnect()
{
	global $db;
	$db = mysql_connect(PHPRPG_DB_HOST, PHPRPG_DB_USER, PHPRPG_DB_PASS) or die(mysql_error());
	mysql_select_db(PHPRPG_DB_NAME) or die(mysql_error());
	return($db);
}


/*																			*/
/* string StartTiming - Initiates script execution timer by returning a		*/
/*					 time marker											*/

function StartTiming()
{
	$micro_time = microtime();
	$time_parts = explode(" ",$micro_time);
	$start_time = $time_parts[1] . substr($time_parts[0],1);
	return $start_time;
}


/*																			*/
/* string StopTiming($startTime) - Stops timer and returns execution time	*/
/*								$startTime is the time marker value		*/

function StopTiming($startTime)
{
	$micro_time = microtime();
	$time_parts = explode(" ",$micro_time);
	$end_time = $time_parts[1] . substr($time_parts[0],1);
	// round up and convert from sec to msec
	$exec_time = round($end_time - $startTime, 5) * 1000;
	return $exec_time;
}


/*																			*/
/* string HostIdentify - Return visitor's host information					*/
/*						Script extracted from: http://www.cgsa.net/php		*/

function HostIdentify()
{
	/*if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
		// case 1.A: proxy && HTTP_X_FORWARDED_FOR is defined
		$b = ereg("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $_SERVER['HTTP_X_FORWARDED_FOR'], $array);
		if ($b && (count($array) >= 1)) {
			return (gethostbyaddr($array[0]));
		} else {
			return ($_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['HTTP_VIA'] . '_' . $_SERVER['HTTP_X_FORWARDED_FOR']);
		}
	} else if ($_SERVER['HTTP_X_FORWARDED']) {
		// case 1.B: proxy && HTTP_X_FORWARDED is defined
		$b = ereg("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $_SERVER['HTTP_X_FORWARDED'], $array);
		if ($b && (count($array) >= 1)) {
			return (gethostbyaddr($array[0]));
		} else {
			return ($_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['HTTP_VIA'] . '_' . $_SERVER['HTTP_X_FORWARDED']);
		}
	} else if ($_SERVER['HTTP_FORWARDED_FOR']) {
		// case 1.C: proxy && HTTP_FORWARDED_FOR is defined
		$b = ereg("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $_SERVER['HTTP_FORWARDED_FOR'], $array);
		if ($b && (count($array) >= 1)) {
			return (gethostbyaddr($array[0]));
		} else {
			return ($_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['HTTP_VIA'] . '_' . $_SERVER['HTTP_FORWARDED_FOR']);
		}
	} else if ($_SERVER['HTTP_FORWARDED']) {
		// case 1.D: proxy && HTTP_FORWARDED is defined
		$b = ereg("^([0-9]{1,3}\.){3,3}[0-9]{1,3}", $_SERVER['HTTP_FORWARDED'], $array);
		if ($b && (count($array) >= 1)) {
			return (gethostbyaddr($array[0]));
		} else {
			return ($_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['HTTP_VIA'] . '_' . $_SERVER['HTTP_FORWARDED']);
		}
	} else if ($_SERVER['HTTP_VIA']) {
		// case 2: proxy && HTTP_(X_) FORWARDED (_FOR) not defined && HTTP_VIA defined
		// other exotic variables may be defined
		return ($_SERVER['HTTP_VIA'] . '_' . $_SERVER['HTTP_X_COMING_FROM'] . '_' . $_SERVER['HTTP_COMING_FROM']);
	} else if($_SERVER['HTTP_X_COMING_FROM'] || $_SERVER['HTTP_COMING_FROM']) {
		// case 3: proxy && only exotic variables defined
		// the exotic variables are not enough, we add the REMOTE_ADDR of the proxy
		return ($_SERVER['REMOTE_ADDR'] . '_' . $_SERVER['HTTP_X_COMING_FROM'] . '_' . $_SERVER['HTTP_COMING_FROM']);
	} else {
		// case 4: no proxy
		// or tricky case: proxy + refresh
		return (gethostbyaddr($_SERVER['REMOTE_ADDR']));
	}*/

	return (gethostbyaddr($_SERVER['REMOTE_ADDR']));
}

/* END of System function library											*/
/*----------------------------------------------------------------------------*/
?>