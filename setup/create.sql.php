<?php

function database_create($db) {
#
# Host: localhost
# Generation Time: Sep 19, 2003 at 10:00 PM
# Server version: 3.23.51
# PHP Version: 4.2.3
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_activity
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_activity (
  day   varchar(12)  NOT NULL default '',
  hour  tinyint(3)   NOT NULL default '0',
  agent varchar(255) NOT NULL default '',
  host  varchar(255) NOT NULL default '',
  ref   text         NOT NULL
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_admins
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_admins (
  user_id smallint(5) unsigned NOT NULL default '0',
  level   tinyint(3)           NOT NULL default '0'
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_battles
#
mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_battles (
  type        char(10)             NOT NULL default '',
  attacker_id smallint(5) unsigned NOT NULL default '0',
  target_id   smallint(5) unsigned NOT NULL default '0',
  map_name    char(20)             NOT NULL default '',
  map_xpos    tinyint(3)  unsigned NOT NULL default '0',
  map_ypos    tinyint(3)  unsigned NOT NULL default '0',
  contents    char(100)            NOT NULL default '',
  post_time   char(255)            NOT NULL default ''
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_chat
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_chat (
  type      char(10)             NOT NULL default '',
  avatar    char(10)             NOT NULL default '',
  emotion   char(3)              NOT NULL default '',
  name      char(20)             NOT NULL default '',
  map_name  char(20)             NOT NULL default '',
  map_xpos  tinyint(3) unsigned  NOT NULL default '0',
  map_ypos  tinyint(3) unsigned  NOT NULL default '0',
  contents  char(100)            NOT NULL default '',
  post_time char(255)            NOT NULL default ''
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_chat_log
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_chat_log (
  avatar    char(10)            NOT NULL default '',
  emotion   char(3)             NOT NULL default '',
  name      char(20)            NOT NULL default '',
  map_name  char(20)            NOT NULL default '',
  map_xpos  tinyint(3) unsigned NOT NULL default '0',
  map_ypos  tinyint(3) unsigned NOT NULL default '0',
  contents  char(100)           NOT NULL default '',
  post_time char(255)           NOT NULL default ''
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_events
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_events (
  task        char(255)            NOT NULL default '',
  priority    tinyint(3)  unsigned NOT NULL default '5',
  requested   char(255)            NOT NULL default '',
  due         char(255)            NOT NULL default '',
  frequency   bigint(7) unsigned   NOT NULL default '0',
  assigned_id smallint(5) unsigned NOT NULL default '0'
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_items
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_items (
  id        mediumint(8) unsigned NOT NULL auto_increment,
  user_id   smallint(5)  unsigned NOT NULL default '0',
  ref_id    mediumint(8) unsigned NOT NULL default '0',
  ident     char(255)             NOT NULL default '',
  used      char(20)              NOT NULL default '',
  indx      smallint(5)           NOT NULL default '0',
  deviation tinyint(3)            NOT NULL default '0',
  mode      char(20)              NOT NULL default '',
  weight    smallint(5)           NOT NULL default '0',
  curse     char(20)              NOT NULL default '',
  img       char(20)              NOT NULL default '',
  map_name  char(20)              NOT NULL default '',
  map_xpos  tinyint(3)   unsigned NOT NULL default '0',
  map_ypos  tinyint(3)   unsigned NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_items_factsheet
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_items_factsheet (
  id         mediumint(8) unsigned NOT NULL default '0',
  name       char(255)             NOT NULL default '',
  type       char(20)              NOT NULL default '',
  quantity   mediumint(8)          NOT NULL default '1',
  indx       smallint(5)           NOT NULL default '0',
  deviation  tinyint(3)            NOT NULL default '0',
  mode       char(20)              NOT NULL default '',
  weight     smallint(5)           NOT NULL default '0',
  gender     set('M','F')          NOT NULL default '',
  race       set('soul','human')   NOT NULL default '',
  profession set('none')           NOT NULL default '',
  curse      char(20)              NOT NULL default '',
  img        char(20)              NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_map
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_map (
  name    char(20)            NOT NULL default '',
  xpos    tinyint(3) unsigned NOT NULL default '0',
  ypos    tinyint(3) unsigned NOT NULL default '0',
  move_up char(3)             NOT NULL default 'N',
  move_ur char(3)             NOT NULL default 'N',
  move_dr char(3)             NOT NULL default 'N',
  move_dn char(3)             NOT NULL default 'N',
  move_dl char(3)             NOT NULL default 'N',
  move_ul char(3)             NOT NULL default 'N',
  type    char(20)            NOT NULL default '',
  subtype char(10)            NOT NULL default '',
  region  char(20)            NOT NULL default ''
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_sessions
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_sessions (
  id         char(32)      NOT NULL default '',
  data       char(255)     NOT NULL default '',
  time_stamp timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  KEY t_stamp (time_stamp)
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_users
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_users (
  user_id      smallint(5)  unsigned NOT NULL auto_increment,
  user_name    char(20)              NOT NULL default '',
  user_pass    char(32)     binary   NOT NULL default '',
  email        char(60)              NOT NULL default '',
  name         char(20)              NOT NULL default '',
  status       char(20)              NOT NULL default '',
  HP           mediumint(8) unsigned NOT NULL default '20',
  HP_MAX       mediumint(8) unsigned NOT NULL default '20',
  MP           mediumint(8) unsigned NOT NULL default '10',
  MP_MAX       mediumint(8) unsigned NOT NULL default '10',
  STM          mediumint(8) unsigned NOT NULL default '200',
  STM_MAX      mediumint(8) unsigned NOT NULL default '200',
  EXP          mediumint(8) unsigned NOT NULL default '0',
  GP           mediumint(8) unsigned NOT NULL default '0',
  STR          tinyint(3)   unsigned NOT NULL default '4',
  NTL          tinyint(3)   unsigned NOT NULL default '4',
  PIE          tinyint(3)   unsigned NOT NULL default '4',
  VIT          tinyint(3)   unsigned NOT NULL default '4',
  DEX          tinyint(3)   unsigned NOT NULL default '4',
  SPD          tinyint(3)   unsigned NOT NULL default '4',
  CW           mediumint(8) unsigned NOT NULL default '0',
  CC           mediumint(8) unsigned NOT NULL default '80',
  map_name     char(20)              NOT NULL default '',
  map_xpos     tinyint(3)   unsigned NOT NULL default '0',
  map_ypos     tinyint(3)   unsigned NOT NULL default '0',
  last_active  char(255)             NOT NULL default '',
  delay        char(255)             NOT NULL default '',
  delay_reason char(255)             NOT NULL default '',
  race         char(10)              NOT NULL default '',
  avatar       char(10)              NOT NULL default '',
  host         char(255)             NOT NULL default '',
  friends      char(255)             NOT NULL default '',
  enemies      char(255)             NOT NULL default '',
  attackers    char(255)             NOT NULL default '',
  PRIMARY KEY  (user_id)
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_users_pending
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_users_pending (
  user_name char(20)  NOT NULL default '',
  user_pass char(20)  NOT NULL default '',
  name      char(20)  NOT NULL default '',
  email     char(60)  NOT NULL default '',
  validate  char(10)  NOT NULL default '',
  rego_time char(255) NOT NULL default ''
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# --------------------------------------------------------

#
# Table structure for table PHPRPG_DB_PREFIX_users_pending
#

mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_weather (
  id          int(11)     NOT NULL auto_increment,
  region      varchar(32) NOT NULL default '0',
  currently   varchar(32) NOT NULL default '',
  wind        smallint(6) NOT NULL default '0',
  wind_dir    varchar(32) NOT NULL default '',
  high        smallint(6) NOT NULL default '0',
  low         smallint(6) NOT NULL default '0',
  season      varchar(32) NOT NULL default '',
  last_change int(14)     NOT NULL default '0',
  PRIMARY KEY  (id)
) TYPE=MyISAM",$db);

if (mysql_error()) echo mysql_error()."<br>";
# -------------------------------------------------------
}


?>