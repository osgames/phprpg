<?php

require_once('..\config.inc.php');

error_reporting (E_ALL);

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

function LI()
{
	return ULLI('LI');
}

function UL()
{
	return ULLI('UL');
}

function cLI()
{
	return ULLI('/');
}

function cUL()
{
	return ULLI('/');
}

// Making lists suck so I made a function to handle doing the tabbing and cleanup.
function ULLI($option, $to = '0')
{
	static $tablevel = 0;
	static $stack = array();

	$retval = "";
	switch($option)
	{
	case 'LI':
		array_push($stack, "LI");
		$retval = str_repeat("\t", $tablevel) . "<LI>";
		break;
	case 'UL':
		array_push($stack, "UL");
		$tablevel++;
		$retval = str_repeat("\t", $tablevel) . "<UL>\n";
		break;
	case '/':
		$close = array_pop($stack);
		if ($close == "UL")
		{
			$retval = str_repeat("\t", $tablevel);
			$tablevel--;
		}
		$retval .= ("</" . $close . ">\n");
		break;
	case 'die':
		for ($i = count($stack); $i > $to; $i--)
		{
			$close = array_pop($stack);
			if ($close == "UL")
			{
				$retval .= str_repeat("\t", $tablevel);
				$tablevel--;
			}
			$retval .= ("</" . $close . ">\n");

		}
		break;
	default:
		$retval = "";
	}
	return $retval;
}


function Create_db_layout_array()
{
	$tables = array();

	$result = mysql_query("SHOW TABLES");

	while ($table = mysql_fetch_array($result))
	{
		$tables[] = array('name' => $table[0], 'data' => '');
	}

	for ($i = 0; $i < count($tables); $i++)
	{
		$name = $tables[$i]['name'];

		$result = mysql_query("DESCRIBE " . $name);

		$order = 0;
		while($row = mysql_fetch_array($result))
		{
			$tables[$i]['data'][$row['Field']] = $order;
			$order++;
		}
	}
	return $tables;
}

$database = Create_db_layout_array();


if (Search_sub_arrays(PHPRPG_DB_PREFIX . '_settings', $database) === NULL)
{
	// _settings hasn't been created yet.  Version pre-0.5.42
	$dbversion = 'pre-0.5.42';
	$database = Create_db_layout_array();
} else {
	$result = mysql_query("SELECT data FROM " . PHPRPG_DB_PREFIX . "_settings WHERE (type='version' AND name='database' LIMIT 1");
	$ver = mysql_fetch_array($result);
	$dbversion = $ver['data'];
}

echo "<BR>Database Version: " . $dbversion . "<BR>\n";
echo UL();

$sqlerror = 'No';

$OK = "<b><font color='green'>OK</font></b>";
$FAILED = "<b><font color='red'>FAILED</font></b>";

// Find number of updates we have to do and use that to set the php timeout.
// 30 seconds per level of updates.  Hope it's enough.
// $avail_ver is coming from index.php, and it obviously needs to go in order
// of oldest first to newest last.
$from = array_search($dbversion, $avail_ver);
$to = array_search(PHPRPG_DATABASE_VERSION, $avail_ver);

if (!($from === NULL) && !($to === NULL))
	set_time_limit(30 * ($to - $from));

// Meat and potatoes part of the updater.
// Versions go from oldest to newest and fall through.  That way
// If it's 4 versions old it gets the updates from all four newer
// Versions.
switch($dbversion)
{
default:
	echo 'Error updating the database!<BR>';
	echo 'Old version not upgradable using auto-updater.<BR>';
	break;
case 'pre-0.5.42':
	// This is the odd one.  Due to the fact that we have no clue
	// which version we are updating from we just look for the updates
	// piece by piece so that someone how got a rogue cvs version before
	// the auto-updater get only the pieces they didn't have.

	echo LI() . "Updating from pre-0.5.42 to 0.5.42:" . cLI();
	// Check for frequency in event system.
	$db_exist = Search_sub_arrays(PHPRPG_DB_PREFIX . '_events' , $database);

	$events = $database[$db_exist]['data'];
	if (empty($events['frequency']))
	{
		echo UL() . LI() . "Adding frequency to " . PHPRPG_DB_PREFIX . '_events: ';

		mysql_query("ALTER TABLE `" . PHPRPG_DB_PREFIX . "_events` ADD `frequency` BIGINT( 7 ) UNSIGNED DEFAULT '0' NOT NULL AFTER `due`") or $sqlerror = 'Yes';


//ALTER TABLE `game_events` ADD `frequency` BIGINT( 7 ) UNSIGNED DEFAULT '0' NOT NULL AFTER `due`
		if ($sqlerror == 'Yes')	continue;

		echo $OK . cLI();

		$freq_array = array('task' => array('health_recovery', 'stamina_recovery', 'clear_chat', 'clear_battle_messages', 'clear_users_pending', 'optimize_chat', 'optimize_sessions', 'optimize_battles', 'optimize_users_pending'),  'frequency' => array('300', '300', '60', '3600', '3600', '1200', '86400', '86400', '172800'));

		echo UL();
		for ($i = 0; $i < count($freq_array['task']); $i++)
		{
			echo LI() . 'Setting default value for task ' . $freq_array['task'][$i] . ': ';

			mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET frequency='" . $freq_array['frequency'][$i] . "' WHERE task='" . $freq_array['task'][$i] . "' LIMIT 1") or $sqlerror = 'Yes';

			if ($sqlerror == 'Yes') continue;

			echo $OK . cLI();
		}
		echo cUL() . cUL();

	}

	// Check for the weather database.
	$db_exist = Search_sub_arrays(PHPRPG_DB_PREFIX . '_weather' , $database);

	if ($db_exist === NULL)
	{
		echo UL() . LI() . "Creating database: " . PHPRPG_DB_PREFIX . "_weather: ";

		mysql_query("CREATE TABLE ".PHPRPG_DB_PREFIX."_weather (
		  id          int(11)     NOT NULL auto_increment,
		  region      varchar(32) NOT NULL default '0',
		  currently   varchar(32) NOT NULL default '',
		  wind        smallint(6) NOT NULL default '0',
		  wind_dir    varchar(32) NOT NULL default '',
		  high        smallint(6) NOT NULL default '0',
		  low         smallint(6) NOT NULL default '0',
		  season      varchar(32) NOT NULL default '',
		  last_change int(14)     NOT NULL default '0',
		  PRIMARY KEY  (id)
		) TYPE=MyISAM",$db) or $sqlerror = 'Yes';

		if ($sqlerror == 'Yes') continue;

		echo $OK . cLI();

		echo UL() . LI() . "Entering first database entry: ";


		mysql_query("INSERT INTO ".PHPRPG_DB_PREFIX."_weather (`id`, `region`, `currently`, `wind`, `wind_dir`, `high`, `low`, `season`, `last_change`) VALUES (1, '', 'Partly Cloudy', 4, 'se', 68, 53, 'Spring', 1065838340)", $db) or $sqlerror = 'Yes';

		if ($sqlerror == 'Yes')	continue;

		echo $OK . cLI();

		echo LI() . 'Setting default value for task weather_update: ';

		mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET frequency='30' WHERE task='weather_update' LIMIT 1") or $sqlerror = 'Yes';

		if ($sqlerror == 'Yes')	continue;

		echo $OK . cLI() . cUL() . cUL();
	}

	// Look for friends system
	$db_exist = Search_sub_arrays(PHPRPG_DB_PREFIX . '_users', $database);

	$users = $database[$db_exist]['data'];

	if (empty($users['friends']) ||empty($users['enemies']) ||empty($users['attackers']))
	{
		echo UL() . LI() . 'Adding friends system to ' . PHPRPG_DB_PREFIX . '_users: ' . cLI();
		echo UL();
		if (empty($database[$db_exist]['data']['friends']))
		{
			echo LI() . 'Adding friends: ';

			mysql_query("ALTER TABLE `" . PHPRPG_DB_PREFIX . "_users` ADD `friends` CHAR( 255 ) DEFAULT '' NOT NULL") or $sqlerror = 'Yes';

			if ($sqlerror == 'Yes')	continue;

			echo $OK . cLI();
		}
		if (empty($database[$db_exist]['data']['enemies']))
		{
			echo LI() . 'Adding enemies: ';

			mysql_query("ALTER TABLE `" . PHPRPG_DB_PREFIX . "_users` ADD `enemies` CHAR( 255 ) DEFAULT '' NOT NULL") or $sqlerror = 'Yes';

			if ($sqlerror == 'Yes')	continue;

			echo $OK . cLI();
		}
		if (empty($database[$db_exist]['data']['attackers']))
		{
			echo LI() . 'Adding attackers: ';

			mysql_query("ALTER TABLE `" . PHPRPG_DB_PREFIX . "_users` ADD `attackers` CHAR( 255 ) DEFAULT '' NOT NULL") or $sqlerror = 'Yes';

			if ($sqlerror == 'Yes')	continue;

			echo $OK . cLI();
		}
		echo cUL() . cUL();
	}


	// The _settings table needs to be made.  It's brand new for 0.5.42.
	echo UL() . LI() . 'Creating ' . PHPRPG_DB_PREFIX . '_settings table: ';

	mysql_query("CREATE TABLE `" . PHPRPG_DB_PREFIX . "_settings` (`type` ENUM('version', 'settings') NOT NULL, `name` CHAR(255) NOT NULL, `data` CHAR(255) NOT NULL)") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes')	continue;

	echo $OK . cLI();
	echo UL();

	echo LI() . 'Setting game version in ' . PHPRPG_DB_PREFIX . '_settings: ';

	mysql_query("INSERT INTO " . PHPRPG_DB_PREFIX . "_settings VALUES ('version', 'game', '0.5.42')") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes')	continue;

	echo $OK . cLI();

	echo LI() . 'Setting database version in ' . PHPRPG_DB_PREFIX . '_settings: ';

	mysql_query("INSERT INTO " . PHPRPG_DB_PREFIX . "_settings VALUES ('version', 'database', '0.5.42')") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes')	continue;

	echo $OK . cLI();

	echo cUL() . cUL();
	// Flush what we have so far, that way it's not just blank the whole time.
	// Probably only useful when they are upgrading something really old. But
	// what the hell.
	flush();

case '0.5.42':

/* /------------------------------------------------------------------------\ */
/* |  This last part should always be last.  If no errors have happened we  | */
/* |  reach here where the version numbers are updated.						| */
/* \------------------------------------------------------------------------/ */

	echo LI() . 'Setting new version numbers: ' . cLI();
	echo UL();

	echo LI() . 'Updating game version: ';
	mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_settings SET data='" . 	PHPRPG_GAME_VERSION . "' WHERE (type='version' AND name='game') LIMIT 1") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes')	continue;

	echo $OK . cLI();

	echo LI() . 'Updating database version: ';
	mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_settings SET data='" . 	PHPRPG_DATABASE_VERSION . "' WHERE (type='version' AND name='database') LIMIT 1") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes') continue;

	echo $OK, cLI() . cUL();

}

if ($sqlerror == 'Yes')
{
	echo $FAILED;
	echo ULLI('die', 0);

	echo 'Installation of the latest database: ' . $FAILED . '!<BR>';
	echo 'Reason: ' . mysql_error() . '<BR>';
}
else
{
	echo cUL();
	echo 'Congratulations!  You\'re database has been successfully updated<BR>';
	echo ' to the new version of ' . PHPRPG_DATABASE_VERSION . '.<BR>';
	echo 'Enjoy the new features and options. :)<BR>';
}

?>