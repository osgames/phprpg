<?php

/* include core information                                                   */
require_once('../config.inc.php');
require_once('../lib.inc.php');


/* Variables needed for use with this setup application                       */
$BASE_VER = PHPRPG_GAME_VERSION;


// Array of versions we can upgrade from.
$avail_ver = array('pre-0.5.42','0.5.42');


/* retrieve the needed information out of $_POST                              */
$password = Retrieve_var('password', '_POST');
$action = Retrieve_var('action', '_POST');


/* Make default connection to the database                                    */
$db = mysql_connect(PHPRPG_DB_HOST,PHPRPG_DB_USER,PHPRPG_DB_PASS);
if ($db) mysql_select_db(PHPRPG_DB_NAME,$db);

// It verifies the password before everything, sending it home if it's wrong.
// This stops someone from making a page that posts action="Reset Password" which
// would give the perp access to the system as admin.
if (!verify_passwd($password))
{
	$action = '';
}

/* draw default header information                                            */
setup_header();

echo "<script language='Javascript' type='text/javascript' src='../md5.js'></script>\n";


/* Main logic for application; the case argumants are descriptive             */
switch ($action) {
case "Reset Password":
	sql_header();

	echo "\n<h3>Reset Administrator Password</h3>";
	echo "This will reset the Administrator to the default password of '<b>".PHPRPG_SETUP_PW."</b>'.";
	echo "\n<form method=POST>";
	echo "\n<input type='submit' name='action' value='Confirm Reset'>";
	echo "\n<input type='submit' value='Cancel'>";
	echo "\n<input type='hidden' name='password' value='$password'>";
	echo "\n</form>";
	break;

case "Confirm Reset":
	sql_header();

	$md5 = md5(PHPRPG_SETUP_PW);

	$sql  = "UPDATE ".PHPRPG_DB_PREFIX."_users SET ";
	$sql .= "user_pass='$md5' ";
	$sql .= "WHERE user_name='admin'";

	//@mysql_query($sql,$db);

	echo "\n<h3>Reset Administrator Password</h3>";
	echo "Administrator's password has been changed to '<b>".PHPRPG_SETUP_PW."</b>'.";
	echo "\n<p>";
	echo "\n<form action='".PHPRPG_BASE . "index.php' method='POST'>";
	echo "\n<input type='submit' name='' value='Go to Login'>";
	echo "\n</form>";
	break;

case "New Install":
	sql_header();

	echo "\n<h3>Installing phpRPG $BASE_VER</h3>";
	echo "This will create all the necessary tables, overriding any existing tables.  Are you sure you want to continue?";
	echo "\n<form method=POST>";
	echo "\n<input type='submit' name='action' value='Confirm Install'>";
	echo "\n<input type='submit' value='Cancel'>";
	echo "\n<input type='hidden' name='password' value='$password'>";
	echo "\n</form>";
	break;

case "Confirm Install":
	sql_header();

	echo "Creating Tables...";

	create_tables();

	echo "\n<br>Importing Default Data...";

	import_data();

	if (!mysql_error($db)) {
		echo "\n<h3>Installation Complete</h3>";
		echo "If you see no errors on this page, your installation was successful.";
		echo "\n<p>";
		echo "\n<form  action='".PHPRPG_BASE . "index.php' method='POST'>";
		echo "\n<input type='submit' name='' value='Go to Login'>";
	}
	else {
		echo "\n<h3>Installation Failed!</h3>";
		echo "Errors occured in this installation. This is not good.";
		echo "\n<p>";
		echo "\n<form method=POST>";
		echo "\n<input type='submit' name='' value='Cancel'>";
		echo "\n<input type='hidden' name='password' value='$password'>";
		echo "\n</form>";
	}
	break;

case "Upgrade":

	$sqlerror = 'No';

	$result = mysql_query("SELECT data FROM " . PHPRPG_DB_PREFIX . "_settings WHERE (type='version' AND name='database' LIMIT 1") or $sqlerror = 'Yes';

	if ($sqlerror == 'Yes')
	{
		$version = 'pre-0.5.42';
	} else {
		$data = mysql_fetch_array($result);
		$version = $data['data'];
	}

	sql_header();

	if (in_array($version, $avail_ver)) {
		echo "\n<h3>Upgrading database to " . PHPRPG_DATABASE_VERSION . " for phpRPG $BASE_VER</h3>";

		echo "\n<form method=POST>";
		echo "\n<input type='hidden' value='" . $version . "'>";

		echo "<BR>\nIt is highly recommended that you backup your existing database ";
		echo "before continuing.<br>This can be done on the command line using ";
		echo "\n<b>mysqldump</b> like such:<p>";
		echo "$ mysqladmin -u USERNAME -p -e --add-drop-table ".PHPRPG_DB_NAME." > phprpg-old.sql<p>";
		echo "\n<input type=submit name=action value='Continue Upgrade'>";
		echo "\n<input type='submit' name='' value='Cancel'>";
		echo "\n<input type='hidden' name='password' value='$password'>";
		echo "\n</form>";
	}
	else {
		echo "\n<h3>Upgrade to database version " . PHPRPG_DATABASE_VERSION . "not available.</h3>";
		echo "The version that you are attempting to install is not ";
		echo "available as an automatic upgrade.";
		echo "\n<p>$BASE_VER may be an unstable version ";
		echo " in which you will be required to either alter your ";
		echo "tables manually, or not at all.";
		echo "\n<p>";
		echo "\n<form method=POST>";
		echo "\n<input type='submit' name='' value='Cancel'>";
		echo "\n</form>";
	}
	break;

case "Continue Upgrade":
	sql_header();

	include_once('./upgrade.inc.php');

	if ($sqlerror == 'Yes')
	{
		echo "\n<form method='POST'>";
		echo "<input type='submit' name='' value='Back to Menu'>";
		echo "<input type='hidden' name='password' value='$password'>";
	} else {
		echo "\n<form  action='".PHPRPG_BASE."index.php' method='POST'>";
		echo "\n<input type='submit' name='' value='Go to Login'>";
	}
	echo '</form>';
	break;

case 'Cancel':
	# DO NOT ADD A BREAK, THIS IS SUPPOSED TO DROP THROUGH!!!
	$password = "";

default:

	if (verify_passwd($password)) {
		sql_header();
		echo "\n<h3>Welcome to phpRPG $BASE_VER</h3>";
		echo "Which procedure would you like to preform?";
		echo "\n<p>";
		echo "\n<form method=POST>";

		echo "\n<select name='action'>";
		echo "\n<option value='Upgrade'>Upgrade</option>";
		echo "\n<option value='New Install'>New Install</option>";
		echo "\n<option value='Reset Password'>Reset Admin Password</option>";
		echo "\n</select>";
		echo "\n<input type='submit' name='submit' value='Go'>";
        echo "\n<input type='submit' name='action' value='Cancel'>";
		echo "\n<input type='hidden' name='password' value='$password'>";
		echo "\n</form>";
	}
	else {
		echo "\n<script language='Javascript' type='text/javascript'>";
		echo "\nfunction DoSubmit()";
		echo "\n{";
		echo "\n	mainpw = document.getElementById('mainpw');";
		echo "\n	mainpwMD5 = document.getElementById('mainmd5');";
		echo "\n	mainpwMD5.value = calcMD5(mainpw.value);";
		echo "\n	mainpw.value = ''";
		echo "\n}";
		echo "\n</script>";
		echo "\n<form name='main' method='POST' onSubmit='DoSubmit();'>";
		echo "\n<table border='0'>";
		echo "\n<tr>";
		echo "\n<td>";

		check_config();

		echo "\n<h1>Welcome to phpRPG $BASE_VER</h1>";
		echo "\n<h3>Automatic installation and upgrade</h3>";
		echo "Setup password:\n";
		echo "\n<input size=12 type='password' name='passwordMD5' id='mainpw' value=''>";
		echo "\n<input type='hidden' name='password' id='mainmd5' value=''>";

		if (!empty($password)) {
			echo " <font color='#660000'>INVALID PASSWORD</font>";
		}

		echo "\n<br>";
		echo "\n<input type='submit' name='action' value='Continue'>";
		echo "\n<input type='reset' name='action' value='Cancel'>";
		echo "\n</td>";
        echo "\n<td valign='top'>";
		echo "\n<h4>phpRPG Support</h4>";
		echo "\n<li><a target=_new href='http://phpRPG.org/'>phpRPG Homepage</a></li>";
		echo "\n<li><a target=_new href='http://phpRPG.org/support/'>phpRPG Support</a></li>";
		echo "\n<li><a target=_new href='http://phpRPG.org/forums/'>phpRPG Forums</a></li>";
		echo "\n<li><a target=_new href='http://phpRPG.org/irc/'>phpRPG IRC</a></li>";
		echo "\n</td>";
		echo "\n</tr>";

		echo "\n<tr>";
		echo "\n<td colspan='2'>";
		echo "\n<p><hr size=1 noshade></p>";

		license();

		echo "\n</td>";
		echo "\n</tr>";
		echo "\n</table>";
		echo "\n</form>";
	}
	break;
}



/* draws default footer information                                           */
setup_footer();



/* close the default MySQL connection                                         */
mysql_close($db);






/* Draws the header used by every component in this application.              */
function setup_header() {
	echo "\n<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 3.2//EN\">";
	echo "\n<html>";
	echo "\n<head>";
	echo "\n<title>phpRPG Setup</title>";
	echo "\n<style>";
	echo "\n\ttd,body {font-family: verdana, helvetica, arial;}";
	echo "\n</style>";
	echo "\n</head>";
	echo "\n<body bgcolor=\"white\" link=\"#000000\" vlink=\"#006699\">";
	echo "\n";
	echo "\n<img src=\"../images/phprpg_button2.gif\" alt=\"phpRPG\">";
	echo "\n";
	echo "\n<table border=0 cellspacing=0 cellpadding=5 width=\"95%\">";
	echo "\n<tr>";
	echo "\n<td>";
	echo "\n";
}

/* Main authentication algorythem                                             */
function verify_passwd($pw) {

	if (empty($pw)) return(false);

	$SETUP_PW = md5(constant('PHPRPG_SETUP_PW'));

	if ($pw == $SETUP_PW) return(true);
	else return(false);
}

/* Used to check for basic configuration requirements - acts as a way to      *
 * see if phpRPG will run on your system                                      */
function check_config() {
	global $db;

	if (function_exists('session_start')) {
		$sessions = true;
	}
	else $sessions = false;

	if (function_exists('gd_info')) {

		$gd_version = true;

		if (function_exists('imagegif')) $gif_support = true;
		else $gif_support = false;

		if (function_exists('imagejpeg')) $jpeg_support = true;
		else $jpeg_support = false;

		if (function_exists('imagepng')) $png_support = true;
		else $png_support = false;

		if (function_exists('imagewbmp')) $wbmp_support = true;
		else $wbmp_support = false;
	}
	else $gd_version = false;

	$regglobal = ini_get('register_globals');

	$OK = "<b><font color='green'>OK</font></b>";
	$FAILED = "<b><font color='red'>FAILED</font></b>";

	echo "<h4>Configuration Check</h4>\n";
	echo "<pre>\n";
	echo "Database connection ... ".(!empty($db) ? "$OK" : "$FAILED")."\n";
	echo "Base Directory      ... ".(constant('PHPRPG_BASE') ? "$OK" : "$FAILED")."\n";
	echo "Image Directory     ... ".(constant('PHPRPG_IMG') ? "$OK" : "$FAILED")."\n";
	echo "Session support     ... ".($sessions ? "$OK" : "$FAILED")."\n";
	echo "GD support          ... ".($gd_version ? "$OK" : "$FAILED")."\n";

	if ($gd_version) {
		echo " - GIF support      ... ".($gif_support ? "$OK" : "$FAILED")."\n";
		echo " - JPEG support     ... ".($jpeg_support ? "$OK" : "$FAILED")."\n";
		echo " - PNG support      ... ".($png_support ? "$OK" : "$FAILED")."\n";
		echo " - WBMP support     ... ".($wbmp_support ? "$OK" : "$FAILED")."\n";
	}

	if (!empty($regglobal))
	{
		echo '<font size="5" color="red">Warning!</font><br>';
		echo '<table border="1" bordercolor="red"><tr><td>';
		echo 'You currently have register_globals set to On<BR>';
		echo 'in your php.ini file.  While this was required<BR>';
		echo 'for older versions of phprpg, it has since been<BR>';
		echo 'updated to run fine with register_globals set<BR>';
		echo 'to off.  Things are much more secure this way<BR>';
		echo 'and we highly recommend you run it this way.<BR>';
		echo 'If you need it on for another php program you<BR>';
		echo 'have as well it should be possible to disable<BR>';
		echo 'it for phprpg using .htaccess files.  Check<BR>';
		echo 'your PHP documentation for more information.<BR>';
		echo '</td></tr></table>';
	}
	echo "</pre>\n";
}

/* draws the default licensing infomation                                     */
function license() {
	echo "<h3>Legal Information</h3>\n";
	echo "<blockquote><pre>\n";
	echo "phpRPG: PHP RPG Framework\n";
	echo "Copyright (c) 2001-2002 Timothy TS Chung <ttschung@users.sourceforge.net>\n";
	echo "\n";

	# replace below with an include of the LICENSE file?
	echo "This program is free software; you can redistribute it and/or modify it\n";
	echo "under the terms of the GNU General Public License as published by the Free\n";
	echo "Software Foundation; either version 2 of the License, or (at your option)\n";
	echo "any later version.\n";
	echo "\n";
	echo "This program is distributed in the hope that it will be useful, but WITHOUT\n";
	echo "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or\n";
	echo "FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for\n";
	echo "more details.\n";
	echo "\n";
	echo "You should have received a copy of the GNU General Public License along\n";
	echo "with this program; if not, write to the Free Software Foundation, Inc., 59\n";
	echo "Temple Place - Suite 330, Boston, MA  02111-1307, USA.\n";
	echo "</pre></blockquote>\n";
}

/* Draws the default footer for all components used in this application       */
function setup_footer() {
	echo "\n</td>";
	echo "\n</tr>";
	echo "\n</table>";
	echo "\n</body>";
	echo "\n</html>";
}

/* This function includes the create.sql.php file which creates the tables    *
 * used by phpRPG                                                             */
function create_tables() {
	global $db;
	include_once("./create.sql.php");
	database_create($db);
}

/* This function includes the data.sql.php file which inserts data into       *
 * the phpRPG tables                                                          */
function import_data() {
	global $db;
	include_once("./data.sql.php");
	database_data($db);
}

/* This function provides basic infomation conserning the MySQL settings      */
function sql_header() {

	global $password;

	if (!verify_passwd(@$password)) {
		die("ERROR: Authentication Failure!");
	}

	echo "\n<hr size=1 noshade>";
	echo "\n<table border=0 cellspacing=1 cellpadding=1 width=\"100%\">";
	echo "\n<tr bgcolor=\"#9fb6cd\">";
	echo "\n\t<td>Database: <b>".PHPRPG_DB_NAME."</b></td>";
	echo "\n\t<td>Host: <b>".PHPRPG_DB_HOST."</b></td>";
	echo "\n\t<td>User: <b>".PHPRPG_DB_USER."</b></td>";
	echo "\n\t<td>Prefix: <b>".PHPRPG_DB_PREFIX."</b></td>";
	echo "\n</tr>";
	echo "\n</table>";
	echo "\n<hr size=1 noshade>";
}


?>
