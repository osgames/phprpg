<?php
/******************************************************************************/
/*																			  */
/* online.php - Displays a list of players online							  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 3 June 2001														  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_events.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');
require_once('template_header.inc.php');

DbConnect();

$current_time = time();
$online_range = $current_time - PHPRPG_SESSION_EXPIRY;
$result = mysql_query("SELECT name, exp, host, last_active FROM " . PHPRPG_DB_PREFIX . "_users WHERE last_active > $online_range");
$online_number = mysql_num_rows($result);

echo '
<table cellpadding="0" cellspacing="10" width="100%" border="0">
<tr><td width="100%" valign="top">
';

OpenTable('title', '100%');
if ($online_number == 1)
{
	echo 'Currently, ' . $char['name'] . ' Is The Only Player Online...';
}
else
{
	echo 'Currently ' . $online_number . ' Players Online';
}

OpenTable('content');
echo '
<div align="justify">
Online :: ' . $char['map_name'] . '<br>
';

include_once('template_menu.inc.php');

echo '
<table cellpadding="0" cellspacing="4" border=0>
<tr><td width="120"><font size="2" color="#eeeeee">Player</font></td><td width="120"><font size="2" color="#eeeeee">Experience</font></td>';
if ($char['admin_level'] > 8)
{
	echo '<td width="90"><font size="2" color="#eeeeee">Last Active</font></td><td width="300"><font size="2" color="#eeeeee">Host</font></td>';
}
echo '</tr>';
while ($player = mysql_fetch_array($result))
{
	echo '<tr><td><font size="2" color="#bbbbbb">' . $player['name'] . '</font></td><td><font size="2" color="#bbbbbb">' . $player['exp'] . '</font></td>';
	if ($char['admin_level'] > 8)
	{
		$last_active_time = $current_time - $player['last_active'];
		echo '<td><font size="2" color="#bbbbbb">' . $last_active_time . '</font></td><td><font size="2" color="#bbbbbb">' . $player['host'] . '</font></td>';
	}
	echo '</tr>';
}
echo '
</table>
<br>

';

// Top 10 Players
$result = mysql_query("SELECT name FROM " . PHPRPG_DB_PREFIX . "_users ORDER BY EXP DESC LIMIT 10");

echo '
<table cellpadding="0" cellspacing="4" border=0>
<tr><td width="150"><font face="arial" size="3" color="#f3f3f3"><b>Top 10 Players</b></font><br></td><td width="50"><font size="2" color="#eeeeee">Rank</font></td><td width="120"><font size="2" color="#eeeeee">Player</font></td></tr>';
for ($i = 1; $player = mysql_fetch_array($result); $i++)
{
	echo '<tr><td></td><td><font size="2" color="#bbbbbb">' . $i . '</font></td><td><font size="2" color="#bbbbbb">' . $player['name'] . '</font></td></tr>';
}
echo '
</table>
<br>
';

// Greediest Players
$result = mysql_query("SELECT DISTINCT user_id, COUNT(*) AS items FROM " . PHPRPG_DB_PREFIX . "_items WHERE user_id!=0 GROUP BY user_id ORDER BY items DESC LIMIT 10");

echo '
<table cellpadding="0" cellspacing="4" border=0>
<tr><td width="150"><font face="arial" size="3" color="#f3f3f3"><b>Greediest Players</b></font><br></td><td width="50"><font size="2" color="#eeeeee">Rank</font></td><td width="120"><font size="2" color="#eeeeee">Player</font></td><td width="120"><font size="2" color="#eeeeee">Number of Items</font></td></tr>';
for ($i = 1; $player = mysql_fetch_array($result); $i++)
{
	$result_name = mysql_query("SELECT name FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=" . $player['user_id'] . " LIMIT 1");
	list($greedy_player) = mysql_fetch_array($result_name);
	echo '<tr><td></td><td><font size="2" color="#bbbbbb">' . $i . '</font></td><td><font size="2" color="#bbbbbb">' . $greedy_player . '</font></td><td><font size="2" color="#bbbbbb">' . $player['items'] . '</font></td></tr>';
}
echo '
</table>
';

echo '
<div align="right"><a href="javascript:history.go(-1)">Back</a><br></div>
';
OpenTable('close');

echo '
</td><td width="172" valign="top">
';

// Stats Box
require_once('template_stats.inc.php');

echo '
</td></tr></table>
';

require_once('template_footer.inc.php');
?>