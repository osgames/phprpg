<?php
/******************************************************************************/
/*																			*/
/* template_stats.inc.php - HTML Time and Stats Box							*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 2 April 2002													 */
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

DbConnect();

// Time Box
OpenTable('title', '160');
echo '<center>Time and Date</center>';

OpenTable('content');
echo GameDate();
OpenTable('close');

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="5" border="0"><br>
';

// Weather Box
OpenTable('title', '165');
echo '<center>Weather Condition</center>';

OpenTable('content');
echo WeatherBox();
OpenTable('close');

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="5" border="0"><br>
';

// Stats Box
OpenTable('title', '160');
echo $char['name'];

OpenTable('content');
echo '
<table cellpadding="2" cellspacing="0" width="100%" border="0">
';

$bar_percentage = number_format($char['HP'] / $char['HP_MAX'] * 100, 0);
if ($bar_percentage == '100')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_green.png" width="100" height="7" border="0">';
}
elseif ($bar_percentage == '0')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="100" height="7" border="0">';
}
else
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="' . (100 - $bar_percentage) . '" height="7" border="0"><img src="' . PHPRPG_IMG . 'bar_green.png" width="' . $bar_percentage . '" height="7" border="0">';
}
echo '
<tr>
<td><div align="left"><font face="arial" size="2" color="#eeeeee">health</font></div></td>
<td><div align="right"><font face="arial" size="2" color="#bbbbbb">' . $char['HP'] . ' / ' . $char['HP_MAX'] . '</font></div></td>
</tr>
<tr>
<td colspan="2"><div align="right"><img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0">'
. $append_string . '<img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0"><br><img
src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="2" border="0"></div></td>
</tr>
';

$bar_percentage = number_format($char['MP'] / $char['MP_MAX'] * 100, 0);
if ($bar_percentage == '100')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_orange.png" width="100" height="7" border="0">';
}
elseif ($bar_percentage == '0')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="100" height="7" border="0">';
}
else
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="' . (100 - $bar_percentage) . '" height="7" border="0"><img src="' . PHPRPG_IMG . 'bar_orange.png" width="' . $bar_percentage . '" height="7" border="0">';
}
echo '
<tr>
<td><div align="left"><font face="arial" size="2" color="#eeeeee">mana</font></div></td>
<td><div align="right"><font face="arial" size="2" color="#bbbbbb">' . $char['MP'] . ' / ' . $char['MP_MAX'] . '</font></div></td>
</tr>
<tr>
<td colspan="2"><div align="right"><img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0">'
. $append_string . '<img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0"><br><img
src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="2" border="0"></div></td>
</tr>
';

$bar_percentage = number_format($char['STM'] / $char['STM_MAX'] * 100, 0);
if ($bar_percentage == '100')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_yellow.png" width="100" height="7" border="0">';
}
elseif ($bar_percentage == '0')
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="100" height="7" border="0">';
}
else
{
	$append_string = '<img src="' . PHPRPG_IMG . 'bar_empty.png" width="' . (100 - $bar_percentage) . '" height="7" border="0"><img src="' . PHPRPG_IMG . 'bar_yellow.png" width="' . $bar_percentage . '" height="7" border="0">';
}
echo '
<tr>
<td><div align="left"><font face="arial" size="2" color="#eeeeee">stamina</font></div></td>
<td><div align="right"><font face="arial" size="2" color="#bbbbbb">' . $char['STM'] . ' / ' . $char['STM_MAX'] . '</font></div></td>
</tr>
<tr>
<td colspan="2"><div align="right"><img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0">'
. $append_string . '<img src="' . PHPRPG_IMG . 'bar_side.png" width="1" height="7" border="0"><br><img
src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="2" border="0"></div></td>
</tr>
';

echo '
<tr>
<td><div align="left"><font face="arial" size="2" color="#eeeeee">currency</font></div></td>
<td><div align="right"><font face="arial" size="2" color="#bbbbbb"><img src="' . PHPRPG_IMG . 'gold.png" width="10" height="10" border="0"> $
' . $char['GP'] . '</font></div></td>
</tr>

<tr>
<td colspan="2"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" vspace="4" border="0"></td>
</tr>

<tr>
<td><div align="left"><font face="arial" size="2" color="#eeeeee">experience</font></div></td>
<td><div align="right"><font face="arial" size="2" color="#bbbbbb">' . $char['EXP'] . '</font></div></td>
</tr>

</table>
';
OpenTable('close');


?>