<?PHP

error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_forum.inc.php');
require_once('lib_interactions.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$option = Retrieve_var('option');
$admincat = Retrieve_var('option');
$cat_name = Retrieve_var('cat_name');
$forum_name = Retrieve_var('forum_name');
$description = Retrieve_var('description');
$cat = Retrieve_var('cat');
$adminforum = Retrieve_var('adminforum');
$fid = Retrieve_var('fid');
$subject = Retrieve_var('subject');
$body = Retrieve_var('body');
$tid = Retrieve_var('tid');
$id = Retrieve_var('id');

$notAdmin = 'You are not a forum admin but you are in a forum admin area.  <a href="index.php">Go away</a>.';
$cols = 70;
$allowedHTML = '<a><b><u><i><ol><ul><li><pre>';
$bg = '#260836';

#always admin
//$admin = 0;

require_once('template_header.inc.php');

switch ($option) {
	case 'addcat':
		if (checkAdmin('forum')) {
			openTable('title', '100%');
			echo 'Add a Category';
			openTable('content');
			echo '<form name=form method=post action=' . PHP_SELF .'?s='.$s. '>';
			echo '<input type=hidden name=option value=addcatnow>';
			echo 'Add a category<br><br>';
			echo 'category name:  <input type=text name=cat_name maxlength=100><br>';
			echo 'admin category?  <input type=checkbox name=admincat><br>';
			echo '<input type=submit name=submit>';
			echo '</form>';
			openTable('close');
		} else {
			echo $notAdmin;
		}
		break;
	case 'addcatnow':
		if (checkAdmin('forum')) {
			if (!empty($cat_name)) {
				$admincat = ($admincat == 'on') ? 1 : 0;

				$result = mysql_query("INSERT INTO forum_cat (name, admin) VALUES ('" . fixQuotes($cat_name) . "', $admincat)");
				if ($result)
					echo 'Adding category '.$cat_name.' worked<br>';
				else
					echo 'Adding category '.$cat_name.' did not work<br>';
				echo '<br><a href="' . PHP_SELF . '?s='.$s.'&option=admin">go back to the admin page</a>';
			}
		} else {
			echo $notAdmin;
		}
		break;
	case 'addforum':
		if (checkAdmin('forum')) {
			openTable('title', '100%');
			echo 'Add a Forum';
			openTable('content');
			echo '<form name=form method=post action='.PHP_SELF.'?s='.$s.'>';
			echo '<input type=hidden name=option value=addforumnow>';
			echo '<table><tr><td align=right>forum name:</td>';
			echo '<td><input type=text name=forum_name maxlength=100></td></tr>';
			echo '<tr><td align=right>description:</td><td><textarea name=description cols='.$cols.' rows=10></textarea></td></tr>';
			echo '<tr><td align=right>category:</td><td>';
			echo '<select name=cat>';
			$result = mysql_query("SELECT * FROM forum_cat");
			while ($result && list($id, $name) = mysql_fetch_row($result)) {
				echo '<option value='.$id.'>'.$name;
			}
			echo '</select>';
			echo '</td></tr>';
			echo '<tr><td>admin forum?  <input type=checkbox name=adminforum></td></tr>';
	//		echo '<tr><td align=right></td><td></td></tr>';
			echo '<tr><td align=right><input type=submit value=submit></td><td><input type=reset value=reset></td></tr>';

			echo '</form>';
			openTable('close');
		} else {
			echo $notAdmin;
		}
		break;
	case 'addforumnow':
		if (checkAdmin('forum')) {
			openTable('title', '100%');
			echo 'Add Forum';
			openTable('content');
			if (!empty($forum_name) && !empty($description) && !empty($cat)) {
				$adminforum = ($adminforum == 'on') ? 1 : 0;

				$result = mysql_query("INSERT INTO forums (id, cat_id, name, description, admin) VALUES ('', $cat, '".fixQuotes($forum_name)."', '".fixQuotes($description)."', $adminforum)");
				if ($result)
					echo 'Adding forum '.$forum_name.' worked.<br><br>';
				else
					echo 'Adding forum '.$forum_name.' did not work.<br>';
				echo '<br><a href="' . PHP_SELF .'?s='.$s.'&option=admin">go back to the admin page</a>';
			}
			openTable('close');
		} else {
			echo $notAdmin;
		}
		break;
	case 'admin':
		if (checkAdmin('forum')) {
			openTable('title', '100%');
			echo 'Forum Admin';
			openTable('content');
			echo '<li><a href="' . PHP_SELF.'?s='.$s.'&option=addcat">add a forum category</a><br>';
			echo '<li><a href="' . PHP_SELF.'?s='.$s.'&option=addforum">add a forum</a><br><br>';
			echo '<a href="' . PHP_SELF.'?s='.$s.'">click here to go back to the forum</a>';
			openTable('close');
		} else {
			echo $notAdmin;
		}
		break;

	case 'newtopic':
		openTable('title', '100%');
		echo 'New Topic';
		openTable('content');
		echo '<center><form name=form method=post action=' . PHP_SELF .'?s='.$s.'>';
		echo '<input type=hidden name=option value=newtopicpost>';
		echo '<input type=hidden name=fid value="'.$forum_id.'">';
		echo '<table><tr><td align=right>subject:</td>';
		echo '<td><input type=text name=subject maxlength=100></td></tr>';
		echo '<tr><td align=right>body:</td><td><textarea name=body cols='.$cols.' rows=10></textarea><br>allowed HTML tags:<br>'.htmlspecialchars($allowedHTML).'</td></tr>';
		echo '</td></tr>';
		echo '<tr><td align=right><input type=submit value=submit></td><td><input type=reset value=reset></td></tr>';
		echo '</form></center>';
		openTable('close');
		break;

	case 'newtopicpost':
		if (!empty($subject) && !empty($fid)) {
			$subject = addslashes(strip_tags($subject));
			$body = addslashes(nl2br(strip_tags($body, $allowedHTML)));
			$thing = mysql_query("SELECT thread_id FROM forum_posts ORDER BY thread_id DESC LIMIT 1");
			if ($thing) {
				list($tid) = mysql_fetch_row($thing);
				$tid++;
				$query = "INSERT INTO forum_posts (id, forum_id, thread_id, date, author, subject, message, seen) VALUES ('', $fid, $tid, '".time()."', '".$char['user_id']."', '$subject', '$body', '".$char['user_id']."')";
				$result = mysql_query($query);
				if ($result) {
					$query = "UPDATE forums SET num_topics=num_topics+1, num_posts=num_posts+1, last_post_date='".time()."', last_post_who='".$char['user_id']."' WHERE id = $fid";
					$result = mysql_query($query);
					openTable('title', '100%');
					echo 'New Topic Posting Worked';
					openTable('content');
					echo '<center>' . getNav($tid) . '</center>';
					closeTable('content');
				}
			}
		}
		break;

	case 'deletepost':
		if (checkAdmin('forum')) {
			if (!empty($id) && !empty($tid) && !empty($fid)) {
				$result = mysql_query("DELETE FROM forum_posts WHERE id = $id");
				$result = mysql_query("UPDATE forums SET num_posts=num_posts-1 WHERE id = $fid");
				$result = mysql_query("SELECT id FROM forum_posts WHERE thread_id = $tid");
				if (mysql_num_rows($result) == 0) {
					$result = mysql_query("UPDATE forums SET num_topics=num_topics-1 WHERE id = $fid");
					openTable('title', '100%');
					echo 'Delete worked and topic gone.';
					openTable('content');
					echo '<a href="' . PHP_SELF .'?s='.$s.'&option=viewforum&id='.$fid.'">click here to return to the forums</a>';
					openTable('close');
				} else {
					openTable('title', '100%');
					echo 'Delete worked and topic not gone.';
					openTable('content');
					echo '<a href="' . PHP_SELF .'?s='.$s.'&option=viewforum&id='.$fid.'">click here to return to the forums</a>';
					openTable('close');
				}
			} else {
				echo 'need some more info.';
			}
		} else {
			echo $notAdmin;
		}

		break;

	case 'deletecat':


		break;

	case 'viewtopic':
		if (!empty($tid) && !empty($fid)) {

			openTable('title', '100%');
			echo 'View Topic';
			openTable('content');

			$i = 1;
			$thecolor = $bg;


			echo '<center><br>';
			echo getNav($tid) . '<br><br>';
			echo '<table width="90%" border=0 cellpadding=1 cellspacing=0 align=center valign=top><tr><td bgcolor=white>';
			echo '<table border=0 cellpadding=1 cellspacing=1 width="100%">';
			$result = mysql_query("SELECT f.*, u.name, u.title FROM forum_posts f, $TablePrefix"."_user u WHERE f.thread_id = $tid AND u.user_id = f.author ORDER BY date ASC");
			$thing = "";
			while (list($id, $fids, $tids, $date, $author, $subject, $message, $read, $name, $title) = mysql_fetch_row($result)) {
				if ($thing == "") {
					$thing = $subject;
				}

				$new = true;
				$str = explode(";", $read);
				if (sizeof($str) > 0) {
					for ($i = 0; $i < sizeof($str); ++$i) {
						if ($char['user_id'] == $str[$i]) {
							$new = false;
						}
					}
				}
				if ($new == true)
					mysql_query("UPDATE forum_posts SET seen = '" . $read . ";" . $char["user_id"] . "' WHERE id = $id");

				echo '<tr>';
				echo '<td colspan=2 bgcolor="'.$thecolor.'">';
				if ($new == true)
					echo '<img src="' . PHPRPG_IMG . '/red_folder.gif">';
				else
					echo '<img src="' . PHPRPG_IMG . '/folder.gif">';
				echo '<font size=1> - -- --- -- - </font><b>'.stripslashes($subject).'</b></td>';
				if (checkAdmin('forum')) {
					echo '<td rowspan=2 bgcolor="'.$thecolor.'"><a href="' . PHP_SELF .'?s='.$s.'&option=deletepost&id='.$id.'&fid='.$fids.'&tid='.$tids.'">Delete</a></td>';
				}
				echo '</tr><tr valign=top bgcolor="'.$thecolor.'">';
				echo '<td width="20%" bgcolor="'.$thecolor.'"><b>'.$name.'</b>'.((!empty($title))?(' - '.$title):('')).'<br><font size=1>'.date("g:i a, F j, Y", $date).'</font></td>';
				echo '<td width="80%" bgcolor="'.$thecolor.'">'.stripslashes($message).'</td>';
				echo '</tr>';

				if ($thecolor == $bg) {
					$thecolor = "#000060";
				} else {
					$thecolor = $bg;
				}
			}
			echo '</table></td></tr></table><br>';
			$links = '[ <a href="' . PHP_SELF .'?s='.$s.'&option=replytopic&tid='.$tid.'&fid='.$fid.'&subject='.$thing.'">Reply</a> ]';
			echo $links.'</center><br>';

			openTable('close');
		}
		break;

	case 'replytopic':
		if (!empty($tid)) {
			openTable('title', '100%');
			echo 'Reply';
			openTable('content');
			echo '<center><form name=form method=post action=' . PHP_SELF .'?s='.$s.'>';
			echo '<input type=hidden name=option value=replytopicpost>';
			echo '<input type=hidden name=fid value="'.$fid.'">';
			echo '<input type=hidden name=tid value="'.$tid.'">';
			echo '<table><tr><td align=right>subject:</td>';
			echo '<td><input type=text name=subject maxlength=100 value="'.$subject.'"></td></tr>';
			echo '<tr><td align=right valign=top>body:</td><td><textarea name=body cols='.$cols.' rows=10></textarea><br>allowed HTML tags:<br>'.htmlspecialchars($allowedHTML).'</td></tr>';
			echo '</td></tr>';
			echo '<tr><td align=right><input type=submit value=submit></td><td><input type=reset value=reset></td></tr></table>';
			echo '<center><a href="' . PHP_SELF .'?s='.$s.'">Back to the forums</a></center>';
			echo '</form>';

			// HERE
			echo '<table cellspacing=0 cellpadding=0 border=1 width="75%">';
			$result = mysql_query("SELECT f.*, u.name FROM forum_posts f, $TablePrefix"."_user u WHERE f.thread_id = $tid AND u.user_id = f.author ORDER BY date ASC");
			$i = 0;
			while (list($id, $fid, $tid, $date, $author, $subject, $message, $read, $name) = mysql_fetch_row($result)) {
				echo '<tr bgcolor="">';
				echo '<td colspan=2><font size=1> - -- --- -- - </font><b>'.stripslashes($subject).'</b></td>';
				if (checkAdmin('forum')) {
					echo '<td rowspan=2><a href="' . PHP_SELF .'?s='.$s.'&option=deletepost&id='.$id.'&fid='.$fid.'&tid='.$tid.'">Delete</a></td>';
				}
				echo '</tr><tr valign=top>';
				echo '<td width="20%">'.$name.'<br><font size=1>'.date("g:i a, F j, Y", $date).'</font></td>';
				echo '<td width="80%">'.stripslashes($message).'</td>';
				echo '</tr>';
				$i++;
			}
			echo '</table></center>';


			openTable('close');
		}
		break;

	case 'replytopicpost':
		if (!empty($subject) && !empty($tid) && !empty($fid)) {
			$subject = addslashes(strip_tags($subject));
			$body = addslashes(nl2br(strip_tags($body, $allowedHTML)));
			$query = "INSERT INTO forum_posts (id, forum_id, thread_id, date, author, subject, message, seen) VALUES ('', $fid, $tid, '".time()."', '".$char['user_id']."', '$subject', '$body', '".$char['user_id']."')";
			$result = mysql_query($query);
			if ($result) {
				$query = "UPDATE forums SET num_posts=num_posts+1, last_post_date='".time()."', last_post_who='".$char['user_id']."' WHERE id = $fid";
				$result = mysql_query($query);
				openTable('title', '100%');
				echo 'Reply';
				openTable('content');
				echo '<center>'.getNav($tid).'</center>';
				closeTable('content');
			}
		}
		break;

		break;

	case 'deletethread':

		if (checkAdmin('forum')) {
			if (!empty($tid)) {
				$result = mysql_query("SELECT forum_id FROM forum_posts WHERE thread_id = $tid");
				list($fid) = mysql_fetch_row($result);
				$count = mysql_num_rows($result);
				$result = mysql_query("UPDATE forums SET num_topics = num_topics - 1, num_posts = num_posts - $count WHERE id = $fid");
				$result = mysql_query("DELETE FROM forum_posts WHERE thread_id = $tid");
				if ($result) {
					openTable('title', '100%');
					echo 'thread delete worked.';
					openTable('content');
					echo '<center>'.getNav(-1, $fid).'</center>';
					openTable('close');
				} else {
					echo 'thread delete didn\'t work.';
				}
			} else {
				echo 'I don\'t know which thread to delete.';
			}
		} else {
			echo $notAdmin;
		}
		break;

	case 'viewforum':
		$forum = mysql_query("SELECT name, description FROM forums WHERE id = $id");

		if (!empty($id) && $forum) {
			openTable('title', '100%');
			$fid = $id;
			list($name, $desc) = mysql_fetch_row($forum);
			echo '' . $name . '<br>';
			openTable('content');

			echo $desc . '<br>';

			echo '<br><center>'.getNav(-1, $id).'</center><br><br>';

			$sortMethod = "";
			$sortThing = "";
			if (empty($sort) || $sort == "sort") {
				$sortMethod = '-sort';
				$sortThing = '<img src="' . PHPRPG_IMG . '/sortasc.gif" border=0>';
				$result = mysql_query("SELECT thread_id FROM forum_posts WHERE forum_id = $id ORDER BY date DESC");
			} elseif ($sort == "-sort") {
				$sortMethod = 'sort';
				$sortThing = '<img src="' . PHPRPG_IMG . '/sortdesc.gif" border=0>';
				$result = mysql_query("SELECT thread_id FROM forum_posts WHERE forum_id = $id ORDER BY date ASC");
			}
			if (mysql_error()) echo mysql_errno() . ':  ' . mysql_error();

			// nothing
			// Topic
			// replies
			// poster
			// last post
			// admin (maybe)

			echo '<center><table width="75%" border=0 cellpadding=1 cellspacing=0 align=center valign=top><tr><td bgcolor=white>';
			echo '<table width="100%" border=0 cellspacing=1 cellpadding=1>';
			echo '<tr align=center><td width="5%" bgcolor="'.$bg.'"></td><td align=left bgcolor="'.$bg.'">Topic</td><td bgcolor="'.$bg.'">Replies</td><td bgcolor="'.$bg.'">Poster</td><td bgcolor="'.$bg.'"><a href="'.PHP_SELF.'?s='.$s.'&option=viewforum&id='.$fid.'&sort='.$sortMethod.'">Last Post '.$sortThing.'</a></td>';
			if (checkAdmin('forum')) {
				echo '<td bgcolor="'.$bg.'">Admin</td>';
			}
			echo '</tr>';
			$lasttid = -1;
			$num = 0;
			/*
			if (mysql_num_rows($result) > 20) {
				echo '<tr><td colspan=99 align=right>';
				echo '<a href="index.php?option=viewforum&id='.$fid.'&page='.($page+1).'">Next</a>';
				echo '</td></tr>';
			}
			*/
			$ids = "";
			while ($result && list($thread_id) = mysql_fetch_row($result)) {

				if (!contains($ids, $thread_id)) {

					$thing = mysql_query("SELECT id, forum_id, thread_id, date, author, subject, seen FROM forum_posts WHERE thread_id = $thread_id");
					$array = mysql_fetch_array($thing);


					$r1 = mysql_query("SELECT seen FROM forum_posts WHERE thread_id = $thread_id");
					$notnew = false;
					while (list($read) = mysql_fetch_row($r1)) {
						$str = explode(";", $read);
						if (contains($str, $char['user_id']) == false) {
							$notnew = true;
						}
					}


					/*
					$new = true;
					$r1 = mysql_query("SELECT seen FROM forum_posts WHERE thread_id = $thread_id");
					while (list($read) = mysql_fetch_row($r1)) {
						$str = explode(";", $read);
						if (sizeof($str) > 0) {
							for ($i = 0; $i < sizeof($str); ++$i) {
								if ($char['user_id'] == $str[$i]) {
									$new = false;
								}
							}
						}
					}
					*/

					// folder

					echo '<tr align=center><td bgcolor="'.$bg.'">';
					if ($notnew == true) {
						if (mysql_num_rows($r1) >= 10) echo '<img src="' . PHPRPG_IMG . '/hot_red_folder.gif">';
						else echo '<img src="' . PHPRPG_IMG . '/red_folder.gif">';
					} else {
						if (mysql_num_rows($r1) >= 10) echo '<img src="' . PHPRPG_IMG . '/hot_folder.gif">';
						else echo '<img src="' . PHPRPG_IMG . '/folder.gif">';
					}

					echo '</td>';

					// Topic

					//$thing = mysql_query("SELECT subject, $forum_id FROM forum_posts WHERE thread_id = $thread_id ORDER BY date ASC");
					//if ($thing && list($subject) = mysql_fetch_row($thing))
						echo '<td align=left bgcolor="'.$bg.'"><a href="' . PHP_SELF .'?s='.$s.'&option=viewtopic&tid='.$array['thread_id'].'&fid='.$array['forum_id'].'">'.$array['subject'].'</a></td>';

					// replies

					$thing = mysql_query("SELECT id FROM forum_posts WHERE thread_id = ".$array['thread_id']);
					if ($thing)
						echo '<td bgcolor="'.$bg.'">' . (mysql_num_rows($thing)-1) . '</td>';
					else
						echo '<td>'.mysql_error().'</td>';

					// poster

					$thing = mysql_query("SELECT u.name FROM forum_posts f, $TablePrefix"."_user u WHERE f.thread_id = ".$array['thread_id']." AND f.author = u.user_id ORDER BY date ASC");
					if ($thing && (list($name) = mysql_fetch_row($thing)))
						echo '<td bgcolor="'.$bg.'">' . $name . '</td>';
					else
						echo '<td>'.mysql_error().'</td>';

					// last post

					$thing = mysql_query("SELECT f.date, u.name FROM forum_posts f, $TablePrefix"."_user u WHERE f.forum_id = ".$array['forum_id']." AND f.thread_id = ".$array['thread_id']." AND f.author = u.user_id ORDER BY date DESC");
					if ($thing && list($ldate, $name) = mysql_fetch_row($thing))
						echo '<td bgcolor="'.$bg.'"><font size=2>' . date("g:i a, F j, Y", $ldate) . '<br>by '.$name.'</font></td>';
					else
						echo '<td bgcolor="'.$bg.'">'.mysql_error().'</td>';

					// admin (maybe)

					if (checkAdmin('forum')) {
						echo '<td bgcolor="'.$bg.'"><a href="' . PHP_SELF.'?s='.$s.'&option=deletethread&tid='.$array['thread_id'].'&forum_id='.$array['forum_id'].'">Delete</a></td>';
					}

					echo '</tr>';
				}
				$ids[] = $thread_id;
				$num ++;
			}
			echo '</table></td></tr></table></center><br>';

			$links = '[ <a href="' . PHP_SELF.'?s='.$s.'">Forums</a> | <a href="' . PHP_SELF . '?s='.$s.'&option=newtopic&forum_id='.$fid.'">New Topic</a> ]';

			echo '<br><center>[ <a href="'.PHP_SELF.'?s='.$s.'&option=newtopic&forum_id='.$fid.'">New Topic</a> ]</center><br>';

			openTable('close');
		}
		break;
	default:
		openTable('title', '100%');
		echo '<a href="'.PHP_SELF.'?s='.$s.'">Forums</a>';
		openTable('content');

		$result = mysql_query("SELECT * FROM forum_cat");
		echo '<center>';
		echo '<table width="75%" border=0 cellpadding=1 cellspacing=0 align=center valign=top><tr><td bgcolor=white>';
		echo '<table border=0 cellpadding=1 cellspacing=1 width="100%">';
		echo '<tr><td width="5%" bgcolor="'.$bg.'"></td><td width="60%" bgcolor="'.$bg.'"><a href="'.PHP_SELF.'?s='.$s.'">Forum</a></td><td align=center bgcolor="'.$bg.'">Topics</td><td align=center bgcolor="'.$bg.'">Posts</td><td align=center bgcolor="'.$bg.'">Last Post</td></tr>';
		while($result && list($catid, $cat, $admin) = mysql_fetch_row($result)) {
			// this $admin is whether or not the $category is an admin cat.  NOT whether or not the user is an admin
			$print = false;
			if ($admin == 0) {
				$print = true;
			} elseif ($admin == 1 && checkAdmin('forum')) {
				$print = true;
			}

			if (!empty($category)) {
				if ($category != $catid) {
					echo '<tr><td colspan=5 bgcolor="'.$bg.'"><a href='.PHP_SELF.'?s='.$s.'&category='.$catid.'>'.$cat.'</a></td></tr>';
					$print = false;
				}
			}

			if ($print) {
				echo '<tr><td colspan=5 bgcolor="'.$bg.'"><a href='.PHP_SELF.'?s='.$s.'&category='.$catid.'>'.$cat.'</a></td></tr>';
				$r2 = mysql_query("SELECT * FROM forums WHERE cat_id = $catid ORDER BY id");
				while ($r2 && list($id, $catid, $admin, $adminpostonly, $name, $desc, $num_topics, $num_posts, $last_post_date, $last_post_who) = mysql_fetch_row($r2)) {
					$print = false;
					if ($admin == 0) {
						$print = true;
					} elseif ($admin == 1 && checkAdmin('forum')) {
						$print = true;
					}

					if ($print) {
						echo '<tr><td valign=middle align=center bgcolor="'.$bg.'">';

						$r1 = mysql_query("SELECT seen FROM forum_posts WHERE forum_id = $id");
						$notnew = false;
						while (list($read) = mysql_fetch_row($r1)) {
							$str = explode(";", $read);
							if (contains($str, $char['user_id']) == false) {
								$notnew = true;
							}
						}

						if ($notnew == true) echo '<img src="' . PHPRPG_IMG . '/red_folder.gif">';
						else echo '<img src="' . PHPRPG_IMG . '/folder.gif">';

						echo '</td><td bgcolor="'.$bg.'"><a href="' . PHP_SELF.'?s='.$s.'&option=viewforum&id='.$id.'">'.$name.'</a><br>';
						echo '<table width="100%" cellpadding=0 cellspacing=0><tr><td width="5%"> </td><td><font size=2>'.$desc.'</font></td></tr></table>';
						echo '</td>';
						echo '<td align=center bgcolor="'.$bg.'">'.$num_topics.'</td>';
						echo '<td align=center bgcolor="'.$bg.'">'.$num_posts.'</td>';
						if ($last_post_who == 0) {
							echo '<td align=center bgcolor="'.$bg.'">no one</td>';
						} else {
							if ($num_topics != 0 && $num_posts != 0) {
								$thing = mysql_query("SELECT name FROM $TablePrefix" . "_user WHERE user_id = $last_post_who");
								if ($thing && (list($name) = mysql_fetch_row($thing))) {
									echo '<td align=center bgcolor="'.$bg.'"><font size=2>'.date("g:i a, F j, Y", $last_post_date).'<br>'.$name.'</font></td>';
								}
							} else {
								echo '<td align=center bgcolor="'.$bg.'">no one</td>';
							}
						}
						echo '</tr>';
					}
				}
			}

		}
		echo '</table></td></tr></table>';

		if (checkAdmin('forum')) {
			echo '<br><a href="' . PHP_SELF.'?s='.$s.'&option=admin">click here to admin</a>';
		}

		echo '<br><a href="local.php?s='.$s.'&">go back</a>';

		openTable('close');

		break;
}

include_once('template_footer.inc.php');



?>