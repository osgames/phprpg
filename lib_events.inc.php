<?php
/******************************************************************************/
/*																			*/
/* lib_events.inc.php - Event Handler and Processor - the Cron, Load Balancer */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 2 April 2002													 */
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

// sort by priority, time - optimise tables... chat > sessions > pending
// also NPC handling and constructions
DbConnect();

$result_events = mysql_query("SELECT task, requested, due, frequency FROM " . PHPRPG_DB_PREFIX . "_events WHERE (assigned_id = '0' AND due < " . time() . " AND frequency != '0') ORDER BY priority, requested LIMIT " . PHPRPG_EVENT_COUNT . " FOR UPDATE");

$events_string = '';

while ($event = mysql_fetch_array($result_events))
{
	$frequency = $event['frequency'];
	switch ($event['task'])
	{
	case 'clear_chat':
		// Move chat messages longer than a specified time frame 5 mins (300s) to chat_log every minute
		$online_range = time() - 300;
		$result_insert = mysql_query("INSERT " . PHPRPG_DB_PREFIX . "_chat_log (avatar, emotion, name, map_name, map_xpos, map_ypos, contents, post_time) SELECT avatar, emotion, name, map_name, map_xpos, map_ypos, contents, post_time FROM " . PHPRPG_DB_PREFIX . "_chat WHERE (post_time < $online_range AND type = 'chat')");
		$result_delete = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_chat WHERE post_time < $online_range");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' clear_chat';
		break;

	case 'optimize_chat':
		// Optimize chat table 20 minutes
		$result = mysql_query("OPTIMIZE TABLE " . PHPRPG_DB_PREFIX . "_chat");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' optimize_chat';
		break;

	case 'clear_users_pending':
		// Clean up! -> Purge pending accounts registered greater than 48 hours (172800s) every hour
		$online_range = time() - 172800;
		$result_delete = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE rego_time < $online_range");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' clear_users_pending';
		break;

	case 'stamina_recovery':
		// Stamina recovery every 5 minutes
		// -> amount of recovery depends on fatigue (ie current STM status) and VIT

		// Bonus stamina recovery if player is camping...?

		// Check how many cycles this event might have been missed - Not a true algorithm, but better than nothing :)
		$event_cycles = round((time() - $event['due']) / (60 * 5)) + 1;

		//	STM	>60% -> 4 per 5 mins = 48 per hour
		$result_stamina_up = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM = CEILING(STM + 4 * VIT / 8) * $event_cycles WHERE (STM/STM_MAX) > 0.6 AND STM != STM_MAX");

		//	STM 30-60% -> 3 per 5 mins = 36 per hour
		$result_stamina_up = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM = CEILING(STM + 3 * VIT / 8) * $event_cycles WHERE (STM/STM_MAX) > 0.3 AND (STM/STM_MAX) <= 0.6 AND STM != STM_MAX");

		//	STM 15-30% -> 2 per 5 mins = 24 per hour
		$result_stamina_up = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM = CEILING(STM + 2 * VIT / 8) * $event_cycles WHERE (STM/STM_MAX) > 0.15 AND (STM/STM_MAX) <= 0.3 AND STM != STM_MAX");

		//	STM  0-15% -> 1 per 5 mins = 12 per hour
		$result_stamina_up = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM = CEILING(STM + 1 * VIT / 8) * $event_cycles WHERE (STM/STM_MAX) >= 0 AND (STM/STM_MAX) <= 0.15 AND STM != STM_MAX");

		$result_stamina_flat = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET STM = STM_MAX WHERE STM > STM_MAX");

		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' stamina_recovery';
		break;

	case 'health_recovery':
		// Health recovery every 5 minutes
		// -> amount of recovery depends on VIT

		// Check how many cycles this event might have been missed
		$event_cycles = round((time() - $event['due']) / (60 * 5)) + 1;

		$result_health_up = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET HP = ROUND(HP + VIT / 8) * $event_cycles");

		$result_health_flat = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET HP = HP_MAX WHERE HP > HP_MAX");

		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' health_recovery';
		break;

	case 'optimize_users_pending':
		// Optimize users_pending table every 48 hours
		$result_optimize = mysql_query("OPTIMIZE TABLE " . PHPRPG_DB_PREFIX . "_users_pending");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' optimize_users_pending';
		break;

	case 'prune_users':
		// Remove users who are not admins, are below the quick expire exp amount and have not used thier accounts in 2 weeks
		// Remove users who are not admins, are above the quick expire exp amount and have not used thier accounts in 3 months
		// Optimize users table
		$cur_time = time();
		$weeks    = $cur_time - (60*60*24*7*3);
		$months   = $cur_time - (60*60*24*7*4*3);
		$users    = PHPRPG_DB_PREFIX."_users";
		$admins   = PHPRPG_DB_PREFIX."_admins";
		$user_expire = PHPRPG_USER_EXPIRY;

		$result_prune_users = mysql_query("SELECT $users.user_id as user_id, $admins.level as user_level FROM $users LEFT OUTER JOIN $admins ON $users.user_id=$admins.user_id WHERE ( last_active <= $weeks AND EXP > $user_expire ) OR ( last_active <= $months AND EXP <= $user_expire )");
		if ($row = mysql_fetch_assoc($result_prune_users))
		{
			$items = PHPRPG_DB_PREFIX."_items";
			do
			{
				$level   = intval($row['user_level']);
				if (empty($level))
				{
					$user_id = intval($row['user_id']);
					mysql_query("DELETE FROM $items WHERE user_id=$user_id");
					mysql_query("DELETE FROM $users WHERE user_id=$user_id");
				}
			}
			while ($row = mysql_fetch_assoc($result_prune_users));
		}
		$events_string .= " prune_users";

		$result_optimize = mysql_query("OPTIMIZE TABLE $users");
		$events_string .= ' optimize_users';

		$next_update = $cur_time + $frequency;
		$events = PHPRPG_DB_PREFIX."_events";
		$result_update = mysql_query("UPDATE $events SET requested = '$cur_time', due = '$next_update' WHERE task='prune_users' LIMIT 1");
		break;

	case 'optimize_sessions':
		// Optimize session table every 24 hours
		$result_optimize = mysql_query("OPTIMIZE TABLE " . PHPRPG_DB_PREFIX . "_sessions");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' optimize_sessions';
		break;

	case 'clear_battle_messages':
		// Clean up the battle messages table every hour remove all messages 5 hours or older
		$online_range = time() - (60 * 60 * 5);
		$result_purge = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_battles WHERE post_time < $online_range");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' purge_battle_database';
		break;

	case 'optimize_battles':
		// Optimize battle messages every 24 hours
		$result_optimize = mysql_query("OPTIMIZE TABLE ". PHPRPG_DB_PREFIX . "_battles");
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' optimize_battles';
		break;

	case 'weather_update':
		require_once("./event_weather.php");
		updateWeather();
		$next_update = time() + $frequency;
		$result_update = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_events SET requested = '" . time() . "', due = '" . $next_update . "' WHERE task = '" . $event['task'] . "' LIMIT 1");
		$events_string .= ' weather_update';
		break;
	}
}



?>