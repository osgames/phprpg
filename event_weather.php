<?PHP

function updateWeather() {

	$TablePrefix = PHPRPG_DB_PREFIX;

	$sunny = 'Sunny';
	$partlyCloudy = 'Partly Cloudy';
	$mostlyCloudy = 'Mostly Cloudy';
	$rain = 'Rain';
	$fog  = 'Fog';

	$summer = 'Summer';
	$spring = 'Spring';
	$fall   = 'Fall';
	$winter = 'Winter';

	$result = mysql_query("SELECT * FROM ".$TablePrefix."_weather");

	while (list($id, $region, $currently, $wind, $wind_dir, $high, $low, $season, $last_change) = mysql_fetch_row($result)) {
		switch ($season) {
		case $summer:
			// DETERMINE WHAT IT WILL BE LIKE NEXT
			mt_srand ((double) microtime() * 1000000);
			$rand = mt_rand(1, 100);
			if ($currently == $sunny) {
				if ($rand < 50) {
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $partlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $mostlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 60) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $rain) {
				if ($rand < 50) {
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $fog) { // will only stay foggy for 1 turn
				if ($rand < 25) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 50) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				}
			}
			// DETERMINE THE TEMP
			mt_srand ((double) microtime() * 1000000);
			$high = mt_rand(85, 100);
			$low = mt_rand(75, 85);
			break;
		case $fall:
			mt_srand ((double) microtime() * 1000000);
			$rand = mt_rand(1, 100);
			if ($currently == $sunny) {
				if ($rand < 40) {
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $partlyCloudy) {
				if ($rand < 40) {
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 50) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 80) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $mostlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 65) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $rain) {
				if ($rand < 50) {
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $fog) { // will only stay foggy for 1 turn
				if ($rand < 25) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 50) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				}
			}
			// DETERMINE THE TEMP
			mt_srand ((double) microtime() * 1000000);
			$high = mt_rand(40, 60);
			$low = mt_rand(25, 39);
			break;
		case $spring:
			mt_srand ((double) microtime() * 1000000);
			$rand = mt_rand(1, 100);
			if ($currently == $sunny) {
				if ($rand < 50) {
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $partlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $mostlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 60) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $rain) {
				if ($rand < 50) {
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $fog) { // will only stay foggy for 1 turn
				if ($rand < 25) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 50) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				}
			}
			// DETERMINE THE TEMP
			mt_srand ((double) microtime() * 1000000);
			$high = mt_rand(66, 80);
			$low = mt_rand(50, 65);
			break;
		case $winter:

			mt_srand ((double) microtime() * 1000000);
			$rand = mt_rand(1, 100);
			if ($currently == $sunny) {
				if ($rand < 30) {
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 70) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 85) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $partlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 60) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 80) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 95) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $mostlyCloudy) {
				if ($rand < 50) {
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 65) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $rain) {
				if ($rand < 50) {
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 55) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 90) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $fog;
					$wind = chooseWind($fog);
					$wind_dir = chooseWindDir();
				}
			} elseif ($currently == $fog) { // will only stay foggy for 1 turn
				if ($rand < 25) {
					$currently = $sunny;
					$wind = chooseWind($sunny);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 50) {
					$currently = $partlyCloudy;
					$wind = chooseWind($partlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand < 75) {
					$currently = $mostlyCloudy;
					$wind = chooseWind($mostlyCloudy);
					$wind_dir = chooseWindDir();
				} elseif ($rand <= 100) {
					$currently = $rain;
					$wind = chooseWind($rain);
					$wind_dir = chooseWindDir();
				}
			}
			// DETERMINE THE TEMP
			mt_srand ((double) microtime() * 1000000);
			$high = mt_rand(20, 40);
			$low = mt_rand(0, 19);
			break;
		} //switch $season

		$threeMonths = (60*60*24*28*3)/4;

		if (time() > (time() + $threeMonths)) {
			if ($season == $summer)
				$season = $fall;
			elseif ($season == $fall)
				$season = $winter;
			elseif ($season == $winter)
				$season = $spring;
			elseif ($season == $spring)
				$season = $summer;
		}

		$sql = "UPDATE $TablePrefix"."_weather SET currently = '$currently', wind = '$wind', wind_dir = '$wind_dir', high = '$high', low = '$low', season = '$season', last_change = '".(time() + $threeMonths)."' WHERE region = '$region'";
		mysql_query($sql);
	} // while list()
}

function chooseWind($thing) {
	mt_srand ((double) microtime() * 1000000);
	$dir = 0;

	$sunny = 'Sunny';
	$partlyCloudy = 'Partly Cloudy';
	$mostlyCloudy = 'Mostly Cloudy';
	$rain = 'Rain';
	$fog  = 'Fog';

	switch ($thing) {
	case $sunny:
		return mt_rand(1,10);
		break;
	case $partlyCloudy:
		return mt_rand(1,15);
		break;
	case $mostlyCloudy:
		return mt_rand(5,15);
		break;
	case $rain:
		return mt_rand(1,20);
		break;
	case $fog:
		return mt_rand(1,10);
		break;
	}
}

function chooseWindDir() {
	mt_srand ((double) microtime() * 1000000);
	$dir = mt_rand(1,6);
	switch ($dir) {
	case 1:
		return "n";
		break;
	case 2:
		return "ne";
		break;
	case 3:
		return "se";
		break;
	case 4:
		return "s";
		break;
	case 5:
		return "sw";
		break;
	case 6:
		return "nw";
		break;
	default:
		return "n";
	}
}


?>