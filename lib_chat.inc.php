<?php
/******************************************************************************/
/*																			*/
/* lib_chat.inc.php - Chat Box (template_chat.inc.php) parser				*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 20 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

if (!empty($voice))
{
	$voice = strip_tags($voice);

	// Emoticons with greater number of characters go first
	if (eregi('\^_\^$', $voice))
	{
		$voice = substr($voice, 0, -3); $emotion = 'sml';
	}
	elseif (eregi('>:\($', $voice))
	{
		$voice = substr($voice, 0, -3); $emotion = 'ang';
	}
	elseif (eregi('-_-$', $voice))
	{
		$voice = substr($voice, 0, -3); $emotion = 'ano';
	}
	elseif (eregi('O_o$', $voice))
	{
		$voice = substr($voice, 0, -3); $emotion = 'spe';
	}
	elseif (eregi(':\)$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'sml';
	}
	elseif (eregi('X\)$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'sml';
	}
	elseif (eregi(':\($', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'sad';
	}
	elseif (eregi(':\o$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'shk';
	}
	elseif (eregi(':\O$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'shk';
	}
	elseif (eregi(':\|$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'emb';
	}
	elseif (eregi(':.$', $voice))
	{
		$voice = substr($voice, 0, -2); $emotion = 'emb';
	}


	// temp..
	$emotion = 'std';

	DbConnect();

	$query = "INSERT " . PHPRPG_DB_PREFIX . "_chat (type, avatar, emotion, name, map_name, map_xpos, map_ypos, contents, post_time) ";
	$query .= "VALUES ('chat', '" . $char['avatar'] . "', '$emotion', '" . $char['name'] . "', '" . $char['map_name'] . "', " . $char['map_xpos'] . ", " . $char['map_ypos'] . ", '$voice', '" . time() . "')";
	$result = mysql_query($query);
}


?>