<?php

error_reporting (E_ALL);

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

DbConnect();


/* Clear_attacker: Takes and clears the entire attacker list for that user_id			*/
/* $user_id: The user_id who's whole list is to be cleared.								*/
function Clear_attacker($user_id)
{
	$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET attackers='' WHERE user_id=$user_id LIMIT 1");
}

/* Add_attacker: Takes the current attacker and adds them to the start of the defeneder's */
/* attacker list.  It rebuilds the list making sure to keep it under the limit by dropping*/
/* the oldest attacker and also removes the previous instance of the current attacker.	*/
/* $defender_user_id: The user_id of the person being attacked, needs their list changed. */
/* $attacker_user_id: The user_id of the person who needs to be added to the list.		*/

function Add_attacker($defender_user_id, $attacker_user_id)
{
	$result = mysql_query("SELECT attackers FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$defender_user_id LIMIT 1");

	$attacker_string = mysql_fetch_row($result);

	$attacker_list = explode(',', $attacker_string[0]);

	$at_pos = array_search($attacker_user_id, $attacker_list);

	$new_attacker_list[] = $attacker_user_id;
	$array_pos = 0;
	while($array_pos < count($attacker_list))
	{
		if ($array_pos != $at_pos && $attacker_list[$array_pos] != '')
		 $new_attacker_list[] = $attacker_list[$array_pos];
		$array_pos++;
	}

	while (count($new_attacker_list) > MAX_ATTACKERS)
	{
	 array_pop($new_attacker_list);
	}

	$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET attackers='" . implode(',', $new_attacker_list) . "' WHERE user_id=$defender_user_id LIMIT 1");
}

function Modify_interactions($target_user_id, $the_user_id, $modify_type, $modify_what)
{
	$result = mysql_query("SELECT name FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=" . $target_user_id . " LIMIT 1");

	$temp = mysql_fetch_row($result);

	$name = $temp[0];

	if ($modify_type != 'add' && $modify_type != 'remove')
	{
		$return_string = 'That is not a valid command!';
		return $return_string;
	}

	if ($modify_what != 'friends' && $modify_what != 'enemies')
	{
		$return_string = 'Invalid list to modify!';
		return $return_string;
	}

	$result = mysql_query("SELECT " . $modify_what . " FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=" . $the_user_id . " LIMIT 1");

	$temp = mysql_fetch_row($result);

	$the_array = explode(',', $temp[0]);

	$count = count($the_array);

	if ($modify_what == 'friends')
	{
		$max_count = MAX_FRIENDS;
	} else if ($modify_what == "enemies") {
		$max_count = MAX_ENEMIES;
	}

	if ($modify_type == 'add' && $count == $max_count)
	{
		$return_string = 'Can not add ' . $name . ' to your ' . $modify_what . ' list.  Max amount reached.';
		return $return_string;
	}

	$foundat = array_search($target_user_id, $the_array);

	if ($modify_type == 'remove' && $foundat === FALSE)
	{
		$return_string = $name . ' is not in the ' . $modify_what . ' list to remove.';
		return $return_string;
	}

	if ($modify_type == 'add' && !($foundat === FALSE))
	{
		$return_string = $name . ' is already on your ' . $modify_what . ' list.';
		return $return_string;
	}

	if ($the_array[0] == '')
	{
		$the_array = array();
	}

	if ($modify_type == 'add')
	{
		$the_array[] = $target_user_id;
	} else {
		$temp_array = $the_array;
		unset($the_array);
		$the_array = array();
		$position = 0;

		while ($position < $count)
		{
			if ($position != $foundat)
			{
				$the_array[] = $temp_array[$position];
			}
			$position++;
		}
	}

	$temp = implode(',', $the_array);

	$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET " . $modify_what . "='" . $temp . "' WHERE user_id=" . $the_user_id . " LIMIT 1");

	if ($modify_type == 'add')
	{
		$text = ' added to ';
	} else {
		$text = ' removed from ';
	}
	$return_string = $name . $text . 'your ' . $modify_what . ' list.';
	return $return_string;
}

/*  Get_interaction_list: Gets friend, enemy and attacker lists and stores them in	*/
/*  an array, which it returns.														*/
/*  Friend_user_id : User_id of the person who's list we're getting.				 */
/*  User_map_x : The current map x position.										 */
/*  User_map_y : The current map y position.										 */
/*  If user_map_x, user_map_y are left out then it gets the whole list.				*/

function Get_interaction_list($the_user_id, $user_map_x = NULL, $user_map_y = NULL)
{
	$interactions_array = array();

	$result = mysql_query("SELECT friends, enemies, attackers FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id=$the_user_id LIMIT 1");

	$strings_array = mysql_fetch_row($result);

	if ($user_map_x == NULL || $user_map_y == NULL)
	{
	 $limit = "";
	} else {
	 $limit = "AND map_xpos='$user_map_x' AND map_ypos='$user_map_y'";
	}

	$loop_count = 0;
	$array_count = 0;

	while ($loop_count < 3)
	{
		$result = mysql_query("SELECT user_id, name, avatar FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_id IN($strings_array[$loop_count]) $limit");

		if ($result == FALSE)
		{
	$loop_count++;
		continue;
		}

		if ($loop_count == 0)
		{
		 $interaction_flag = INTERACTION_FRIEND;
		} else if ($loop_count == 1) {
		 $interaction_flag = INTERACTION_ENEMY;
		} else {
		 $interaction_flag = INTERACTION_ATTACKER;
		}

		$array_key = NULL;

		while ($player = mysql_fetch_array($result, MYSQL_ASSOC))
		{
		$found_in = Search_sub_arrays($player['user_id'], $interactions_array);
			if (!($found_in === NULL))
			{
				$interactions_array[$found_in]['type_flag'] |= $interaction_flag;
			} else {
				$player['type_flag'] = $interaction_flag;
				$interactions_array[$array_count] = $player;
				$array_count++;
			}
		}
		$loop_count++;
	}
	return $interactions_array;
}


/* Set_interaction_list: Takes the array passed to it and rebuilds the list.				*/
/* NOTE: It doesn't rebuild the list for the attackers as those have to stay in			*/
/* the same order.  Attacker list can only be modified by using Clear_atttackers to delete */
/* ALL of the attackers from it or Add_attacker to add a new one.						 */
/* $the_user_id: The user_id for the person who's list is being updated.					*/
/* $list_array: The array that contains the updated list.								 */

function Set_interaction_list($the_user_id, $list_array)
{
	$num_records = count($list_array);

	$interaction_strings = array('friends' => '', 'enemies' => '', 'attackers' => '');

	$cur_count = 0;

	while ($cur_count < $num_records)
	{
		$player = $list_array[$cur_count];
		if ($player['type_flag'] & INTERACTION_FRIEND)
		{
			if ($num_interactions['friends'] > 0)
				$interaction_strings['friends'] .= ',';
		$interaction_strings['friends'] .= $player['user_id'];
		}
		if ($player['type_flag'] & INTERACTION_ENEMY)
		{
			if ($num_interactions['enemies'] > 0)
				$interaction_strings['enemies'] .= ',';
		$interaction_strings['enemies'] .= $player['user_id'];
		}
		if ($player['type_flag'] & INTERACTION_ATTACKER)
		{
			if ($num_interactions['attackers'] > 0)
				$interaction_strings['attackers'] .= ',';
		$interaction_strings['attackers'] .= $player['user_id'];
		}
		$cur_count++;
	}

	$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET friends='" . $interaction_strings['friends'] . "', enemies='" . $interaction_strings['enemies'] . "', attackers='" . $interaction_strings['attackers'] . "' WHERE user_id=$the_user_id LIMIT 1");

	return $interaction_strings;
}


?>