<?php
/******************************************************************************/
/*																			  */
/* Friends.php - Friend List												  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 22 August 2001													  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_events.inc.php');
require_once('lib_interactions.inc.php');
require_once('lib_session.inc.php');
require_once('lib_template.inc.php');

$option = Retrieve_var('option');
$target = Retrieve_var('target');
$type = Retrieve_var('type');

require_once('template_header.inc.php');

echo '
<table cellpadding="0" cellspacing="10" width="100%" border="0">
<tr><td width="100%" valign="top">
';

OpenTable('title', '100%');
echo 'p h &#39; @ n t a s y';

OpenTable('content');
echo '
<div align="justify">
Friends Mode @ ' . $char['map_name'] . '<br>
';

require_once('template_menu.inc.php');

echo '
<table cellpadding="0" cellspacing="15" border="0">
<tr><td valign="top">
';

// Navigation window
require_once('template_nav.inc.php');

echo '
</td><td valign="top" width="100%">
';

// Chat box
require_once('template_chat.inc.php');

echo '
<div align="right">
';

// Items on the ground
require_once('template_dropped.inc.php');

echo '
</div>
';

echo '
</td></tr></table>
</div>';

OpenTable('close');
echo '
</td><td width="172" valign="top">
';

// Stats Box
require_once('template_stats.inc.php');

echo '
</td></tr></table>
';

require_once('template_footer.inc.php');


?>