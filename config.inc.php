<?php

/******************************************************************************/
/*																			  */
/* config.inc.php - Configuration file										  */
/*																			  */
/******************************************************************************/

/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 3 June 2001														  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/





/*																			  */
/* General Configuration													  */
/* ---------------------													  */
/* PHPRPG_DEBUG : Project debug mode, enter a boolean value (True or False)	  */
/* PHPRPG_DEBUG_RV_SHOW: Shows variables that have to be retrieved.			  */
/* PHPRPG_DEBUG_AUTODIRECT: Determines whether it directs using a link or	  */
/*							header().  True means use a link.				  */
/* WEB_SERVER : Allow you to run game locally, without having to change names */
/* PHPRPG_BASE : Base URL to where the main files are located, with trailing  */
/*				slash '/' (e.g. 'http://www.yourwebserver.com/game_one/')	  */
/* PHPRPG_IMG : Base URL to where the image files are located; again, include */
/*			 the trailing slash												  */
/* PHPRPG_MAIL : Appear as the sender's e-mail address on e-mails and notices */
/*				sent to players												  */
/* PHPRPG_SETUP_PW: Setup password                                            */
/*																			  */

define('PHPRPG_DEBUG', True);
define('PHPRPG_DEBUG_RV_SHOW', True);
define('PHPRPG_DEBUG_AUTOREDIRECT', True);

define('WEB_SERVER', 'True');

if (WEB_SERVER == 'True')
{
	define('PHPRPG_BASE', 'http://www.yourwebserver.com/phantasy/');
	define('PHPRPG_IMG', 'http://www.yourwebserver.com/phantasy/images/');
} else if (WEB_SERVER == 'False') {
	define('PHPRPG_BASE', 'http://localhost/phantasy/');
	define('PHPRPG_IMG', 'http://localhost/phantasy/images/');
}
define('PHPRPG_MAIL',     'noreply@yourwebserver.com');
define('PHPRPG_SETUP_PW', 'god');

/*																			  */
/* MySQL Database Configuration												  */
/* ----------------------------												  */
/* PHPRPG_DB_HOST : MySQL host; server name only (e.g. 'mysql.webserver.com') */
/* PHPRPG_DB_NAME : Name of MySQL database									  */
/* PHPRPG_DB_USER : MySQL username											  */
/* PHPRPG_DB_PASS : MySQL password											  */
/* PHPRPG_DB_PREFIX : The prefix prepended to all table names				  */
/*																			  */

define('PHPRPG_DB_HOST', 'localhost');
define('PHPRPG_DB_NAME', 'phantasy');
define('PHPRPG_DB_USER', 'username');
define('PHPRPG_DB_PASS', 'password');
define('PHPRPG_DB_PREFIX', 'ntasy');

/*																			*/
/* Game Configuration														*/
/* ------------------														*/
/* PHPRPG_SESSION_EXPIRY : Seconds before automatic session termination		*/
/* PHPRPG_EPOCH : UNIX TIMESTAMP that defines the start of time				*/
/* WEATHER_UPDATE_FREQ : Seconds between weather updates                    */
/* PHPRPG_EVENT_COUNT : Number of events to process in one batch            */
/*																			*/

define('PHPRPG_SESSION_EXPIRY',	1200);
define('PHPRPG_EPOCH',			94737600);
define('PHPRPG_EVENT_COUNT',	3);

/*																			  */
/* In Game Settings															  */
/* ----------------															  */
/* MAX_USERS_PER_TILE : Number of users visible before it's a crowd.		  */
/* MAX_FRIENDS : Maximum number of friends a player can have.				  */
/* MAX_ENEMIES : Maximum number of enemies a player can set.				  */
/* MAX_ATTACKERS : Maximum number of last attackers to keep.			  */
/*																			  */

define('MAX_USERS_PER_TILE',	25);
define('MAX_FRIENDS',			20);
define('MAX_ENEMIES',			20);
define('MAX_ATTACKERS',			20);

// Widths to format the view correctly.

define('WIDTH_AVATAR_ICON',		49);
define('WIDTH_BATTLE_ICON',		22);
define('WIDTH_INTERACTION',		20);

/*																			  */
/* Interaction Settings														  */
/* INTERACTION_FRIEND, INTERACTION_ENEMY, INTERACTION_ATTACKER				  */
/* Flags to be used to determine when displaying people what icons			  */
/* to give them.															  */
/*																			  */

define('INTERACTION_FRIEND',	1);
define('INTERACTION_ENEMY',		2);
define('INTERACTION_ATTACKER',	4);

/*																			  */
/* Administrator settings													  */
/* These variables are for the minimum administrator level needed to do each  */
/* particular thing.														  */
/*																			  */

define('ADMIN_TRACKER', '2');
define('ADMIN_MAP_EDITOR', '8');
define('ADMIN_EVENTS', '10');
define('ADMIN_HALT_GAME', '11');
define('ADMIN_TELEPORT', '11');

/*																			  */
/* Version settings															  */
/* These settings are used for determining if we are running the latest		  */
/* versions of things.  GAME_VERSION doesn't have a use right now.			  */
/* DATABASE_VERSION will be used to allow updates to happen without having to */
/* dump the whole thing and start from scratch.								  */
/*																			  */

define('GAME_VERSION', '0.5.42');
define('DATABASE_VERSION', '0.5.42');

/*																			  */
/* DO NOT MODIFY BEYOND THIS POINT											  */
/* ----------------------------												  */
/* PHP_SELF: Script name only (no path)										  */
/*																			  */



ereg("([^\\/]*)$", $_SERVER['PHP_SELF'], $PHPSELF);

define('PHP_SELF', $PHPSELF[1]);

?>