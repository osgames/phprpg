<?php
/******************************************************************************/
/*																			*/
/* template_footer.inc.php - HTML Footer									 */
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 15 March 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
	exit;
}

DbConnect();

if (PHPRPG_DEBUG)
{
	$exec_time = StopTiming($start_time);

	echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="10" border="0" alt=""><br>
<div align="center"><font color="' . BackgroundColour(-50) . '" style="font: 11px">
[script: ' . PHP_SELF . ']
[exec: ' . $exec_time . 'ms]
[host: ' . $_SERVER['HTTP_HOST'] . ']
	';

	@$uptime_result = exec('/usr/bin/uptime');

	if ($uptime_result)
	{
		// For situation like this -> 12:05pm up 153 days, 17:01, 0 users, load average: 8.02, 7.91, 7.67
		if (!eregi('up ([0-9]*)[ days, ]*([0-9]*):([0-9]*)', $uptime_result, $matches))
		{
			// For situation like this -> 12:04pm up 153 days, 26 min, 0 users, load average: 6.06, 5.75, 5.54
			eregi('up ([0-9]*)[ days, ]*()([0-9]*) min', $uptime_result, $matches);
			$matches[2] = '0';
		}
		$uptime = $matches[1] . 'd ' . $matches[2] . 'h ' . $matches[3] . 'm';

		eregi('load average: ([0-9]*.[0-9]*),', $uptime_result, $matches);
		$load_average = $matches[1];
		echo '
[uptime: ' . $uptime . ']
[load average: ' . $load_average . ']
		';
	}

	echo '<br>';

	if (!empty($events_string))
	{
		echo '[event:' . $events_string . ']<br>';
	}

	echo '
</font></div>
	';
}

echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="10" border="0" alt=""><br>
</div>
</td></tr>
</table>
</body>
</html>
';

?>