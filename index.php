<?php
/******************************************************************************/
/*																			  */
/* index.php - phpRPG log-in / title page									  */
/*																			  */
/******************************************************************************/
/*																			  */
/* Requirements: PHP, MySQL and web-browser									  */
/*																			  */
/* Author: Timothy TS Chung													  */
/*		<ttschung@users.sourceforge.net>									  */
/*																			  */
/* Created: 3 June 2001														  */
/*																			  */
/* Copyright (c) 2001-2002 Timothy TS Chung									  */
/*																			  */
/* This file is part of phpRPG (http://phpRPG.org/)							  */
/*																			  */
/* phpRPG is free software; you can redistribute it and/or modify			  */
/* it under the terms of the GNU General Public License as published by		  */
/* the Free Software Foundation; either version 2 of the License, or		  */
/* (at your option) any later version.										  */
/*																			  */
/* This program is distributed in the hope that it will be useful,			  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			  */
/* GNU General Public License for more details.								  */
/*																			  */
/* You should have received a copy of the GNU General Public License		  */
/* along with this program; if not, write to the Free Software				  */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			  */
/******************************************************************************/


error_reporting (E_ALL);

require_once('config.inc.php');
require_once('lib.inc.php');
require_once('lib_template.inc.php');
require_once('lib_events.inc.php');

DbConnect();

$option = Retrieve_var('option');

if (PHPRPG_DEBUG) {	$start_time = StartTiming(); }

switch ($option)
{
	case 'activate':
		// Activating an account

		$user_name = Retrieve_var('user_name');
		$validate = Retrieve_var('validate');

		require_once('template_header.inc.php');

		OpenTable('title', '600');
		echo 'Account Activation';

		OpenTable('content');
		if (isset($user_name) && isset($validate))
		{
			$result = mysql_query("SELECT user_name, user_pass, name, email, validate FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE user_name='$user_name' AND validate='$validate' LIMIT 1");
			if ($result)
			{
				// Is there a matching pair of username and validation string in the pending table?
				if (mysql_num_rows($result) != 0)
				{
					// Yes -> insert a new row in the main user table

					// Starting values of all stats
					$start_map_name = 'Realm of Souls';
					$start_map_x = 1;
					$start_map_y = 2;
					$start_race = 'Soul';
					$start_avatar = 'soul';

					list($user_name, $user_pass, $name, $email, $validate) = mysql_fetch_row($result);
					$result  = mysql_query("
					INSERT " . PHPRPG_DB_PREFIX . "_users SET
					user_name='$user_name',
					user_pass='" . md5($user_pass) . "',
					email='$email',
					name='$name',
					HP=20,
					HP_MAX=20,
					MP=10,
					MP_MAX=10,
					STM=200,
					STM_MAX=200,
					EXP=0,
					GP=0,
					STR=4,
					NTL=4,
					PIE=4,
					VIT=4,
					DEX=4,
					SPD=4,
					CW=0,
					CC=80,
					map_name='$start_map_name',
					map_xpos=$start_map_x,
					map_ypos=$start_map_y,
					last_active=0,
					race='$start_race',
					avatar='$start_avatar',
					host='" . HostIdentify() . "',
					friends='',
					enemies='',
					attackers=''
					") or die('Database Error: ' . mysql_error() . '<br>');
					// Remove from the pending table
					$result = mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE user_name = '$user_name' LIMIT 1");
					echo 'Congratulations ' . $name . ', your account has been activated!<br><br><a href="index.php">Return to login</a>';
				}
				else
				{
					// No - return error to user
					echo 'Validation Error: Your account has either been activated or does not exist in our database!<br>';
					echo 'Please note that accounts not activated within 48 hours are automatically deleted.';
				}
			}
			else
			{
				echo 'Database Error: ' . mysql_error();
			}
		}
		else
		{
			echo 'Validation Error: The information you are providing is either incorrect or incomplete!';
		}
		OpenTable('close');
		require_once('template_footer.inc.php');
		break;

	case 'validate':
		// Validate new account registration

		$user_name = Retrieve_var('user_name');
		$user_pass1 = Retrieve_var('user_pass1');
		$user_pass2 = Retrieve_var('user_pass2');
		$email = Retrieve_var('email');
		$name = Retrieve_var('name');

		$error_msg = '';

		if (!($user_name && $user_pass1 && $user_pass2 && $email && $name))
		{
			// missing required field
			$error_msg = 'One or more of the required fields are empty.';
		}
		elseif ($user_pass1 != $user_pass2)
		{
			// password mismatch
			$error_msg = 'Passwords do not match.';
		}
		elseif (eregi(' ', $user_pass1))
		{
			// password contained empty space
			$error_msg = 'Passwords cannot contain empty space.';
		}
		elseif (!(eregi('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$', $email)))
		{
			// invalid email
			$error_msg = 'Invalid e-mail!<br><font size="1">A valid e-mail address is required to complete the registration.</font>';
			$email = '*Error';
		}
		elseif (!(eregi('^[a-z0-9]*$', $user_name)))
		{
			// invalid username
			$error_msg = 'A username may only contain alpha-numeric characters.';
			$user_name = '*Error';
		}
		elseif (!(eregi('^[a-z]*$', $name)))
		{
			// invalid name
			$error_msg = 'A character name may only compose of alphabets.<br><font size="1">Please Kindly put some thoughts choosing your character name as a sign of respect to the roleplaying community.</font>';
			$name = '*Error';
		}
		else
		{
			// no problems with form, so far...
			$result1 = mysql_query("SELECT user_id FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_name='$user_name' LIMIT 1");
			$result2 = mysql_query("SELECT user_name FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE user_name='$user_name' LIMIT 1");
			if ((mysql_num_rows($result1) != 0) || (mysql_num_rows($result2) != 0))
			{
				// username exists in either main or pending user database
				$error_msg = 'Sorry, the username ' . $user_name . ' has already been taken!';
				$user_name = "*$user_name";
			}
			$result1 = mysql_query("SELECT user_id FROM " . PHPRPG_DB_PREFIX . "_users WHERE name='$name' LIMIT 1");
			$result2 = mysql_query("SELECT user_name FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE name='$name' LIMIT 1");
			if ((mysql_num_rows($result1) != 0) || (mysql_num_rows($result2) != 0))
			{
				// character name already exists
				$error_msg = 'Sorry, the character name ' . $name . ' has already been taken!';
				$name = "*$name";
			}
			$result1 = mysql_query("SELECT user_id FROM " . PHPRPG_DB_PREFIX . "_users WHERE email='$email' LIMIT 1");
			$result2 = mysql_query("SELECT user_name FROM " . PHPRPG_DB_PREFIX . "_users_pending WHERE email='$email' LIMIT 1");
			if ((mysql_num_rows($result1) != 0) || (mysql_num_rows($result2) != 0))
			{
				// email already exists
				$error_msg = 'An account has already been registered under this e-mail address!';
				$email = "*$email";
			}
		}

		if ($error_msg == '')
		{
			// Passed validation
			// Generate a random validation string (length of 10 characters)
			$validation_string = '';
			mt_srand((double) microtime() * 100000);
			for ($i = 0; $i < 10; $i++)
			{
				// ASCII for capital letters range from 65 - 90, digits are from 48 - 57
				$rand_val = mt_rand(65, 100);
				if ($rand_val > 90)
				{
					// if $rand_val > 90, then 91 would generate digit '0'
					// ... 100 would generate digit '9'
					$rand_val = $rand_val - 91;
					$validation_string .= $rand_val;
				} else {
					// generate upper case letter
					$validation_string .= chr($rand_val);
				}
			}

			// Insert user details into pending table
			$user_time = time();
			$result = mysql_query("
			INSERT " . PHPRPG_DB_PREFIX . "_users_pending (user_name, user_pass, name, email, validate, rego_time) VALUES
			('$user_name', '$user_pass1', '$name', '$email', '$validation_string', $user_time)
			");

			$message  = "Greetings $name,\n\n";
			$message .= "We have recently received your application at " . PHPRPG_BASE . ".\n\n";
			$message .= "To complete the registration, you will have to validate your account through the link below:\n\n";
			$message .= PHPRPG_BASE . "index.php?option=activate&user_name=$user_name&validate=$validation_string\n\n";
			$message .= "Once your account has been activated, you can login with:\n";
			$message .= " > username: $user_name\n";
			$message .= " > password: $user_pass1\n\n";
			$message .= "PLEASE NOTE: Accounts not activated in 48 hours are considered unsuccessful and will automatically be deleted.\n\n";
			$message .= "Thank you for registering with phpRPG. I hope you'll enjoy it!!\n\n";
			$message .= "Timothy Chung (aka Axolotl)\n\n";
			$message .= "http://phpRPG.org/\n";
			$message .= "http://sourceforge.net/projects/phprpg/\n\n";

			$subject = '[phpRPG] Validation Code';

			$headers  = "From: phpRPG Registrar <" . PHPRPG_MAIL . ">\n";
			$headers .= "X-Sender: phpRPG Registrar <" . PHPRPG_MAIL . ">\n";
			$headers .= "X-Mailer: PHP " . phpversion() . "\n";
			$headers .= "X-Priority: 1\n";
			$headers .= "Return-Path: phpRPG Registrar <" . PHPRPG_MAIL . ">\n";

			// Send validation email! ~ how exciting! :)
			mail($email, $subject, $message, $headers);

			require_once('template_header.inc.php');

			OpenTable('title', '600');
			echo 'Account Registration';

			OpenTable('content');
			echo '
			<br>Thank you for registering.<br><br>
			You will have to validate your account before you can continue. Please check your e-mail for details.<br><br>
			<b>NOTE:</b> To conserve resources, accounts not activated in 48 hours are considered unsuccessful and will
			automatically be deleted from the server.<br><br>
			<a href="' . PHPRPG_BASE . '">Return to main</a>
			';
			OpenTable('close');

			require_once('template_footer.inc.php');

			break;
		}
		else
		{
			// Registration error - pass variables and fall through to case 'register'
			$error_msg = 'Error: ' . $error_msg . '<br><br>';
		}

		// Fall through if account registration fail

	case 'register':
		// New account registration

		require_once('template_header.inc.php');

		echo '<form action="index.php?option=validate" method="post" autocomplete="off">';

		OpenTable('title', '600');
		echo 'Account Registration';

		OpenTable('content');
		echo '<div align="right"><br>';

		if (@$error_msg) { echo $error_msg; }

		echo '
		<table cellpadding="5" cellspacing="0" border="0">
			<tr>
				<td><div align="right">Username:<br><font size="1">Your login username is not case sensitive.</font></div></td>
				<td><input type="text" name="user_name" maxlength="20" size="24" value="' . @$user_name . '" class="input"></td>
			</tr>
			<tr>
				<td><div align="right">Password:</div></td>
				<td><input type="password" name="user_pass1" maxlength="20" size="24" class="input"></td>
			</tr>
			<tr>
				<td><div align="right">Confirm Password:</div></td>
				<td><input type="password" name="user_pass2" maxlength="20" size="24" class="input"></td>
			</tr>
			<tr>
				<td><div align="right">Email:<br><font size="1">You must provide a valid e-mail to activate your account.</font></div></td>
				<td><input type="text" name="email" maxlength="60" size="24" value="' . @$email . '" class="input"></td>
			</tr>
			<tr>
				<td><div align="right">Character Name:<br><font size="1">Please respect the roleplaying community by selecting a fantasy character name.<br>Do not use names which are out of context.</font></div></td>
				<td><input type=text name="name" maxlength="20" size="24" value="' . @$name . '" class="input"></td>
			</tr>
		</table>
		<input type="image" name="Register" src="' . PHPRPG_IMG . 'register.png" width="119" height="33" border="0" alt=""></div>
		';
		OpenTable('close');
		echo '</form>';

		include('template_footer.inc.php');

		break;

	case 'login':
		// Login verification

		$user_name = Retrieve_var('user_name');
		$user_pass = Retrieve_var('user_pass');

		if (!empty($user['user_id'])) { unset($user['user_id']); }

		$result = mysql_query("SELECT user_id, name FROM " . PHPRPG_DB_PREFIX . "_users WHERE user_name='$user_name' AND user_pass='$user_pass' LIMIT 1");
		$user = mysql_fetch_array($result);

		if ($user['user_id'])
		{
			// User exists! -> Starts session after deleting old sessions from user.
			$result = mysql_query("SELECT id, data, time_stamp FROM " . PHPRPG_DB_PREFIX . "_sessions");

			while ($res = mysql_fetch_array($result))
			{
				$sess_user = explode('"', $res['data']);

				if (isset($sess_user[1]))
				{
					if ($sess_user[1] == $user['user_id'])
					{
						mysql_query("DELETE FROM " . PHPRPG_DB_PREFIX . "_sessions WHERE id = '" . $res['id'] . "'");
					}
				}
			}
			// Generate new random session id
			mt_srand((double)microtime()*100000);
			$s = md5(uniqid(mt_rand()));

			require_once('./lib_session.inc.php');

			$user_id = $user['user_id'];
			$user_time = time();

			$_SESSION['user_id'] = $user_id;
			$_SESSION['user_time'] = $user_time;
			$_SESSION['sw'] = Retrieve_var('screenwidth');

			$user_host = HostIdentify();
			$result = mysql_query("UPDATE " . PHPRPG_DB_PREFIX . "_users SET last_active='$user_time', host='$user_host' WHERE user_id='$user_id' LIMIT 1");

			require_once('template_header.inc.php');

			OpenTable('title', '600');
			echo 'Welcome to ph&#39;@ntasy!';

			OpenTable('content');
			echo 'Greetings, ' . $user['name'] . '...<br>';

			// Display admin notes to players
			require_once('notes.php');

			echo '<br><div align="right"><a href="local.php?s=' . $s . '"><img src="' . PHPRPG_IMG . 'start.png" width="81" height="30" border="0" alt=""></a></div>';
			OpenTable('close');

			require_once('template_footer.inc.php');

			break;
		} else {
			// Incorrect login or database error -> fallthrough
			$login_error = True;
		}

	default:
		// Default page

		require_once('template_header.inc.php');

		// Log visitor's information
		if (empty($_SERVER['HTTP_USER_AGENT'])) { $_SERVER['HTTP_USER_AGENT'] = getenv('HTTP_USER_AGENT'); }
		if (empty($_SERVER['HTTP_REFERER'])) { $_SERVER['HTTP_REFERER'] = getenv('HTTP_REFERER'); }

		$result = mysql_query("INSERT DELAYED " . PHPRPG_DB_PREFIX . "_activity (day, hour, agent, host, ref) VALUES ('" . date('Y-m-d', mktime()) . "', " . date('H', mktime()) . ", '" . $_SERVER['HTTP_USER_AGENT'] . "', '" . HostIdentify() . "', '" . $_SERVER['HTTP_REFERER'] . "')") or die('Database Error: ' . mysql_error() . '<br>');

		echo "\n<script language='Javascript' src='md5.js'></script>";
		echo "\n<script language='Javascript'>";
		echo "\n<!--";
		echo "\nfunction loginonsubmit()";
		echo "\n{";
		echo "\ndocument.forms['login_form'].screenwidth.value = screen.width;";
		echo "\ndocument.forms['login_form'].user_pass.value = calcMD5(document.forms['login_form'].user_passMD5.value);";
		echo "\ndocument.forms['login_form'].user_passMD5.value = '';";
		echo "\nreturn true;";
		echo "\n}";
		echo "\n-->";
		echo "\n</script>";
		echo '
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="10" border="0" alt=""><br>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td width="100%"><img src="' . PHPRPG_IMG . 'x.png" width="0" height="122" border="0" alt=""><br></td>
<td width="547"><img src="' . PHPRPG_IMG . 'phantasy_top.png" width="547" height="122" border="0" alt=""></td>
</tr>
<tr>
<td bgcolor="#000000" align="left" valign="bottom">

<form action="index.php?option=login" method="post" name="login_form" autocomplete="off" onSubmit="loginonsubmit();">
<input type="hidden" name="user_pass" value="">

<table cellpadding="3" cellspacing="0" border="0">
		';

		if (@$login_error) { echo '<tr><td></td><td colspan="2"><font size="2"><i>Login error!</i></font></td></tr>'; }

		echo '
<tr>
<td><img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="10" border="0" alt=""></td>
<td><font size="2">username</font></td>
<td><input type="text" name="user_name" size="20" maxlength="22" class="input"></td>
</tr>
<tr>
<td></td>
<td><font size="2">password</font></td>
<td><input type="password" name="user_passMD5" size="20" maxlength="22" class="input"></td>
</tr>
<tr><td></td><td colspan="2"><div align="right">
<a href="index.php?option=register"><font size="1">[register]</font></a>
<a href="http://sourceforge.net/projects/phprpg/"><font size="1">[project phpRPG]</font></a>
<input type="submit" value="Login" class="inputbutton"><br>
</div>
</td></tr>
</table>
<input type="hidden" name="screenwidth" value="800">
</form>

<script language="Javascript">
<!--
	document.onLoad = document.login_form.user_name.focus();
// -->
</script>

</td>
<td><img src="' . PHPRPG_IMG . 'phantasy_bot.png" width="547" height="268" border="0" alt=""></td>
</tr>
</table>
<br>
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="15" border="0" alt=""><br>
		';

		echo '
<div align="center">
<a href="http://phprpg.org/"><img src="' . PHPRPG_IMG . 'phprpg_button1.gif"
	width="88" height="31" border="0" alt="phpRPG.org"></a>
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="6" border="0" alt="">
<a href="http://sourceforge.net/projects/phprpg/"><img src="http://sourceforge.net/sflogo.php?group_id=29487&type=1"
	width="88" height="31" border="0" alt="SourceForge.net"></a>
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="6" border="0" alt="">
<a href="http://www.php.net/"><img src="' . PHPRPG_IMG . 'php.png" width="88" height="31"
	border="0" alt="PHP"></a>
<img src="' . PHPRPG_IMG . 'x.png" width="0" height="0" hspace="6" border="0" alt="">
<a href="http://www.mysql.com/"><img src="' . PHPRPG_IMG . 'mysql.png" width="88" height="31"
	border="0" alt="MySQL"></a><br>
</div>
		';

	require_once('template_footer.inc.php');
}


?>