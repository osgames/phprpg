<?php
$seconds_since_project = time() - mktime(15,6,0,6,16,2001);
$days_since_project = floor($seconds_since_project / (60 * 60 * 24));

echo '
<br>
<font style="font-size: 18px; font-family: Arial;">Tis all about thinking... coding... testing... thinking and thinking...</font><br>
It has been ' . $days_since_project . ' days since this phpRPG project was established.<br>
<br>
The game is yet to be fully playable. There is much improvement in this version and there will be more to come.
At the moment, the game system is still a bit flawed... These will be rectified until version 0.5 is ready!<br>
<br>
<b>1 June 2002</b> It seems like the exp and hp rise is not working properly. I will fix that once I have time for it. Browse the CVS and see whether you can pick the bugs. :)<br>
<br>
<font style="font-size: 18px; font-family: Arial;">Instructions</font><br>
A [Local View] is the area bounded by a single tile. If there are other players or items in the area, they will be shown.
Your unique position on the map is indicated below the navigation box. You can navigate between areas by clicking on adjacent tiles or using
your keyboard if JavaScript is enabled:<br>
- [1] -> move down-left<br>
- [2] -> move down<br>
- [3] -> move down-right<br>
- [4] -> move up-left<br>
- [5] -> move up<br>
- [6] -> move up-right<br>
- [enter] -> enter chat mode (focus chat box)<br>
- [ESC] -> exit chat mode<br>
<br>
To the right of the user interface is the game time and player statistics. The timing in ph&#39;@ntasy is four times faster than real time.
Each cycle of the day is indicated by the order of red, yellow, green and blue.<br>
<br>
Moving around the map consumes stamina and every move of the player may or may not incur a real-time delay.<br>
<br>
If there are items on the ground, you can simply pick it up by clicking on it. To use a weapon, you will have to equip it in the [Player Status] menu.
Sometimes an item which is rare requires greater experience to recognise. You can click on [identify] to try your luck and chance.<br>
<br>
[Battle Mode] allows combat to be engaged. Each attack is initiated by clicking on the [Attack] icon. On every attack, the target would counterattack.
Once the health of a player has reached zero, he or she will fall unconscious and be transferred to a specific location. Any item on the defeated
player will be dropped onto the ground.<br>
<br>
[Online Players] tells you who is actually logged on. At the bottom are top 10 players who have achieved the most experience.<br>
<br>
To start off with, move around the map and pick up some "auras" if you can find one. Equip the auras and start beating people up! :)<br>
<br>
Target One, Target Two and Target Three would be your ideal first targets.<br>
<br>
A more refined documentation will be on its way once the game is more completed! Enjoy!<br>
<br>
<br>
<font style="font-size: 18px; font-family: Arial;">Help Wanted!</font><br>
We are in need of three major positions:<br>
[1] PHP / MySQL Coders<br>
[2] Web Designers<br>
[3] Graphics Artists<br>
<br>
If you are interested in helping us, I strongly recommend you doing the following:<br>
[1] Download a copy of the latest source and get a feel of the coding style<br>
- Latest release is version 0.3. Available <a href="http://sourceforge.net/project/showfiles.php?group_id=29487">here</a>.<br>
- Current running version is 0.5. At the moment only available on the <a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/phprpg/">CVS</a>.<br>
<br>
[2] Subscribe to the <a href="http://sourceforge.net/mail/?group_id=29487">mailing lists</a> so you can give introduce yourself and participate in our discussions<br>
<br>
[3] Join the <a href="http://phprpg.sourceforge.net/irc/">IRC</a> channel where most active discussions take place<br>
<br>
[4] Take a look at the brief draft on recommended <a href="http://phprpg.sourceforge.net/developers/standards/">coding standards</a><br>
<br>
[5] Drop us an <a href="http://phprpg.sourceforge.net/contact/">e-mail</a> if you have any other concerns<br>
';
?>