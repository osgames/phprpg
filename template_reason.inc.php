<?php
/******************************************************************************/
/*																			*/
/* template_reason.inc.php - Outputs Reasons - Success or Failure			*/
/*																			*/
/******************************************************************************/
/*																			*/
/* Requirements: PHP, MySQL and web-browser									*/
/*																			*/
/* Author: Timothy TS Chung													*/
/*		<ttschung@users.sourceforge.net>									*/
/*																			*/
/* Created: 18 April 2002													*/
/*																			*/
/* Copyright (c) 2001-2002 Timothy TS Chung									*/
/*																			*/
/* This file is part of phpRPG (http://phpRPG.org/)							*/
/*																			*/
/* phpRPG is free software; you can redistribute it and/or modify			*/
/* it under the terms of the GNU General Public License as published by		*/
/* the Free Software Foundation; either version 2 of the License, or		 */
/* (at your option) any later version.										*/
/*																			*/
/* This program is distributed in the hope that it will be useful,			*/
/* but WITHOUT ANY WARRANTY; without even the implied warranty of			*/
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			 */
/* GNU General Public License for more details.								*/
/*																			*/
/* You should have received a copy of the GNU General Public License		 */
/* along with this program; if not, write to the Free Software				*/
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  */
/*																			*/
/******************************************************************************/


error_reporting (E_ALL);

require_once('lib.inc.php');
require_once('lib_session.inc.php');

if (eregi('.inc.php', PHP_SELF))
{
	if (PHPRPG_DEBUG_AUTOREDIRECT)
	{
		echo 'Can not access this file directly!<br>';
		echo '<a href="' . PHPRPG_BASE . 'index.php">Click to continue</a>';
	} else {
		header("Location: index.php");
	}
}

DbConnect();

switch($reason)
{
case 'stamina':
	echo '<br><font color="#eeeeee">MOVEMENT NOT POSSIBLE:</font> You do not have enough stamina remaining!';
	break;
case 'delay':
	if ($user_time < $char['delay'])
	{
		// Delay exists and reason was requested -> show reason
		switch ($char['delay_reason'])
		{
			case 'disarm_penalty':
				$delay_reason = 'You are still recovering from disarming an item!';
				break;
			case 'arm_penalty':
				$delay_reason = 'You are still recovering from arming the weapon!';
				break;
			case 'take_penalty':
				$delay_reason = 'You are still recovering from taking an item!!';
				break;
			case 'move_penalty':
				$delay_reason = 'Your physical status does not allow you to move at such speed!';
				break;
			default:
				$delay_reason = 'Unexpected delay! Please contact an admin!';
		}

		echo '<br><font size="1"><font color="#eeeeee">NOT POSSIBLE:</font> ' . $delay_reason . ' Please wait for another <font color="#eeeeee">' . abs($user_time - $char['delay']) . ' sec</font></font>.';
	}
	break;
}


?>